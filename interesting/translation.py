from modeltranslation.translator import translator, TranslationOptions
from .models import Interesting


class InterestingTranslationOptions(TranslationOptions):
    fields = ('name', 'short_description', 'description')


translator.register(Interesting, InterestingTranslationOptions)