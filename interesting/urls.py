from django.conf.urls import patterns, url
from .views import PlaceDetailView

urlpatterns = patterns('',
                       url(r'^$', 'interesting.views.home', name='home'),
                       url(r'^(?P<slug>\w+)/$', PlaceDetailView.as_view(), name='detail'),
                       )