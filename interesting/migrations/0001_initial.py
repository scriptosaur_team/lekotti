# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Interesting'
        db.create_table(u'interesting_interesting', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True)),
            ('name_fi', self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('winter', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('summer', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('short_description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('short_description_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('short_description_fi', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('short_description_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description', self.gf('cked.fields.RichTextField')()),
            ('description_ru', self.gf('cked.fields.RichTextField')(null=True, blank=True)),
            ('description_fi', self.gf('cked.fields.RichTextField')(null=True, blank=True)),
            ('description_en', self.gf('cked.fields.RichTextField')(null=True, blank=True)),
            ('map', self.gf('addresspicker.fields.AddressPickerField')(max_length=64, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True)),
        ))
        db.send_create_signal(u'interesting', ['Interesting'])

        # Adding model 'InterestingPhoto'
        db.create_table(u'interesting_interestingphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['interesting.Interesting'])),
        ))
        db.send_create_signal(u'interesting', ['InterestingPhoto'])


    def backwards(self, orm):
        # Deleting model 'Interesting'
        db.delete_table(u'interesting_interesting')

        # Deleting model 'InterestingPhoto'
        db.delete_table(u'interesting_interestingphoto')


    models = {
        u'interesting.interesting': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Interesting'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'description': ('cked.fields.RichTextField', [], {}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'map': ('addresspicker.fields.AddressPickerField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'short_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'summer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'winter': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'interesting.interestingphoto': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'InterestingPhoto'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['interesting.Interesting']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['interesting']