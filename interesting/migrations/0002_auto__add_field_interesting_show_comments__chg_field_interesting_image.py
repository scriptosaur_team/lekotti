# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Interesting.show_comments'
        db.add_column(u'interesting_interesting', 'show_comments',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # Changing field 'Interesting.image'
        db.alter_column(u'interesting_interesting', 'image', self.gf('django_resized.forms.ResizedImageField')(max_length=100, max_width=1000))

        # Changing field 'InterestingPhoto.image'
        db.alter_column(u'interesting_interestingphoto', 'image', self.gf('django_resized.forms.ResizedImageField')(max_length=100, max_width=1000))

    def backwards(self, orm):
        # Deleting field 'Interesting.show_comments'
        db.delete_column(u'interesting_interesting', 'show_comments')


        # Changing field 'Interesting.image'
        db.alter_column(u'interesting_interesting', 'image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100))

        # Changing field 'InterestingPhoto.image'
        db.alter_column(u'interesting_interestingphoto', 'image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100))

    models = {
        u'interesting.interesting': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Interesting'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'description': ('cked.fields.RichTextField', [], {}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1000'}),
            'map': ('addresspicker.fields.AddressPickerField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'short_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'show_comments': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'summer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'winter': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'interesting.interestingphoto': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'InterestingPhoto'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['interesting.Interesting']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1000'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['interesting']