from django import forms
from .models import PlaceComment


class CommentForm(forms.ModelForm):
    class Meta:
        model = PlaceComment
        fields = ['message']