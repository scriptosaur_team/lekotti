from django.contrib import admin
from main.utils import TranslationTabMixin
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.admin import AdminImageMixin
from .models import *


class GalleryInline(AdminImageMixin, admin.TabularInline):
    model = InterestingPhoto
    extra = 8


class InterestingAdmin(AdminImageMixin, TranslationTabMixin):
    list_display = ['name', 'slug', 'winter', 'summer', 'ordering']
    list_editable = ['winter', 'summer', 'ordering', 'slug']
    inlines = [GalleryInline]
    fieldsets = (
        (None, {'fields': ['name', 'slug', 'image', 'short_description', 'description', 'map']}),
        (_('view options'), {'classes': ('collapse',),
                             'fields': ['winter', 'summer', 'show_comments', 'ordering']}),
        (_('SEO winter'), {'classes': ('collapse',),
                           'fields': ['title_winter', 'meta_description_winter', 'meta_keywords_winter']}),
        (_('SEO summer'), {'classes': ('collapse',),
                           'fields': ['title_summer', 'meta_description_summer', 'meta_keywords_summer']}),
    )


class PlaceCommentAdmin(admin.ModelAdmin):
    list_display = ['message', 'author', 'add_time']
    list_filter = ['place']
    ordering = ['-add_time']


admin.site.register(Interesting, InterestingAdmin)
admin.site.register(PlaceComment, PlaceCommentAdmin)