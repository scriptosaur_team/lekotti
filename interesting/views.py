from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import get_object_or_404
import json
from main.utils import get_season
from .models import *
import datetime
from django.views.generic import DetailView
from django.views.generic.edit import FormView
from .forms import CommentForm
from django.shortcuts import redirect


def home(request):
    season = get_season(request)
    interesting_places = Interesting.objects.filter(**{season: True})
    map_data = {place.id: {
        'name': unicode(place.name),
        'coords': unicode(place.map).split(','),
        'address': unicode(place.address),
        'phone': unicode(place.phone),
        'link': unicode(place.get_absolute_url())
    } for place in interesting_places}
    json_data = json.dumps(map_data)
    return render_to_response(
        'interesting/home.html',
        {
            'interesting_places': interesting_places,
            'json_data': json_data
        },
        context_instance=RequestContext(request)
    )


def detail(request, pk):
    interesting = get_object_or_404(Interesting, pk=pk)
    return render_to_response('interesting/detail.html', {'interesting': interesting},
                              context_instance=RequestContext(request))


class PlaceDetailView(DetailView, FormView):
    form_class = CommentForm
    model = Interesting
    template_name = 'interesting/detail.html'
    context_object_name = 'interesting'

    def get_context_data(self, **kwargs):
        context = super(PlaceDetailView, self).get_context_data(**kwargs)
        context['comments'] = PlaceComment.objects.filter(place=self.get_object())
        context['form'] = CommentForm
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = CommentForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.place = self.object
            f.add_time = datetime.datetime.now()
            f.author = request.user
            f.save()
            if request.is_ajax():
                self.template_name = 'main/comment_created.html'
                ctx = self.get_context_data(form=form)
                ctx.update({'comment': f})
                return self.render_to_response(ctx)
            else:
                return redirect(self.object)
        else:
            return self.render_to_response(self.get_context_data(form=form))