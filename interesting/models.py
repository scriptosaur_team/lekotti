from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField
from cked.fields import RichTextField
from addresspicker.fields import AddressPickerField
from django_resized import ResizedImageField
from main.models import Comment, SEOFields


class Interesting(SEOFields, models.Model):
    name = models.CharField(_('name'), max_length=512)
    slug = models.SlugField(_('slug'), unique=True, null=True, blank=True)
    ordering = models.IntegerField(_('ordering'), default=100)
    image = ResizedImageField(max_width=1000, max_height=1000, upload_to='activity', verbose_name=_('main image'))
    winter = models.BooleanField(_('active in winter'), default=True)
    summer = models.BooleanField(_('active in summer'), default=True)
    short_description = models.TextField(_('short description'), blank=True, null=True)
    description = RichTextField(_('description'))
    map = AddressPickerField(_('place on map'), max_length=64, null=True, blank=True)
    phone = models.CharField(_('phone'), max_length=64, null=True, blank=True)
    address = models.CharField(_('address'), max_length=512, null=True, blank=True)
    show_comments = models.BooleanField(_('enable comments'), default=False)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('interesting place')
        verbose_name_plural = _('interesting places')

    def __unicode__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('interesting:detail', args=[str(self.slug)])


class InterestingPhoto(models.Model):
    image = ResizedImageField(max_width=1000, upload_to='event_gallery', verbose_name=_('image'))
    ordering = models.IntegerField(_('ordering'), default=100)
    event = models.ForeignKey(Interesting)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('photo gallery')
        verbose_name_plural = _('photo gallery')


class PlaceComment(Comment):
    place = models.ForeignKey(Interesting, verbose_name=_('place'))

    class Meta(Comment.Meta):
        verbose_name = _('place comment')
        verbose_name_plural = _('place comments')