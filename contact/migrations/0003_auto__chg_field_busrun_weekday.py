# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'BusRun.weekday'
        db.alter_column(u'contact_busrun', 'weekday', self.gf('django.db.models.fields.PositiveSmallIntegerField')())

    def backwards(self, orm):

        # Changing field 'BusRun.weekday'
        db.alter_column(u'contact_busrun', 'weekday', self.gf('django.db.models.fields.CharField')(max_length=9))

    models = {
        u'contact.busrun': {
            'Meta': {'ordering': "['weekday', 'arrival']", 'object_name': 'BusRun'},
            'arrival': ('django.db.models.fields.TimeField', [], {}),
            'departure': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'weekday': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'contact.busrunpoint': {
            'Meta': {'object_name': 'BusRunPoint'},
            'bus_run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contact.BusRun']"}),
            'bus_stop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contact.BusStop']"}),
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'contact.busstop': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'BusStop'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['contact']