# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BusRunPoint'
        db.create_table(u'contact_busrunpoint', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bus_run', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contact.BusRun'])),
            ('bus_stop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contact.BusStop'])),
            ('direction', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('time', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'contact', ['BusRunPoint'])

        # Adding model 'BusRun'
        db.create_table(u'contact_busrun', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('weekday', self.gf('django.db.models.fields.CharField')(max_length=9)),
            ('arrival', self.gf('django.db.models.fields.TimeField')()),
            ('departure', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal(u'contact', ['BusRun'])

        # Adding model 'BusStop'
        db.create_table(u'contact_busstop', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal(u'contact', ['BusStop'])


    def backwards(self, orm):
        # Deleting model 'BusRunPoint'
        db.delete_table(u'contact_busrunpoint')

        # Deleting model 'BusRun'
        db.delete_table(u'contact_busrun')

        # Deleting model 'BusStop'
        db.delete_table(u'contact_busstop')


    models = {
        u'contact.busrun': {
            'Meta': {'object_name': 'BusRun'},
            'arrival': ('django.db.models.fields.TimeField', [], {}),
            'departure': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'weekday': ('django.db.models.fields.CharField', [], {'max_length': '9'})
        },
        u'contact.busrunpoint': {
            'Meta': {'object_name': 'BusRunPoint'},
            'bus_run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contact.BusRun']"}),
            'bus_stop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contact.BusStop']"}),
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'contact.busstop': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'BusStop'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['contact']