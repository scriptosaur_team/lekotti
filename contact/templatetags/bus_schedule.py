from django import template
from ..models import BusRun, BusStop, BusRunPoint
from datetime import datetime

register = template.Library()


@register.inclusion_tag('contact/bus_schedule.html', takes_context=True)
def bus_schedule(context, schedule_date=datetime.today()):
    request = context['request']
    weekday = schedule_date.weekday()
    bus_runs = BusRun.objects.filter(weekday=weekday)
    bus_stops = BusStop.objects.all()

    def stops4run(run_id, bus_stops):
        stops = []
        stop_points = BusRunPoint.objects.filter(bus_run=run_id)
        for bus_stop in bus_stops:
            stop_table = {
                'name': bus_stop.name,
                'to_lekotti': stop_points.filter(bus_stop=bus_stop, direction='to_lekotti').values_list('time')[0][0],
                'from_lekotti': stop_points.filter(bus_stop=bus_stop, direction='from_lekotti').values_list('time')[0][0]
            }
            stops.append(stop_table)
        return stops
    for bus_run in bus_runs:
        bus_run.table = stops4run(bus_run.id, bus_stops)

    schedule = {
        'today': schedule_date,
        'bus_stops': bus_stops,
        'bus_runs': bus_runs,
        'full': False if request.is_ajax() else True
    }
    return schedule