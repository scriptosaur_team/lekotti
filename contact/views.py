from django.shortcuts import render_to_response
from django.template import RequestContext
import datetime


def bus_schedule(request, month, day, year):
    schedule_date = datetime.date(year=int(year), month=int(month), day=int(day))
    return render_to_response('contact/_bus_schedule.html', {'schedule_date': schedule_date},
                              context_instance=RequestContext(request))