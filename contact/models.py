from django.db import models
from django.utils.translation import ugettext_lazy as _
import datetime


class BusStop(models.Model):
    name = models.CharField(_('name'), max_length=128)
    ordering = models.IntegerField(_('ordering'), default=100)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name = _('bus stop')
        verbose_name_plural = _('bus stops')
        ordering = ['ordering']


class BusRun(models.Model):
    WEEKDAY_CHOICES = (
        (0, _('monday')),
        (1, _('tuesday')),
        (2, _('wednesday')),
        (3, _('thursday')),
        (4, _('friday')),
        (5, _('saturday')),
        (6, _('sunday')),
    )
    weekday = models.PositiveSmallIntegerField(_('day of the week'), choices=WEEKDAY_CHOICES)
    arrival = models.TimeField(_('arrival time'))
    departure = models.TimeField(_('departure time'))

    def verbose_weekday(self):
        return datetime.date(2001, 1, self.weekday+1).strftime('%A')

    def __unicode__(self):
        return u'%s, %s - %s' % (self.verbose_weekday(), self.arrival, self.departure)

    class Meta:
        verbose_name = _('bus run')
        verbose_name_plural = _('bus runs')
        ordering = ['weekday', 'arrival']


class BusRunPoint(models.Model):
    DIRECTION_CHOICES = (
        ('to_lekotti', _('to Lekotti')),
        ('from_lekotti', _('from Lekotti')),
    )
    bus_run = models.ForeignKey(BusRun)
    bus_stop = models.ForeignKey(BusStop)
    direction = models.CharField(_('direction'), choices=DIRECTION_CHOICES, max_length=16)
    time = models.TimeField(_('stop time'), null=True, blank=True)