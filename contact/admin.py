from django.contrib import admin
from .models import *


class BusStopAdmin(admin.ModelAdmin):
    list_display = ['name', 'ordering']
    list_editable = ['ordering']

admin.site.register(BusStop, BusStopAdmin)


class BusRunPointInline(admin.TabularInline):
    model = BusRunPoint
    extra = 8


class BusRunAdmin(admin.ModelAdmin):
    inlines = [BusRunPointInline]


admin.site.register(BusRun, BusRunAdmin)