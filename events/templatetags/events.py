from django import template
from ..models import Event

register = template.Library()


@register.inclusion_tag('events/events_announce.html')
def events_announce():
    events = Event.actual.filter(main=True)
    return {'events': events}


@register.inclusion_tag('events/events_for.html')
def events_for(entity):
    if entity == 'ski_slope':
        events = Event.actual.filter(ski_slope=True)
    elif entity == 'habitation':
        events = Event.actual.filter(habitation=True)
    else:
        events = None
    return {'events': events}