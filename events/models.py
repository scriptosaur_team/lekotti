from django.db import models
from django.db.models import Q
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from cked.fields import RichTextField
from addresspicker.fields import AddressPickerField
from main.models import Comment, SEOFields
import datetime
from django_resized import ResizedImageField


class UpcomingEventsManager(models.Manager):
    def get_query_set(self):
        return super(UpcomingEventsManager, self).get_query_set().filter(date__gte=datetime.date.today())


class ActualEventsManager(models.Manager):
    def get_query_set(self):
        return super(ActualEventsManager, self).get_query_set()\
            .filter(Q(date__gte=datetime.date.today()) | Q(date=None))


class Event(SEOFields, models.Model):
    name = models.CharField(_('title'), max_length=512)
    slug = models.SlugField(_('slug'), unique=True, null=True, blank=True)
    date = models.DateField(_('date'), blank=True, null=True)
    ordering = models.IntegerField(_('ordering'), default=100)
    image = ResizedImageField(max_width=1000, max_height=1000, upload_to='events', verbose_name=_('main image'))
    short_description = models.TextField(_('short description'), blank=True, null=True)
    description = RichTextField(_('description'))
    type = models.PositiveSmallIntegerField(_('type'), choices=(
        (0, _('regional')),
        (2, _('promo'))
    ), null=True, blank=True)

    main = models.BooleanField(_('show on main page'), default=True)
    ski_slope = models.BooleanField(_('show on ski slope page'), default=False)
    habitation = models.BooleanField(_('show on habitation page'), default=False)
    winter = models.BooleanField(_('active in winter'), default=True)
    summer = models.BooleanField(_('active in summer'), default=True)

    map = AddressPickerField(_('place on map'), max_length=64, null=True, blank=True)
    show_comments = models.BooleanField(_('enable comments'), default=False)

    class Meta:
        ordering = ['date', 'ordering']
        verbose_name = _('event')
        verbose_name_plural = _('events')

    def __unicode__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('events:detail', args=[str(self.slug)])

    objects = models.Manager()
    upcoming = UpcomingEventsManager()
    actual = ActualEventsManager()


class EventPhoto(models.Model):
    image = ResizedImageField(max_width=1000, max_height=1000, upload_to='event_gallery', verbose_name=_('image'))
    ordering = models.IntegerField(_('ordering'), default=100)
    event = models.ForeignKey(Event)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('photo gallery')
        verbose_name_plural = _('photo gallery')


class EventComment(Comment):
    event = models.ForeignKey(Event, verbose_name=_('event'))

    class Meta(Comment.Meta):
        verbose_name = _('event comment')
        verbose_name_plural = _('event comments')