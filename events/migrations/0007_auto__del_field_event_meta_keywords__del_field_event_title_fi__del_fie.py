# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Event.meta_keywords'
        db.delete_column(u'events_event', 'meta_keywords')

        # Deleting field 'Event.title_fi'
        db.delete_column(u'events_event', 'title_fi')

        # Deleting field 'Event.title_en'
        db.delete_column(u'events_event', 'title_en')

        # Deleting field 'Event.meta_keywords_en'
        db.delete_column(u'events_event', 'meta_keywords_en')

        # Deleting field 'Event.meta_keywords_fi'
        db.delete_column(u'events_event', 'meta_keywords_fi')

        # Deleting field 'Event.meta_description_fi'
        db.delete_column(u'events_event', 'meta_description_fi')

        # Deleting field 'Event.meta_description'
        db.delete_column(u'events_event', 'meta_description')

        # Deleting field 'Event.meta_keywords_ru'
        db.delete_column(u'events_event', 'meta_keywords_ru')

        # Deleting field 'Event.meta_description_ru'
        db.delete_column(u'events_event', 'meta_description_ru')

        # Deleting field 'Event.meta_description_en'
        db.delete_column(u'events_event', 'meta_description_en')

        # Deleting field 'Event.title'
        db.delete_column(u'events_event', 'title')

        # Deleting field 'Event.title_ru'
        db.delete_column(u'events_event', 'title_ru')

        # Adding field 'Event.title_winter'
        db.add_column(u'events_event', 'title_winter',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_winter_ru'
        db.add_column(u'events_event', 'title_winter_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_winter_fi'
        db.add_column(u'events_event', 'title_winter_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_winter_en'
        db.add_column(u'events_event', 'title_winter_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_winter'
        db.add_column(u'events_event', 'meta_description_winter',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_winter_ru'
        db.add_column(u'events_event', 'meta_description_winter_ru',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_winter_fi'
        db.add_column(u'events_event', 'meta_description_winter_fi',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_winter_en'
        db.add_column(u'events_event', 'meta_description_winter_en',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_winter'
        db.add_column(u'events_event', 'meta_keywords_winter',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_winter_ru'
        db.add_column(u'events_event', 'meta_keywords_winter_ru',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_winter_fi'
        db.add_column(u'events_event', 'meta_keywords_winter_fi',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_winter_en'
        db.add_column(u'events_event', 'meta_keywords_winter_en',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_summer'
        db.add_column(u'events_event', 'title_summer',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_summer_ru'
        db.add_column(u'events_event', 'title_summer_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_summer_fi'
        db.add_column(u'events_event', 'title_summer_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_summer_en'
        db.add_column(u'events_event', 'title_summer_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_summer'
        db.add_column(u'events_event', 'meta_description_summer',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_summer_ru'
        db.add_column(u'events_event', 'meta_description_summer_ru',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_summer_fi'
        db.add_column(u'events_event', 'meta_description_summer_fi',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_summer_en'
        db.add_column(u'events_event', 'meta_description_summer_en',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_summer'
        db.add_column(u'events_event', 'meta_keywords_summer',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_summer_ru'
        db.add_column(u'events_event', 'meta_keywords_summer_ru',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_summer_fi'
        db.add_column(u'events_event', 'meta_keywords_summer_fi',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_summer_en'
        db.add_column(u'events_event', 'meta_keywords_summer_en',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Event.meta_keywords'
        db.add_column(u'events_event', 'meta_keywords',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_fi'
        db.add_column(u'events_event', 'title_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_en'
        db.add_column(u'events_event', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_en'
        db.add_column(u'events_event', 'meta_keywords_en',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_fi'
        db.add_column(u'events_event', 'meta_keywords_fi',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_fi'
        db.add_column(u'events_event', 'meta_description_fi',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description'
        db.add_column(u'events_event', 'meta_description',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_keywords_ru'
        db.add_column(u'events_event', 'meta_keywords_ru',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_ru'
        db.add_column(u'events_event', 'meta_description_ru',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.meta_description_en'
        db.add_column(u'events_event', 'meta_description_en',
                      self.gf('django.db.models.fields.TextField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title'
        db.add_column(u'events_event', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_ru'
        db.add_column(u'events_event', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Event.title_winter'
        db.delete_column(u'events_event', 'title_winter')

        # Deleting field 'Event.title_winter_ru'
        db.delete_column(u'events_event', 'title_winter_ru')

        # Deleting field 'Event.title_winter_fi'
        db.delete_column(u'events_event', 'title_winter_fi')

        # Deleting field 'Event.title_winter_en'
        db.delete_column(u'events_event', 'title_winter_en')

        # Deleting field 'Event.meta_description_winter'
        db.delete_column(u'events_event', 'meta_description_winter')

        # Deleting field 'Event.meta_description_winter_ru'
        db.delete_column(u'events_event', 'meta_description_winter_ru')

        # Deleting field 'Event.meta_description_winter_fi'
        db.delete_column(u'events_event', 'meta_description_winter_fi')

        # Deleting field 'Event.meta_description_winter_en'
        db.delete_column(u'events_event', 'meta_description_winter_en')

        # Deleting field 'Event.meta_keywords_winter'
        db.delete_column(u'events_event', 'meta_keywords_winter')

        # Deleting field 'Event.meta_keywords_winter_ru'
        db.delete_column(u'events_event', 'meta_keywords_winter_ru')

        # Deleting field 'Event.meta_keywords_winter_fi'
        db.delete_column(u'events_event', 'meta_keywords_winter_fi')

        # Deleting field 'Event.meta_keywords_winter_en'
        db.delete_column(u'events_event', 'meta_keywords_winter_en')

        # Deleting field 'Event.title_summer'
        db.delete_column(u'events_event', 'title_summer')

        # Deleting field 'Event.title_summer_ru'
        db.delete_column(u'events_event', 'title_summer_ru')

        # Deleting field 'Event.title_summer_fi'
        db.delete_column(u'events_event', 'title_summer_fi')

        # Deleting field 'Event.title_summer_en'
        db.delete_column(u'events_event', 'title_summer_en')

        # Deleting field 'Event.meta_description_summer'
        db.delete_column(u'events_event', 'meta_description_summer')

        # Deleting field 'Event.meta_description_summer_ru'
        db.delete_column(u'events_event', 'meta_description_summer_ru')

        # Deleting field 'Event.meta_description_summer_fi'
        db.delete_column(u'events_event', 'meta_description_summer_fi')

        # Deleting field 'Event.meta_description_summer_en'
        db.delete_column(u'events_event', 'meta_description_summer_en')

        # Deleting field 'Event.meta_keywords_summer'
        db.delete_column(u'events_event', 'meta_keywords_summer')

        # Deleting field 'Event.meta_keywords_summer_ru'
        db.delete_column(u'events_event', 'meta_keywords_summer_ru')

        # Deleting field 'Event.meta_keywords_summer_fi'
        db.delete_column(u'events_event', 'meta_keywords_summer_fi')

        # Deleting field 'Event.meta_keywords_summer_en'
        db.delete_column(u'events_event', 'meta_keywords_summer_en')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'events.event': {
            'Meta': {'ordering': "['date', 'ordering']", 'object_name': 'Event'},
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('cked.fields.RichTextField', [], {}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'habitation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1000', 'max_height': '1000'}),
            'main': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'map': ('addresspicker.fields.AddressPickerField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'meta_description_summer': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_description_summer_en': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_description_summer_fi': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_description_summer_ru': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_description_winter': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_description_winter_en': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_description_winter_fi': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_description_winter_ru': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_summer': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_summer_en': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_summer_fi': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_summer_ru': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_winter': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_winter_en': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_winter_fi': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_keywords_winter_ru': ('django.db.models.fields.TextField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'short_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'show_comments': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ski_slope': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'summer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title_summer': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_summer_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_summer_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_summer_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_winter': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_winter_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_winter_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_winter_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'winter': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'events.eventcomment': {
            'Meta': {'ordering': "['add_time']", 'object_name': 'EventComment'},
            'add_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 5, 19, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['events.EventComment']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'events.eventphoto': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'EventPhoto'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1000', 'max_height': '1000'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['events']