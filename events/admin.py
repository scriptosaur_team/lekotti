from django.contrib import admin
from main.utils import TranslationTabMixin
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.admin import AdminImageMixin
from .models import *


class GalleryInline(AdminImageMixin, admin.TabularInline):
    model = EventPhoto
    extra = 8


class EventAdmin(AdminImageMixin, TranslationTabMixin):
    list_display = ['name', 'slug', 'winter', 'summer', 'date', 'type', 'ski_slope', 'habitation', 'main']
    list_editable = ['winter', 'summer', 'ski_slope', 'habitation', 'main', 'slug']
    inlines = [GalleryInline]
    fieldsets = (
        (None, {'fields': ['name', 'slug', 'type', 'date', 'image', 'short_description', 'description', 'map']}),
        (_('view options'), {'classes': ('collapse',),
                             'fields': [
                                 'main', 'ski_slope', 'habitation', 'winter', 'summer', 'show_comments', 'ordering'
                             ]}),
        (_('SEO winter'), {'classes': ('collapse',),
                           'fields': ['title_winter', 'meta_description_winter', 'meta_keywords_winter']}),
        (_('SEO summer'), {'classes': ('collapse',),
                           'fields': ['title_summer', 'meta_description_summer', 'meta_keywords_summer']}),
    )


class EventCommentAdmin(admin.ModelAdmin):
    list_display = ['message', 'author', 'add_time']
    list_filter = ['event']
    ordering = ['-add_time']


admin.site.register(Event, EventAdmin)
admin.site.register(EventComment, EventCommentAdmin)
