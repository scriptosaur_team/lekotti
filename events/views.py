from django.shortcuts import render_to_response
from django.template import RequestContext
from .models import Event, EventComment
from main.utils import get_season
import datetime
from django.views.generic import DetailView
from django.views.generic.edit import FormView
from .forms import CommentForm
from django.shortcuts import redirect


def home(request, template='events/home.html', page_template='events/page.html'):
    season = get_season(request)
    context = {
        'events': Event.actual.filter(**{season: True}),
        'page_template': page_template
    }
    if request.is_ajax():
        template = page_template
    return render_to_response(template, context, context_instance=RequestContext(request))


class EventDetailView(DetailView, FormView):
    form_class = CommentForm
    model = Event
    template_name = 'events/detail.html'
    context_object_name = 'event'

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['comments'] = EventComment.objects.filter(event=self.get_object())
        context['form'] = CommentForm
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = CommentForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.event = self.object
            f.add_time = datetime.datetime.now()
            f.author = request.user
            f.save()
            if request.is_ajax():
                self.template_name = 'main/comment_created.html'
                ctx = self.get_context_data(form=form)
                ctx.update({'comment': f})
                return self.render_to_response(ctx)
            else:
                return redirect(self.object)
        else:
            return self.render_to_response(self.get_context_data(form=form))


def event_filter(request, code, template='events/events_filtered.html', page_template='events/page.html'):
    today = datetime.date.today()
    if code == 'promotions':
        event_list = Event.actual.filter(type=2)
    elif code == 'regional':
        event_list = Event.actual.filter(type=0, date__gte=today)
    elif code == 'past':
        event_list = Event.objects.filter(date__lt=today).order_by('-date')
    else:
        event_list = Event.objects.all()

    if get_season(request) == 'summer':
        event_list = event_list.filter(summer=True)
    if get_season(request) == 'winter':
        event_list = event_list.filter(winter=True)

    context = {
        'events': event_list,
        'page_template': page_template,
        'code': code
    }
    if request.is_ajax():
        template = page_template
    return render_to_response(template, context, context_instance=RequestContext(request))