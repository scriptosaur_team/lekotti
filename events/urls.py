from django.conf.urls import patterns, url
from .views import EventDetailView

urlpatterns = patterns('',
                       url(r'^$', 'events.views.home', name='home'),
                       url(r'^(?P<code>promotions)/$', 'events.views.event_filter', name='event_filter'),
                       url(r'^(?P<code>regional)/$', 'events.views.event_filter', name='event_filter'),
                       url(r'^(?P<code>past)/$', 'events.views.event_filter', name='event_filter'),
                       url(r'^(?P<slug>\w+)/$', EventDetailView.as_view(), name='detail'),
                       )