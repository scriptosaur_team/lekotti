from modeltranslation.translator import translator, TranslationOptions
from .models import Event


class EventTranslationOptions(TranslationOptions):
    fields = ('name', 'short_description', 'description')


translator.register(Event, EventTranslationOptions)