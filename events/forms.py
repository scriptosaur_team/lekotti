from django import forms
from .models import EventComment


class CommentForm(forms.ModelForm):
    class Meta:
        model = EventComment
        fields = ['message']