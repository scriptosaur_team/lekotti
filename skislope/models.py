from django.db import models
from django.utils.translation import ugettext_lazy as _
from tablefield.fields import TableField
from cked.fields import RichTextField
from sorl.thumbnail import ImageField, get_thumbnail
import datetime
from django_resized import ResizedImageField
from django.core.exceptions import ValidationError
from django.db.models import Q


class PriceList(models.Model):
    name = models.CharField(_('title'), max_length=256, blank=True, null=True)
    code = models.SlugField(_('slug'), max_length=32, help_text=_('for use in templates'))
    table_data = TableField(_('table'),
                            help_text=_('Click on cell to edit, right-click to ad or remove columns and rows.<br>'
                                        'First row contains table header'))

    def __unicode__(self):
        return u'%s [%s]' % (self.name, self.code)

    class Meta:
        verbose_name = _('price list')
        verbose_name_plural = _('price lists')
        ordering = ['name']


class ActualScheduleManager(models.Manager):
    def get_query_set(self):
        today = datetime.date.today()
        return super(ActualScheduleManager, self).get_query_set().filter(active_from__lte=today, active_to__gte=today)


class ActiveScheduleManager(models.Manager):
    def get_query_set(self):
        return super(ActiveScheduleManager, self).get_query_set().filter(active=True)


class Schedule(models.Model):
    active_from = models.DateField(_('active from'))
    active_to = models.DateField(_('active to'))
    description = RichTextField(_('description'), null=True, blank=True)
    active = models.BooleanField(_('active'), default=True)

    objects = models.Manager()
    actual = ActualScheduleManager()
    active_objects = ActiveScheduleManager()

    def __unicode__(self):
        return u'%s %s %s %s' % (_('from'), self.active_from, _('to'), self.active_to)

    class Meta:
        ordering = ['-active_from']
        verbose_name = _('schedule')
        verbose_name_plural = _('schedules')

    def clean(self):
        if self.active_from > self.active_to:
            raise ValidationError(_('possible dates are reversed?'))
        existing_schedules = Schedule.objects.exclude(id=self.id)
        for sch in existing_schedules:
            if sch.active_from <= self.active_from <= sch.active_to or \
                                    sch.active_from <= self.active_to <= sch.active_to:
                raise ValidationError(_('dates are crossing other schedule: %s') % sch)


class WorkTime(models.Model):
    WEEKDAY_CHOICES = (
        (0, _('monday')),
        (1, _('tuesday')),
        (2, _('wednesday')),
        (3, _('thursday')),
        (4, _('friday')),
        (5, _('saturday')),
        (6, _('sunday')),
    )
    weekday = models.PositiveSmallIntegerField(_('day of the week'), choices=WEEKDAY_CHOICES)
    active = models.BooleanField(_('workday'), default=True)
    open = models.TimeField(_('open time'), null=True, blank=True)
    close = models.TimeField(_('close time'), null=True, blank=True)
    schedule = models.ForeignKey(Schedule)

    def ancient_weekday(self):
        return datetime.date(2001, 1, self.weekday+1)

    def verbose_weekday(self):
        return datetime.date(2001, 1, self.weekday+1).strftime('%A')

    def __unicode__(self):
        return u'%s' % self.verbose_weekday()


class UpcomingRescheduleManager(models.Manager):
    def get_query_set(self):
        check_date = datetime.date.today()
        return super(UpcomingRescheduleManager, self).get_query_set().filter(
            Q(date__gte=check_date, date_end=None) | Q(date_end__gte=check_date)
        )


class Reschedule(models.Model):
    date = models.DateField(_('date'))
    date_end = models.DateField(_('end of period'), blank=True, null=True)
    holiday = models.BooleanField(_('holiday'), default=False)
    open = models.TimeField(_('open time'), null=True, blank=True)
    close = models.TimeField(_('close time'), null=True, blank=True)

    objects = models.Manager()
    upcoming = UpcomingRescheduleManager()

    def __unicode__(self):
        if self.date_end:
            s = u'%s %s %s %s' % (_('from'), self.date, _('to'), self.date_end)
        else:
            s = u'%s' % self.date
        return s

    class Meta:
        ordering = ['date']
        verbose_name = _('reschedule')
        verbose_name_plural = _('reschedules')


class PriceType(models.Model):
    name = models.CharField(_('name'), max_length=26)
    slug = models.SlugField(_('slug'), null=True, blank=True)
    ordering = models.IntegerField(_('ordering'), default=100)
#     image = ImageField(_('image'), upload_to='skislope')
    image = ResizedImageField(max_width=1000, upload_to='skislope', verbose_name=_('image'))
    content = RichTextField(_('tab content'), null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name = _('price type')
        verbose_name_plural = _('price types')
        ordering = ['ordering']