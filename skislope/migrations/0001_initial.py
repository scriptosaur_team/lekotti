# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PriceList'
        db.create_table(u'skislope_pricelist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_fi', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.SlugField')(max_length=32)),
            ('table_data', self.gf('tablefield.fields.TableField')()),
            ('table_data_ru', self.gf('tablefield.fields.TableField')(null=True, blank=True)),
            ('table_data_fi', self.gf('tablefield.fields.TableField')(null=True, blank=True)),
            ('table_data_en', self.gf('tablefield.fields.TableField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'skislope', ['PriceList'])

        # Adding model 'Schedule'
        db.create_table(u'skislope_schedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('active_from', self.gf('django.db.models.fields.DateField')()),
            ('active_to', self.gf('django.db.models.fields.DateField')()),
            ('description', self.gf('cked.fields.RichTextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'skislope', ['Schedule'])

        # Adding model 'WorkTime'
        db.create_table(u'skislope_worktime', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('weekday', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('open', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('close', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('schedule', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['skislope.Schedule'])),
        ))
        db.send_create_signal(u'skislope', ['WorkTime'])

        # Adding model 'Reschedule'
        db.create_table(u'skislope_reschedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('date_end', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('holiday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('open', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('close', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'skislope', ['Reschedule'])


    def backwards(self, orm):
        # Deleting model 'PriceList'
        db.delete_table(u'skislope_pricelist')

        # Deleting model 'Schedule'
        db.delete_table(u'skislope_schedule')

        # Deleting model 'WorkTime'
        db.delete_table(u'skislope_worktime')

        # Deleting model 'Reschedule'
        db.delete_table(u'skislope_reschedule')


    models = {
        u'skislope.pricelist': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceList'},
            'code': ('django.db.models.fields.SlugField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'table_data': ('tablefield.fields.TableField', [], {}),
            'table_data_en': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_fi': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_ru': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'})
        },
        u'skislope.reschedule': {
            'Meta': {'ordering': "['date']", 'object_name': 'Reschedule'},
            'close': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'date_end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'holiday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'open': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'skislope.schedule': {
            'Meta': {'ordering': "['-active_from']", 'object_name': 'Schedule'},
            'active_from': ('django.db.models.fields.DateField', [], {}),
            'active_to': ('django.db.models.fields.DateField', [], {}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'skislope.worktime': {
            'Meta': {'object_name': 'WorkTime'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'close': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'open': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['skislope.Schedule']"}),
            'weekday': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        }
    }

    complete_apps = ['skislope']