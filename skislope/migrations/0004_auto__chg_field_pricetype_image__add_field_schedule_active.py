# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'PriceType.image'
        db.alter_column(u'skislope_pricetype', 'image', self.gf('django_resized.forms.ResizedImageField')(max_length=100, max_width=1000))
        # Adding field 'Schedule.active'
        db.add_column(u'skislope_schedule', 'active',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):

        # Changing field 'PriceType.image'
        db.alter_column(u'skislope_pricetype', 'image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100))
        # Deleting field 'Schedule.active'
        db.delete_column(u'skislope_schedule', 'active')


    models = {
        u'skislope.pricelist': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceList'},
            'code': ('django.db.models.fields.SlugField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'table_data': ('tablefield.fields.TableField', [], {}),
            'table_data_en': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_fi': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_ru': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'})
        },
        u'skislope.pricetype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'PriceType'},
            'content': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'content_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'content_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1000'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '26'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '26', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '26', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '26', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'skislope.reschedule': {
            'Meta': {'ordering': "['date']", 'object_name': 'Reschedule'},
            'close': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'date_end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'holiday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'open': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'skislope.schedule': {
            'Meta': {'ordering': "['-active_from']", 'object_name': 'Schedule'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'active_from': ('django.db.models.fields.DateField', [], {}),
            'active_to': ('django.db.models.fields.DateField', [], {}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'skislope.worktime': {
            'Meta': {'object_name': 'WorkTime'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'close': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'open': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['skislope.Schedule']"}),
            'weekday': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        }
    }

    complete_apps = ['skislope']