from django import template
from ..models import PriceList, WorkTime, Reschedule, Schedule, PriceType
import datetime
from datetime import timedelta
from django.forms.models import model_to_dict
from django.db.models import Q

register = template.Library()


@register.inclusion_tag('skislope/ski_price_variants.html')
def ski_price_variants():
    return {'tabs': PriceType.objects.all()}


@register.inclusion_tag('skislope/ski_price.html')
def ski_price():
    price_data = {'price': {price_list.code: price_list for price_list in PriceList.objects.all()}}
    return price_data


@register.inclusion_tag('skislope/worktime.html')
def worktime():
    today = datetime.date.today()
    today_weekday = today.weekday()
    result = []
    for i in range(7):
        sch_date = today + timedelta(days=i)
        sch_weekday = (today_weekday + i) % 7
        actual_shedule_r = Schedule.active_objects.filter(active_from__lte=sch_date, active_to__gte=sch_date)
        if actual_shedule_r:
            actual_shedule = actual_shedule_r[0]
            wts = WorkTime.objects.filter(schedule=actual_shedule, weekday=sch_weekday)
            if wts:
                sch_worktime = model_to_dict(wts[0])
                if not sch_worktime['active']:
                    sch_worktime = {}
            else:
                sch_worktime = {}
            result.append(sch_worktime)
        else:
            result.append({})
    # schedule = [model_to_dict(row) for row in WorkTime.objects.filter(schedule=Schedule.actual.all()[0])]
    # result = schedule[today_weekday:]
    # result.extend(schedule[:today_weekday])

    def check_changes(check_day):
        check_date = check_day['date']
        changes = Reschedule.upcoming.filter(
            Q(date=check_date, date_end=None) | Q(date__lte=check_date, date_end__gte=check_date)
        )
        if changes:
            check_day.update({
                'open': changes.first().open,
                'close': changes.first().close,
            })
        return check_day

    def add_date(d):
        row_date = today + datetime.timedelta(days=d[0])
        d[1].update({'date': row_date})
        check_result = check_changes(d[1])
        return check_result
    result = map(add_date, enumerate(result))
    return {'schedule': result}