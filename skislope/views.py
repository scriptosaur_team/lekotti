from django.shortcuts import render_to_response
from django.template import RequestContext
from .models import WorkTime, Reschedule, Schedule


def worktime(request):
    return render_to_response('skislope/schedule.html', {
        'worktime': WorkTime.objects.all(),
        'schedules': Schedule.active_objects.all(),
        'reschedule': Reschedule.upcoming.all(),
    }, context_instance=RequestContext(request))