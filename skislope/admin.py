from django.contrib import admin
from main.utils import TranslationTabMixin
from .models import *


class PriceListAdmin(TranslationTabMixin):
    pass


class WorkTimeInline(admin.TabularInline):
    model = WorkTime
    extra = 7
    max_num = 7


class ScheduleAdmin(TranslationTabMixin):
    inlines = [WorkTimeInline]
    list_display = ['__unicode__', 'active']
    list_editable = ['active']


class PriceTypeAdmin(TranslationTabMixin):
    pass


admin.site.register(PriceList, PriceListAdmin)
admin.site.register(Reschedule)
admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(PriceType, PriceTypeAdmin)