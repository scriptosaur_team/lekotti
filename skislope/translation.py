from modeltranslation.translator import translator, TranslationOptions
from .models import PriceList, PriceType, Schedule


class PriceListTranslationOptions(TranslationOptions):
    fields = ('name', 'table_data')


class PriceTypeTranslationOptions(TranslationOptions):
    fields = ('name', 'content',)


class ScheduleTranslationOptions(TranslationOptions):
    fields = ('description',)


translator.register(PriceList, PriceListTranslationOptions)
translator.register(PriceType, PriceTypeTranslationOptions)
translator.register(Schedule, ScheduleTranslationOptions)