DEBUG = True
TEMPLATE_DEBUG = DEBUG

import os.path
SETTINGS_DIR = os.path.dirname(__file__)
PROJECT_ROOT = os.path.dirname(SETTINGS_DIR)


ADMINS = (
    ('Aleksandr Aleksandrov', 'scriptosaur@gmail.com'),
)
MANAGERS = ADMINS


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'lekotti.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

SITE_ID = 1
SECRET_KEY = '4lx(ci7^i1*b+wo42+*u9b2+!76$n@hc!xzkla%5=m=zjt4bn4'
ALLOWED_HOSTS = []
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True


LANGUAGE_CODE = 'ru'
gettext = lambda s: s
LANGUAGES = (
    ('fi', gettext('Finnish')),
    ('ru', gettext('Russian')),
    ('en', gettext('English')),
)
try:
    from lang import LANGUAGES
except ImportError:
    pass
MODELTRANSLATION_DEFAULT_LANGUAGE = 'fi'
MODELTRANSLATION_FALLBACK_LANGUAGES = ('fi', 'ru', 'en')
LOCALE_PATHS = (
    os.path.join(PROJECT_ROOT, "conf/locale"),
)

ROOT_URLCONF = 'lekotti.urls'
WSGI_APPLICATION = 'lekotti.wsgi.application'


INSTALLED_APPS = (
    'object_tools',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.admin',
    'south',
    'sorl.thumbnail',
    'modeltranslation',
    'endless_pagination',
    'cked',
    'widget_tweaks',
    'mptt',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'allauth.socialaccount.providers.vk',
    'allauth.socialaccount.providers.twitter',
    'addresspicker',
    'rosetta',
    'admin_help',
    'crequest',
    'daterange_filter',
    'email_templates'
)

TRANSLATABLE_APPS = (
    'main',
    'activity',
    'interesting',
    'skislope',
    'events',
    'contact',
    'habitation',
    'sitetree',
    'chunks',
    'tablefield',
    'gallery',
    'siteoptions',
    'booking',
)

INSTALLED_APPS += TRANSLATABLE_APPS

MIDDLEWARE_CLASSES = (
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'crequest.middleware.CrequestMiddleware',
    'booking.middleware.BookingSSLMiddleware',
    # 'main.middleware.ExtendFlatpageFallbackMiddleware',
    # 'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_LOADERS = (
    # 'dbtemplates.loader.Loader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)
TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), '..', 'templates').replace('\\', '/'),)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    "django.core.context_processors.request",
    "django.contrib.auth.context_processors.auth",
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
)

DBTEMPLATES_ADD_DEFAULT_SITE = True

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)


ACCOUNT_EMAIL_REQUIRED = False
ACCOUNT_EMAIL_VERIFICATION = 'none'
SOCIALACCOUNT_AUTO_SIGNUP = True
SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'SCOPE': ['email'],
        'METHOD': 'oauth2'
    },
    'google': {
        'SCOPE': ['https://www.googleapis.com/auth/userinfo.profile'],
        'AUTH_PARAMS': {'access_type': 'online'}
    }
}


ADMIN_TOOLS_THEMING_CSS = 'admin_tools/css/theming.css'
ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
ADMIN_TOOLS_MENU = 'menu.CustomMenu'


ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
YANDEX_TRANSLATE_KEY = 'trnsl.1.1.20131201T102937Z.024e4bfe31859a1f.8c135a81729ee37e08d9ffd1b7fbf20eb424f041'

ELFINDER_OPTIONS = {
    'root': os.path.join(PROJECT_ROOT, 'media', 'uploads'),
    'URL': '/media/uploads/',
}
CKEDITOR_OPTIONS = {
    'skin': 'moono',
    'toolbarGroups': [
        {'name': 'mode'},
        {'name': 'cleanup', 'items': ['cleanup', 'PasteText', 'PasteFromWord']},
        {'name': 'basicstyles'},
        {'name': 'links'},
        {'name': 'insert'},
        {'name': 'paragraph', 'groups': ['list', 'indent', 'align']},
    ],
    'filebrowserWindowWidth': 940,
    'filebrowserWindowHeight': 450,
    'enterMode': 2,
}


CSV_DELIMITER = ";"


STATIC_ROOT = ''
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, "static"),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

MEDIA_ROOT = os.path.join(PROJECT_ROOT, "media")
MEDIA_URL = '/media/'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


try:
    from paytrail_settings import *
except ImportError:
    pass


try:
    from local_settings import *
except ImportError:
    pass