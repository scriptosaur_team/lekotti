from lekotti import settings
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from .sitemap import sitemaps
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
admin.autodiscover()

import object_tools
object_tools.autodiscover()


urlpatterns = i18n_patterns('',
                            url(r'^gallery/', include('gallery.urls', namespace='gallery')),
                            url(r'^booking/', include('booking.urls', namespace='booking')),
                            url(r'^activity/', include('activity.urls', namespace='activity')),
                            url(r'^interesting/', include('interesting.urls', namespace='interesting')),
                            url(r'^services/', include('services.urls', namespace='services')),
                            url(r'^events/', include('events.urls', namespace='events')),
                            url(r'^ski_slope/worktime/', 'skislope.views.worktime', name='ski_slope_worktime'),
                            url(r'^bus_schedule/(?P<month>\d{2})/(?P<day>\d{2})/(?P<year>\d{4})/',
                                'contact.views.bus_schedule', name='bus_schedule'),
                            url(r'^admin/help/', include('admin_help.urls', namespace='help')),
                            )


if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
                            url(r'^admin/translate/', include('rosetta.urls')),
                            )


urlpatterns += patterns('',
                        url(r'^admin_tools/', include('admin_tools.urls')),
                        url(r'^cked/', include('cked.urls')),
                        url(r'^lang/(?P<code>[a-z]{2})/$', 'main.views.lang', name='lang'),
                        url(r'^accounts/profile/', 'main.views.login_success'),
                        url(r'^accounts/', include('allauth.urls')),
                        # url(r'^accounts/profile/', RedirectView.as_view(url='/')),
                        url(r'^logout/', 'main.views.user_logout', name='logout'),
                        # url(r'', include('social_auth.urls')),
                        )
urlpatterns += i18n_patterns('',
                             url(r'^admin/', include(admin.site.urls)),
                             )


urlpatterns += patterns('',
                        (r'^%s(.*)$' % settings.STATIC_URL[1:],
                         'django.views.static.serve',
                         {'document_root': settings.STATIC_ROOT}),
                        )
urlpatterns += patterns('',
                        (r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], 'django.views.static.serve',
                         {"document_root": settings.MEDIA_ROOT}),
                        )


if settings.DEBUG:
    urlpatterns += patterns('',
                            url(r'^404/$', TemplateView.as_view(template_name='404.html')),
                            url(r'^500/$', TemplateView.as_view(template_name='500.html')),
                            )


urlpatterns += patterns('',
                        url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
                        url(r'^robots\.txt$', 'main.views.robots')
                        )

urlpatterns += i18n_patterns('',
                             (r'^(?P<url>.*)$', 'main.views.extend_flatpage'),
                             )

urlpatterns += patterns('',
                        (r'^object-tools/', include(object_tools.tools.urls)),
                        )