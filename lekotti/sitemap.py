from django.conf import settings
from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap
from events.models import Event
from activity.models import Activity
from interesting.models import Interesting
from main.models import ExtendFlatPage
from django.core.urlresolvers import reverse
import re


class Sitemap4Lang(GenericSitemap):
    language_code = None

    def __init__(self, info_dict, priority=None, changefreq=None, language_code=None):
        super(Sitemap4Lang, self).__init__(info_dict, priority, changefreq)
        self.language_code = language_code

    def lang_prefix(self, url):
        if self.language_code:
            if re.match('^/(\w{2})/', url):
                url = re.sub('^/(\w{2})/', '/%s/' % self.language_code, url)
            else:
                url = '/%s%s' % (self.language_code, url)
        return url

    def location(self, obj):
        url = obj.get_absolute_url()
        return self.lang_prefix(url)


class StaticViewSitemap(Sitemap4Lang):

    def __init__(self, language_code=None):
        super(StaticViewSitemap, self).__init__({'queryset': None}, language_code=language_code)

    def items(self):
        return [
            'activity:home',
            'interesting:home',
            'events:home',
            'ski_slope_worktime',
        ]

    def location(self, item):
        return self.lang_prefix(reverse(item))


sitemaps = {}

for lang in settings.LANGUAGES:
    language_code = lang[0]
    sitemaps[u'interesting_%s' % language_code] = Sitemap4Lang(
        {
            'queryset': Interesting.objects.all()
        },
        language_code=language_code
    )
    sitemaps[u'activity_%s' % language_code] = Sitemap4Lang(
        {
            'queryset': Activity.objects.all()
        },
        language_code=language_code
    )
    sitemaps[u'events_%s' % language_code] = Sitemap4Lang(
        {
            'queryset': Event.objects.all()
        },
        language_code=language_code
    )
    sitemaps[u'flatpages_%s' % language_code] = Sitemap4Lang(
        {
            'queryset': ExtendFlatPage.objects.filter(registration_required=False)
        },
        language_code=language_code
    )
    sitemaps[u'static_%s' % language_code] = StaticViewSitemap(
        language_code=language_code
    )