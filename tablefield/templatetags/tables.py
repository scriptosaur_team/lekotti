from django import template
from ..models import Table
import json

register = template.Library()


@register.inclusion_tag('tablefield/table.html')
def table(table_slug, variant=""):
    """
    Usage:
        {% table "table_slug" %}
        {% table "table_slug" "collapsible" %}
        {% table "table_slug" "collapsed" %}
    """
    table_slug = str(table_slug)
    variant = str(variant)
    try:
        tbl = Table.objects.filter(code=table_slug)[0]
        tbl.data = json.loads(tbl.table_data)
    except:
        tbl = None
    return {'data': tbl, 'variant': variant}