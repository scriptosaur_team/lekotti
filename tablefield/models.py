from django.db import models
from .fields import TableField
from django.utils.translation import ugettext_lazy as _


class TableGroup(models.Model):
    name = models.CharField(_('name'), max_length=256)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        ordering = ['name']
        verbose_name = _('table group')
        verbose_name_plural = _('table groups')


class Table(models.Model):
    name = models.CharField(_('name'), max_length=256, blank=True, null=True,
                            help_text=_('Table title for presentational purposes.'))
    group = models.ForeignKey(TableGroup, verbose_name=_('group'), null=True, blank=True)
    title = models.CharField(_('title'), max_length=256, blank=True, null=True)
    code = models.SlugField(_('slug'), max_length=32, help_text=_('for use in templates'), unique=True)
    ordering = models.IntegerField(_('ordering'), default=100)
    table_data = TableField(_('table data'),
                            help_text=_('Click on cell to edit, right-click to ad or remove columns and rows.<br>'
                                        'First row contains table header'))
    footnote = models.CharField(_('footnote'), max_length=256, blank=True, null=True)

    def __unicode__(self):
        return u'%s [%s]' % (self.name or self.title or '', self.code)

    class Meta:
        verbose_name = _('table')
        verbose_name_plural = _('tables')
        ordering = ['name']