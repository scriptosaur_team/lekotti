from django.contrib import admin
from main.utils import TranslationTabMixin
from .models import *


class TableAdmin(TranslationTabMixin):
    list_filter = ['group']


admin.site.register(Table, TableAdmin)
admin.site.register(TableGroup)