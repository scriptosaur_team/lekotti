$(function(){
    $('.handsontable-widget').each(function(){
        var $widget = $(this),
            $dataInput = $widget.find('.handsontable-result'),
            $tablePlace = $widget.find('.handsontable-place'),
            data

        try {
            data = $.parseJSON($dataInput.val())
        } catch (e){
            data = [
                ["Column 1", "Column 2"],
                ["Data 1", "Data 2"]
            ]
        }
        function DumpData(){
            $dataInput.val(JSON.stringify($tablePlace.handsontable('getData')))
        }

        $tablePlace.handsontable({
            data: data,
            minSpareRows: 0,
            minSpareCols: 0,
            contextMenu: true,
            afterChange: DumpData,
            afterRemoveRow: DumpData,
            afterCreateRow: DumpData,
            afterRemoveCol: DumpData,
            afterCreateCol: DumpData,
            afterColumnMove: DumpData
        })
    })
})