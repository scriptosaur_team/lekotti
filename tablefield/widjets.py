from django.conf import settings
from django.forms import widgets
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django.forms.util import flatatt
from django.template.loader import render_to_string
from django.utils.html import conditional_escape


class TableWidget(widgets.HiddenInput):
    class Media:
        def __init__(self):
            pass

        css = {'all': (
            settings.STATIC_URL + 'tablefield/css/style.css',
            settings.STATIC_URL + 'tablefield/handsontable/jquery.handsontable.full.css',
        ), }
        js = (
            'http://code.jquery.com/jquery-1.9.1.js',
            settings.STATIC_URL + 'tablefield/handsontable/jquery.handsontable.full.js',
            settings.STATIC_URL + 'tablefield/js/init.js',
        )

    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            final_attrs['value'] = force_unicode(self._format_value(value))
        return mark_safe(render_to_string('tablefield/table_widget.html', {
            'final_attrs': flatatt(final_attrs),
            'value': conditional_escape(force_unicode(value)),
            'id': final_attrs['id']
        }))