from django.db import models
from django import forms
from .widjets import TableWidget


class TableField(models.TextField):
    def __init__(self, *args, **kwargs):
        super(TableField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {
            'form_class': TableFormField,
        }
        defaults.update(kwargs)
        return super(TableField, self).formfield(**defaults)


class TableFormField(forms.fields.CharField):
    def __init__(self, *args, **kwargs):
        kwargs.update({'widget': TableWidget()})
        super(TableFormField, self).__init__(*args, **kwargs)


try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^tablefield\.fields\.TableField"])
except ImportError:
    pass
