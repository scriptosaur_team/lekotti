from modeltranslation.translator import translator, TranslationOptions
from .models import *


class TableTranslationOptions(TranslationOptions):
    fields = ('name', 'title', 'table_data', 'footnote')


translator.register(Table, TableTranslationOptions)