# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Table'
        db.create_table(u'tablefield_table', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_fi', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('title_fi', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.SlugField')(max_length=32)),
            ('table_data', self.gf('tablefield.fields.TableField')()),
            ('table_data_ru', self.gf('tablefield.fields.TableField')(null=True, blank=True)),
            ('table_data_fi', self.gf('tablefield.fields.TableField')(null=True, blank=True)),
            ('table_data_en', self.gf('tablefield.fields.TableField')(null=True, blank=True)),
            ('footnote', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('footnote_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('footnote_fi', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('footnote_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal(u'tablefield', ['Table'])


    def backwards(self, orm):
        # Deleting model 'Table'
        db.delete_table(u'tablefield_table')


    models = {
        u'tablefield.table': {
            'Meta': {'ordering': "['name']", 'object_name': 'Table'},
            'code': ('django.db.models.fields.SlugField', [], {'max_length': '32'}),
            'footnote': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'footnote_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'footnote_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'footnote_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'table_data': ('tablefield.fields.TableField', [], {}),
            'table_data_en': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_fi': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_ru': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['tablefield']