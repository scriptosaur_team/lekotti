from django.contrib.flatpages.views import flatpage
from django.views.generic import TemplateView
from django.http import Http404, HttpResponse
from django.conf import settings
from .views import extend_flatpage
from .utils import get_season


class ExtendFlatpageFallbackMiddleware(object):
    def process_response(self, request, response):
        if response.status_code != 404:
            return response  # No need to check for a flatpage for non-404 responses.
        try:
            return extend_flatpage(request, request.path_info)
        # Return the original response if any errors happened. Because this
        # is a middleware, we can't assume the errors will be caught elsewhere.
        except Http404:
            return response
        except:
            if settings.DEBUG:
                raise
            return response
