from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField, get_thumbnail
from sorl.thumbnail.helpers import ThumbnailError
from django.contrib.flatpages.models import FlatPage
from django.contrib.auth.models import User
from django.conf import settings
import datetime
from datetime import date
from mptt.models import MPTTModel, TreeForeignKey
from django_resized import ResizedImageField
from allauth.socialaccount.models import SocialAccount
from crequest.middleware import CrequestMiddleware
# from .utils import get_season
from siteoptions.models import Season


class ExtendFlatPage(FlatPage):
    show_map = models.BooleanField(_('show map'), default=True)
    show_gallery_in_popup = models.BooleanField(_('show gallery in popup'), default=False)

    class Meta:
        verbose_name = _('flatpage')
        verbose_name_plural = _('flatpages')


class FlatPageImage(models.Model):
    image = ResizedImageField(max_width=1000, upload_to='images', verbose_name=_('image'))
    ordering = models.IntegerField(_('ordering'), default=100)
    page = models.ForeignKey(ExtendFlatPage)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('photo gallery')
        verbose_name_plural = _('photo gallery')


class BannerIcon(models.Model):
    name = models.CharField(_('icon name'), max_length=256)
    image = ImageField(_('icon'), upload_to='banner/icons', blank=True, null=True)
    image_hover = ImageField(_('icon hover'), upload_to='banner/icons', blank=True, null=True)
    ordering = models.IntegerField(_('ordering'), default=100)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        ordering = ['ordering']
        verbose_name = _('banner icon')
        verbose_name_plural = _('banner icons')

    def thumbnail(self):
        try:
            img_resize_url = unicode(get_thumbnail(self.image, '32x32', format="PNG").url)
            return '<img src="%s" alt="%s">' % (img_resize_url, self.name)
        except ThumbnailError:
            return "<span>no file</span>"
    thumbnail.allow_tags = True


class Banner(models.Model):
    name = models.CharField(_('name'), max_length=256)
    banner_icon = models.ForeignKey(BannerIcon, blank=True, null=True, related_name='icon')
    ordering = models.IntegerField(_('ordering'), default=100)
    winter = models.BooleanField(_('active in winter'), default=True)
    summer = models.BooleanField(_('active in summer'), default=True)
    title = models.TextField(_('banner title'), blank=True, null=True)
    button_title = models.CharField(_('button title'), max_length=32, blank=True, null=True)
    description = models.TextField(_('description'), blank=True, null=True)
    link = models.CharField(_('link'), blank=True, null=True, max_length=256)
#     background = ImageField(_('background'), upload_to='banner')
    background = ResizedImageField(max_width=1600, upload_to='banner', verbose_name=_('background'))

    class Meta:
        ordering = ['ordering']
        verbose_name = _('index page banner')
        verbose_name_plural = _('index page banners')

    def __unicode__(self):
        return u'%s' % self.name

    # def thumbnail(self):
    #     try:
    #         img_resize_url = unicode(get_thumbnail(self.icon, '32x32').url)
    #         return '<img src="%s" alt="%s">' % (img_resize_url, self.name)
    #     except ThumbnailError:
    #         return "<span>no file</span>"
    # thumbnail.allow_tags = True


class InnerBanner(models.Model):
#     image = ImageField(_('image'), upload_to='banner')
    image = ResizedImageField(max_width=1600, upload_to='banner', verbose_name=_('image'))
    ordering = models.IntegerField(_('ordering'), default=100)
    winter = models.BooleanField(_('active in winter'), default=True)
    summer = models.BooleanField(_('active in summer'), default=True)

    def __unicode__(self):
        return self.get_thumbnail_html()

    def get_thumbnail_html(self):
        try:
            return '<img src="%s">' % unicode(get_thumbnail(self.image, '470x80').url)
        except ThumbnailError:
            return "<span>no file</span>"
    get_thumbnail_html.allow_tags = True

    class Meta:
        ordering = ['ordering']
        verbose_name = _('inner banner')
        verbose_name_plural = _('inner banners')


class Comment(MPTTModel):
    author = models.ForeignKey(User, verbose_name=_('author'), blank=True, null=True)
    add_time = models.DateTimeField(_('add time'), default=datetime.datetime.now(), blank=True, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    message = models.TextField(_('message'))

    def __unicode__(self):
        return u'%s' % self.message
        
    def get_author_profile_id(self):
        fb_uid = SocialAccount.objects.filter(user_id=self.author.id, provider='facebook')
        try:
            return fb_uid[0].uid
        except:
            return False

    def get_avatar(self):
        try:
            user_social_account = SocialAccount.objects.get(user_id=self.author.id)
            user_provider = user_social_account.get_provider().id
            if user_provider == 'vk':
                try:
                    avatar = user_social_account.extra_data['photo_medium']
                except:
                    avatar = None
            else:
                avatar = user_social_account.get_avatar_url()
            return avatar
        except:
            pass
        return None

    def get_profile_url(self):
        try:
            user_social_account = SocialAccount.objects.get(user_id=self.author.id)
            user_provider = user_social_account.get_provider().id
            if user_provider == 'vk':
                print(user_social_account.extra_data)
                try:
                    profile_url = u'http://vk.com/%s' % user_social_account.extra_data['screen_name']
                except:
                    profile_url = None
            else:
                profile_url = user_social_account.get_profile_url()
            return profile_url
        except:
            pass
        return None

    def get_provider(self):
        try:
            user_social_account = SocialAccount.objects.get(user_id=self.author.id)
            user_provider = user_social_account.get_provider().id
        except:
            user_provider = None
        return user_provider

    def get_author(self):
        try:
            user_social_account = SocialAccount.objects.get(user_id=self.author.id)
            user_provider = user_social_account.get_provider().id
            if user_provider == 'vk':
                try:
                    user_real_names = user_social_account.extra_data['first_name'],\
                                      user_social_account.extra_data['last_name']
                    if user_real_names:
                        return ' '.join(user_real_names)
                    print(user_social_account.extra_data['screen_name'])
                except:
                    pass
        except:
            pass
        return self.author.get_full_name() or self.author.username

    class Meta:
        abstract = True
        ordering = ['add_time']

    class MPTTMeta:
        order_insertion_by = ['add_time']


class Language(models.Model):
    LANG_CODE_CHOICES = [(lang_code, lang_code) for lang_code, lang_name in settings.LANGUAGES]
    name = models.CharField(_('name'), max_length=64, help_text=_('language name in English'))
    code = models.CharField(_('code'), max_length=2,
                            help_text=_('two letter language code, e.g. ru, fi. '
                                        '<a href="http://derevyanko.blogspot.ru/2012/01/iso-639-1.html">'
                                        'Full list of language codes</a>'))
    label = models.CharField(_('label'), max_length=4, help_text=_('for language selector widget'),
                             null=True, blank=True)
    ordering = models.IntegerField(_('ordering'), default=100)

    def __unicode__(self):
        return u'%s [%s]' % (self.name, self.code)

    def get_label(self):
        return u'%s' % self.label if self.label else self.code

    class Meta:
        ordering = ['ordering']
        verbose_name = _('language')
        verbose_name_plural = _('languages')


class PageComment(Comment):
    page = models.ForeignKey(ExtendFlatPage, verbose_name=_('page'))

    class Meta(Comment.Meta):
        verbose_name = _('page comment')
        verbose_name_plural = _('page comments')


class RobotsTXTFile(models.Model):
    url = models.CharField(_('hostname'), max_length=64)
    content = models.TextField(_('robots.txt content'))

    def __unicode__(self):
        return u'%s' % self.url

    class Meta:
        verbose_name = _('robots.txt file')
        verbose_name_plural = _('robots.txt files')


class SEOFields(models.Model):
    title_winter = models.CharField(_('browser title'), max_length=256, null=True, blank=True)
    meta_description_winter = models.TextField(_('meta description'), max_length=256, null=True, blank=True)
    meta_keywords_winter = models.TextField(_('meta keywords'), max_length=256, null=True, blank=True)
    title_summer = models.CharField(_('browser title'), max_length=256, null=True, blank=True)
    meta_description_summer = models.TextField(_('meta description'), max_length=256, null=True, blank=True)
    meta_keywords_summer = models.TextField(_('meta keywords'), max_length=256, null=True, blank=True)

    def current_season(self):
        request = CrequestMiddleware.get_request()
        season = request.COOKIES.get('season')
        if not season:
            today = date.today()
            summer_start_day, summer_start_month = Season.objects.get(code='summer').date_begin.split('.')
            winter_start_day, winter_start_month = Season.objects.get(code='winter').date_begin.split('.')
            try:
                summer_start = date(today.year, int(summer_start_month), int(summer_start_day))
            except ValueError:
                summer_start = date(today.year, 4, 15)
            try:
                summer_end = date(today.year, int(winter_start_month), int(winter_start_day))
            except ValueError:
                summer_end = date(today.year, 10, 15)
            if summer_start < today < summer_end:
                season = 'summer'
            else:
                season = 'winter'
        return season

    def title(self):
        return self.title_summer if self.current_season() == 'summer' else self.title_winter

    def meta_description(self):
        return self.meta_description_summer if self.current_season() == 'summer' else self.meta_description_winter

    def meta_keywords(self):
        return self.meta_keywords_summer if self.current_season() == 'summer' else self.meta_keywords_winter

    class Meta:
        abstract = True