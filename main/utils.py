from datetime import date
from modeltranslation.admin import TranslationAdmin
from siteoptions.models import Season


def get_season(request):
    season = request.COOKIES.get('season')
    if not season:
        today = date.today()
        summer_start_day, summer_start_month = Season.objects.get(code='summer').date_begin.split('.')
        winter_start_day, winter_start_month = Season.objects.get(code='winter').date_begin.split('.')
        try:
            summer_start = date(today.year, int(summer_start_month), int(summer_start_day))
        except ValueError:
            summer_start = date(today.year, 4, 15)
        try:
            summer_end = date(today.year, int(winter_start_month), int(winter_start_day))
        except ValueError:
            summer_end = date(today.year, 10, 15)
        if summer_start < today < summer_end:
            season = 'summer'
        else:
            season = 'winter'
    return season


class TranslationTabMixin(TranslationAdmin):

    class Media:
        def __init__(self):
            pass

        js = (
            'modeltranslation/js/force_jquery.js',
            '/static/js/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }