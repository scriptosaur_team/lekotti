$(document).ready(function() {

function timeConverter(UNIX_timestamp){
 var a = new Date(UNIX_timestamp*1000);
 var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
     var year = a.getFullYear();
     var month = months[a.getMonth()];
     var date = a.getDate();
     var hour = a.getHours();
     var min = a.getMinutes();
     var sec = a.getSeconds();
     var time = date+','+month+' '+year+' '+hour+':'+min+':'+sec ;
     return time;
 }
    var messageMaxLength = 200
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_UK/all.js', function(){
        var pageAccessToken = '760259827321136|e3df330624baa16f91505630d2a74ac7';
        FB.api(
            '/Lekotti/feed?fields=from,message,link,picture&date_format=U&limit=3',
            {access_token : pageAccessToken},
            function(response){
                var fb_messages = response.data;
                if(fb_messages){
                    for(var i in fb_messages){
                        var postDate = new Date(fb_messages[i].created_time),
                        	
                            dateFormatted = timeConverter(postDate),
                            fb_message = fb_messages[i].message,
                            fb_picture = fb_messages[i].picture,
                            user = fb_messages[i].from,
                            fb_link = fb_messages[i].link
                            
                        if(fb_message){
                            if(fb_message.length > messageMaxLength){
                                var trimmedString = fb_message.substr(0, messageMaxLength)
                                fb_message = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + ' …'
                            }
                            fb_message = "<p>" + fb_message + "</p>"
                        } else {
                            fb_message = ''
                        }
                        if(fb_picture){
                            fb_picture = "<img class='img-responsive fb-mess-picture' src='" + fb_picture + "'>"
                        } else {
                            fb_picture = ''
                        }
                        console.log(postDate);
                        console.log(fb_link);
                        var mess =
                            "<div class='fb-message-wrap media'>" +
                                "<img src='https://graph.facebook.com/"+ user.id +"/picture' class='fb-avatar pull-left media-object'>" +
                                "<div class='media-body'>" +
                                	"<a href='1"+ fb_link +"'>"+
                                    "<p>" + user.name + "</p>" + 
                                    fb_message +
                                    fb_picture +
                                    "<time>" + dateFormatted + "</time>" +
                                    "</a>" +
                                "</div>" +
                            "</div>"
                        $("#fb-place").append($(mess));
                    }
                }
            }
        );
    });
})