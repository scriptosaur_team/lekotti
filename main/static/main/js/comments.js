$(function(){
    function CheckAuth(){
        $('.fancybox-inner form').ajaxForm({
            error: function(data){
                $('.fancybox-inner').html(data.responseJSON.html).height('auto')
                CheckAuth()
            },
            success: function(data){
                $('#comments').removeClass('not-auth')
                $.fancybox.close()
            }
        })
    }
    function CommentFormsHandle($forms){
        $forms.ajaxForm({
            beforeSubmit: function(arr, $form, options){
                if($("#comments").hasClass("not-auth")){
                    var $thisForm = $form
                    $.fancybox({
                        href: $("#auth-link").prop("href")||"/accounts/login/",
                        type: "ajax",
                        minWidth: 600,
                        transitionIn  : 'elastic',
                        transitionOut : 'elastic',
                        scrolling : 'no',
                        preload   : true,
                        autoDimensions : false,
                        padding: [50,64,60,64],
                        helpers: {
                            overlay: {
                                locked: false,
                                css : { 'overflow' : 'no' }
                            }
                        },
                        afterShow: function(){
/*
                            $('.fancybox-inner').on('click', 'a', function(e){
                                e.preventDefault()
                                var url = $(this).prop('href')
                                $('.fancybox-inner')
                                    .addClass('load')
                                    .load(url, function(){
                                        $('.fancybox-inner').removeClass('load')
                                        CheckAuth()
                                    })
                            })
                            CheckAuth()
*/
                        },
                        afterClose: function(){
                            if(!$('#comments').hasClass('not-auth')){
                                $thisForm.find('[name=csrfmiddlewaretoken]').val($.cookie('csrftoken'))
                                $thisForm.find('[type=submit]').click()
                            }
                        }
                    })
                    return false
                }
            },
            success: function(responseText, statusText, xhr, $form){
                var $commentCreated = $(responseText)
                var cID = parseInt($form.find('[name=parent]').val())
                var $aComment
                $form.find('textarea').val('')
                if(cID>0){
                    $aComment = $('#comments ul.comments [data-id='+cID+']')
                    $form.hide()
                }else{
                    $aComment = $('#comments ul.comments li').last()
                }
                $commentCreated
                    .css({opacity:0})
                    .insertAfter($aComment)
                    .animate({opacity: 1}, {duration:400})
                CommentFormsHandle($commentCreated.find('form'))
            }
        })
    }
    CommentFormsHandle($("#comments form"))
    $('#comments').on('click', '.reply', function(e){
        e.preventDefault();
        var $commentInner = $(this).parent().siblings(".comment_inner")
        $commentInner.stop().slideToggle(function(){
            if($commentInner.is(':visible')) $commentInner.find('textarea').focus()
        });
    });
    $('#auth-link').click(function(e){
        e.preventDefault()
        $.fancybox({
            href: $("#auth-link").prop("href")||"/accounts/login/",
            type: "ajax",
            minWidth: 600,
            transitionIn  : 'elastic',
            transitionOut : 'elastic',
            scrolling : 'no',
            preload   : true,
            autoDimensions : false,
            padding: [50,64,60,64],
            helpers: {
                overlay: {
                    locked: false,
                    css : { 'overflow' : 'no' }
                }
            },
            afterShow: function(){
                $('.fancybox-inner').on('click', 'a', function(e){
                    e.preventDefault()
                    var url = $(this).prop('href')
                    $('.fancybox-inner')
                        .addClass('load')
                        .load(url, function(){
                            $('.fancybox-inner').removeClass('load')
                            CheckAuth()
                        })
                })
                CheckAuth()
            },
            afterClose: function(){
                window.location.reload()
            }
        })

    })
})