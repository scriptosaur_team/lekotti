# import object_tools
# from django.core import management
# from django.utils.translation import ugettext_lazy as _
# from django.shortcuts import redirect
# from django.conf import settings
#
#
# apps = settings.TRANSLATABLE_APPS
#
#
# class Schemamigration(object_tools.ObjectTool):
#     name = 'schemamigration'
#     label = _('Schemamigration')
#
#     def view(self, request, extra_context=None):
#         for app in apps:
#             try:
#                 management.call_command('schemamigration', app, auto=True)
#             except:
#                 pass
#         return redirect(request.META.get('HTTP_REFERER', 'index'))
#
# object_tools.tools.register(Schemamigration)
#
#
# class Migrate(object_tools.ObjectTool):
#     name = 'migrate'
#     label = _('Migrate')
#
#     def view(self, request, extra_context=None):
#         for app in apps:
#             try:
#                 management.call_command('migrate', app)
#             except:
#                 pass
#         return redirect(request.META.get('HTTP_REFERER', 'index'))
#
# object_tools.tools.register(Migrate)