# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ExtendFlatPage'
        db.create_table(u'main_extendflatpage', (
            (u'flatpage_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['flatpages.FlatPage'], unique=True, primary_key=True)),
            ('title_fi', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('content_fi', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('content_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('content_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('show_map', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('show_gallery_in_popup', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'main', ['ExtendFlatPage'])

        # Adding model 'FlatPageImage'
        db.create_table(u'main_flatpageimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.ExtendFlatPage'])),
        ))
        db.send_create_signal(u'main', ['FlatPageImage'])

        # Adding model 'Banner'
        db.create_table(u'main_banner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_fi', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('icon', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('icon_hover', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('winter', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('summer', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('title', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('title_fi', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('title_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description_fi', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('description_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('background', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'main', ['Banner'])

        # Adding model 'InnerBanner'
        db.create_table(u'main_innerbanner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('winter', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('summer', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'main', ['InnerBanner'])

        # Adding model 'Language'
        db.create_table(u'main_language', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal(u'main', ['Language'])

        # Adding model 'PageComment'
        db.create_table(u'main_pagecomment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('add_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 1, 17, 0, 0), null=True, blank=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.ExtendFlatPage'])),
        ))
        db.send_create_signal(u'main', ['PageComment'])


    def backwards(self, orm):
        # Deleting model 'ExtendFlatPage'
        db.delete_table(u'main_extendflatpage')

        # Deleting model 'FlatPageImage'
        db.delete_table(u'main_flatpageimage')

        # Deleting model 'Banner'
        db.delete_table(u'main_banner')

        # Deleting model 'InnerBanner'
        db.delete_table(u'main_innerbanner')

        # Deleting model 'Language'
        db.delete_table(u'main_language')

        # Deleting model 'PageComment'
        db.delete_table(u'main_pagecomment')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'flatpages.flatpage': {
            'Meta': {'ordering': "(u'url',)", 'object_name': 'FlatPage', 'db_table': "u'django_flatpage'"},
            'content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'enable_comments': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'registration_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sites': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['sites.Site']", 'symmetrical': 'False'}),
            'template_name': ('django.db.models.fields.CharField', [], {'max_length': '70', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'})
        },
        u'main.banner': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Banner'},
            'background': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'icon': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'icon_hover': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'summer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'winter': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'main.extendflatpage': {
            'Meta': {'ordering': "(u'url',)", 'object_name': 'ExtendFlatPage', '_ormbases': [u'flatpages.FlatPage']},
            'content_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'flatpage_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['flatpages.FlatPage']", 'unique': 'True', 'primary_key': 'True'}),
            'show_gallery_in_popup': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_map': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_fi': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.flatpageimage': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'FlatPageImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.ExtendFlatPage']"})
        },
        u'main.innerbanner': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'InnerBanner'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'summer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'winter': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'main.language': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Language'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'main.pagecomment': {
            'Meta': {'ordering': "['add_time']", 'object_name': 'PageComment'},
            'add_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 1, 17, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.ExtendFlatPage']"})
        },
        u'sites.site': {
            'Meta': {'ordering': "(u'domain',)", 'object_name': 'Site', 'db_table': "u'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['main']