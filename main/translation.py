from modeltranslation.translator import translator, TranslationOptions
from .models import *


class ExtendFlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


class BannerTranslationOptions(TranslationOptions):
    fields = ('name', 'title', 'description', 'button_title',)


class SEOFieldsTranslationOptions(TranslationOptions):
    fields = (
        'title_winter', 'meta_description_winter', 'meta_keywords_winter',
        'title_summer', 'meta_description_summer', 'meta_keywords_summer',
    )


translator.register(ExtendFlatPage, ExtendFlatPageTranslationOptions)
translator.register(Banner, BannerTranslationOptions)
translator.register(SEOFields, SEOFieldsTranslationOptions)