from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.utils import translation
from django.utils.safestring import mark_safe
from django.template import loader, RequestContext
from django.views.decorators.csrf import csrf_protect
from django.contrib.sites.models import get_current_site
from django.http import Http404, HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404
from .forms import CommentForm
from django.contrib.auth import logout
from urlparse import urlparse
import re

from .models import *


def lang(request, code):
    next_page = request.META.get('HTTP_REFERER', '/')
    next_page_parts = urlparse(next_page)
    new_path = re.sub('^/(\w{2})/', '/%s/' % code, next_page_parts.path)
    if next_page_parts.query:
        new_path += '?%s' % next_page_parts.query
    response = HttpResponseRedirect(new_path)
    if code and translation.check_for_language(code):
        # print(code)
        # response.set_cookie(settings.LANGUAGE_COOKIE_NAME, code, max_age=365*24*60*60)
        translation.activate(code)
    return response


DEFAULT_TEMPLATE = 'flatpages/default.html'


def extend_flatpage(request, url):
    """
    Public interface to the flat page view.

    Models: `flatpages.flatpages`
    Templates: Uses the template defined by the ``template_name`` field,
        or :template:`flatpages/default.html` if template_name is not defined.
    Context:
        flatpage
            `flatpages.flatpages` object
    """
    if not url.startswith('/'):
        url = '/' + url
    site_id = get_current_site(request).id
    try:
        f = get_object_or_404(ExtendFlatPage,
                              url__exact=url, sites__id__exact=site_id)
    except Http404:
        if not url.endswith('/') and settings.APPEND_SLASH:
            url += '/'
            f = get_object_or_404(ExtendFlatPage,
                                  url__exact=url, sites__id__exact=site_id)
            return HttpResponsePermanentRedirect('%s/' % request.path)
        else:
            raise
    return render_flatpage(request, f)


@csrf_protect
def render_flatpage(request, fp):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.page = fp
            f.add_time = datetime.datetime.now()
            f.author = request.user
            f.save()
            if request.is_ajax():
                a_form = CommentForm()
                return render_to_response('main/comment_created.html', {'comment': f, 'form': a_form},
                                          context_instance=RequestContext(request))
            else:
                return HttpResponseRedirect(request.path)
    else:
        form = CommentForm()
    if fp.registration_required and not request.user.is_authenticated():
        from django.contrib.auth.views import redirect_to_login
        return redirect_to_login(request.path)
    if fp.template_name:
        t = loader.select_template((fp.template_name, DEFAULT_TEMPLATE))
    else:
        t = loader.get_template(DEFAULT_TEMPLATE)

    fp.title = mark_safe(fp.title)
    fp.content = mark_safe(fp.content)
    fp.comments = PageComment.objects.all()
    fp.form = form

    c = RequestContext(request, {
        'flatpage': fp,
    })
    response = HttpResponse(t.render(c))
    return response


def login_success(request):
    return render_to_response('login_success.html', {},  context_instance=RequestContext(request))


def user_logout(request):
    logout(request)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def robots(request):
    url = request.META['SERVER_NAME']
    try:
        robots_content = RobotsTXTFile.objects.get(url=url).content
    except RobotsTXTFile.DoesNotExist:
        robots_content = ''
    return HttpResponse(robots_content, content_type="text/plain")