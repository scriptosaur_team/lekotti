from django import template
from ..models import Banner
register = template.Library()
from ..utils import get_season


@register.inclusion_tag('main/banner.html', takes_context=True)
def banner(context):
    request = context['request']
    season = get_season(request)
    slides = Banner.objects.filter(**{season: True})
    return {'slides': slides}
