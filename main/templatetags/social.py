import twitter
import datetime
from django import template
register = template.Library()


@register.inclusion_tag('main/twitter_feed.html', takes_context=True)
def twitter_feed(context):
    api = twitter.Api(consumer_key='9X3iPdCQjG79XkJJHiEkA',
                      consumer_secret='B33eqJDgFjMbZHgVhS6LJEobZWWdPV7jcp1dF8Afu2s',
                      access_token_key='37550995-4zavidA5NPCmsPgMHDA8MHLC1F2Aas3eIIrYTE2lU',
                      access_token_secret='tsgHJgDOh33hHHmlfHPqn9vVrzJpo2um1R9ar59EiF4nz')

    def create_post(p):
        p.date = datetime.datetime.fromtimestamp(int(p.created_at_in_seconds))
        return p
    posts = [create_post(post) for post in api.GetUserTimeline(screen_name='Lekottifi')[:3]]

    return {'posts': posts}