from django import template
register = template.Library()
import json
from django.utils.translation import get_language


@register.tag
def google_map(parser, token):
    """
    tag parameters:
        place: {
            map: 'lat,lng',
            name: 'string',
            phone: 'string',
            address: 'string',
            link: 'string'
        }
    or:
        places: list of places such as above

    """
    tokens = token.split_contents()
    return GoogleMapNode(tokens)


class GoogleMapNode(template.Node):
    def __init__(self, params):
        self.params = params
        if len(self.params) == 3:
            if params[1] == 'places':
                self.places = template.Variable(params[2])
            elif params[1] == 'place':
                self.place = template.Variable(params[2])

    def render(self, context):
        ctx = template.Context(context)
        request = context['request']
        ctx.update({'show_contact_link': False if request.path == '/contact/' else True})
        places = []
        try:
            if self.places:
                places = self.places.resolve(context)
        except AttributeError:
            try:
                if self.place:
                    places = [self.place.resolve(context)]
            except AttributeError:
                pass
        if places:
            def get_coords(map_field):
                coords = map_field.split(',')
                if len(coords) == 2:
                    try:
                        return [float(coords[0]), float(coords[1])]
                    except:
                        return None
                return None
            places_data = {p.id: {
                'name': getattr(p, 'name', ''),
                'phone': getattr(p, 'phone', ''),
                'address': getattr(p, 'address', ''),
                'coords': get_coords(p.map),
                'link': p.get_absolute_url(),
            } for p in places}
            ctx.update({'places_json': json.dumps(places_data)})
            ctx.update({'show_contact_link': False})
            ctx.update({'LANGUAGE_CODE': get_language()})
        tpl = template.loader.get_template("main/google_map.html")
        return tpl.render(ctx)