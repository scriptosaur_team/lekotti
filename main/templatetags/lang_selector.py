from django import template
register = template.Library()
from django.utils.translation import get_language
from django.conf import settings
from ..models import Language
import re


@register.inclusion_tag('main/lang_selector.html', takes_context=True)
def lang_selector(context):
    path = context['request'].path

    def build_lang_link(l):
        l.link = re.sub('^/(\w{2})/', '/%s/' % l.code, path)
        return l
    languages = [build_lang_link(lang) for lang in Language.objects.all()]
    return {
        'languages': languages,
        'LANGUAGE_CODE': get_language()
    }


@register.simple_tag()
def lang_link(link):
    if link[:4] == 'http':
        return link
    else:
        return u'/%s%s' % (get_language(), re.sub('^/(\w{2})/', '/', link))