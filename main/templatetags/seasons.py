from django import template
from ..utils import get_season
register = template.Library()


@register.inclusion_tag('main/season_switch.html', takes_context=True)
def season_switch(context):
    season = get_season(context['request'])
    return {'season_active': season}
