from django.contrib import admin
from .utils import TranslationTabMixin
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from .models import *
from django import forms
from cked.widgets import CKEditorWidget
from django.utils.translation import ugettext_lazy as _


class ExtendFlatPageForm(FlatpageForm):
    content_ru = forms.CharField(widget=CKEditorWidget())
    content_en = forms.CharField(widget=CKEditorWidget())
    content_fi = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = ExtendFlatPage


class ExtendFlatPageImageInline(admin.TabularInline):
    model = FlatPageImage


class ExtendFlatPageAdmin(FlatPageAdmin, TranslationTabMixin):
    form = ExtendFlatPageForm
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'show_map', 'show_gallery_in_popup', 'sites')}),
        (_('Advanced options'), {'classes': ('collapse',),
                                 'fields': ('enable_comments', 'registration_required', 'template_name')}),
    )

    inlines = [ExtendFlatPageImageInline]


admin.site.unregister(FlatPage)
admin.site.register(ExtendFlatPage, ExtendFlatPageAdmin)


class BannerAdmin(TranslationTabMixin):
    list_display = ['name', 'link', 'ordering', 'summer', 'winter', 'button_title']
    list_display_links = ['name']
    list_editable = ['link', 'ordering', 'summer', 'winter', 'button_title']


admin.site.register(Banner, BannerAdmin)


class InnerBannerAdmin(admin.ModelAdmin):
    list_display = ['get_thumbnail_html', 'ordering']
    list_editable = ['ordering']


admin.site.register(InnerBanner, InnerBannerAdmin)


class LanguageAdmin(admin.ModelAdmin):
    list_display = ['name', 'label', 'code', 'ordering']
    list_editable = ['ordering']


admin.site.register(Language, LanguageAdmin)


class PageCommentAdmin(admin.ModelAdmin):
    list_display = ['author', 'add_time', 'message', 'page']
    list_filter = ['page']
    ordering = ['-add_time']


class BannerIconAdmin(admin.ModelAdmin):
    list_display = ['thumbnail', 'name', 'ordering']
    list_display_links = ['thumbnail', 'name']
    list_editable = ['ordering']


admin.site.register(PageComment, PageCommentAdmin)

admin.site.register(RobotsTXTFile)

admin.site.register(BannerIcon, BannerIconAdmin)