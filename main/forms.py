from django import forms
from .models import PageComment


class CommentForm(forms.ModelForm):
    class Meta:
        model = PageComment
        fields = ['message', 'parent']
        widgets = {
            'parent': forms.HiddenInput(),
        }