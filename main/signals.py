from django.db.models import signals
from django.conf import settings
import os.path
from .models import Language


def lang_config():
    settings_dir = settings.SETTINGS_DIR
    lang_settings_file = os.path.join(settings_dir, 'lang.py')
    languages = Language.objects.all()
    with open(lang_settings_file, 'w') as handle:
        handle.write('# coding: utf-8\ngettext = lambda s: s\nLANGUAGES = (\n')
        for lang in languages:
            ustr = u'    (\'%s\', gettext(\'%s\')),\n' % (lang.code, lang.name)
            handle.write(ustr.encode('utf8'))
        handle.write(')\n')
    handle.close()


def lang_created(sender, instance, created, **kwargs):
    return lang_config()


def lang_deleted(sender, instance, **kwargs):
    return lang_config()


signals.post_save.connect(lang_created, sender=Language)
signals.post_delete.connect(lang_deleted, sender=Language)