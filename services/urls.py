from django.conf.urls import patterns, url

urlpatterns = patterns('',
                       url(r'^$', 'services.views.home', name='home'),
                       url(r'^ski/$', 'services.views.ski', name='ski'),
                       url(r'^ropes_course/$', 'services.views.ropes_course', name='ropes_course'),
                       url(r'^places/$', 'services.views.places', name='places'),
                       )