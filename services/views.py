from django.shortcuts import render_to_response
from django.template import RequestContext


def home(request):
    return render_to_response('services/home.html', {}, context_instance=RequestContext(request))


def ski(request):
    return render_to_response('services/ski.html', {}, context_instance=RequestContext(request))


def ropes_course(request):
    return render_to_response('services/ropes_course.html', {}, context_instance=RequestContext(request))


def places(request):
    return render_to_response('services/places.html', {}, context_instance=RequestContext(request))