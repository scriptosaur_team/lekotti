$(function(){
    $('.season_switch a').click(function(e){
        e.preventDefault();
        $.cookie('season', $(this).data('season'), { expires: 30, path: '/' });
        window.location.reload();
    });
    function SetGalleryButtonsWidth(){$('.photo-gallery .arrow').width(Math.ceil((parseInt($('.photo-gallery').width())-650)/2))}
    $('.photo-gallery .iosslider').each(function(){
        var $this = $(this),
            $prev = $this.siblings('.prev'),
            $next = $this.siblings('.next');
        $this.iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            infiniteSlider: true,
            snapSlideCenter: true,
            navNextSelector: $next,
            navPrevSelector: $prev,
            onSliderLoaded: SetGalleryButtonsWidth,
            onSliderResize: SetGalleryButtonsWidth
        });
    });
    function SetupPopupGallery(){
        $(".popup-gallery .iosslider").iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            infiniteSlider: true,
            navNextSelector: $(".popup-gallery .next"),
            navPrevSelector: $(".popup-gallery .prev")
        });
        $(".popup-gallery .show_photo").click(function(e){
            e.preventDefault();
            var imgUrl = $(this).attr('href'),
                $mainImage = $('.popup-gallery .main-image');
            $mainImage.addClass('load');
            $(new Image()).attr('src', imgUrl).load(function() {
                $mainImage
                    .removeClass('load')
                    .find('.image')
                    .css({backgroundImage: 'url('+imgUrl+')'});
            });
        });
        $('.fancybox-inner').on('click', '.other-gallery', function(e){
            e.preventDefault();
            var url = $(this).prop('href');
            $('.fancybox-inner')
                .addClass('load')
                .load(url, function(){
                    $('.fancybox-inner').removeClass('load');
                    SetupPopupGallery();
                });
            }
        )
        var slidesWidth = 0;
        $(".popup-gallery .slider .slide").each(function(){
            slidesWidth += parseInt($(this).outerWidth(true));
        });
        if(slidesWidth>$(".popup-gallery .iosslider").width()){
            $(".popup-gallery .arrow").fadeIn();
        }
    }
    $('.photo-gallery').on('click', '.show_gallery', function(e){
        e.preventDefault()
        var $this = $(this),
            galleryUrl = $this.prop('href');
            galleryUrl += galleryUrl.indexOf('?') > -1 ? '&': '?';
            galleryUrl += 'active=' + $this.closest('.slide').index();
        $.fancybox({
            href: galleryUrl,
            type: 'ajax',
            padding: [60,64,60,64],
            helpers: {
                overlay: {
                    locked: false
                }
            },
            afterShow: SetupPopupGallery,
            wrapCSS: 'fb-popup-gallery'
        })
    });
    $(".table-collapsible .price-list h4").click(function(){
        $(this).toggleClass("open").next(".table-toggle").slideToggle("slow")
    });

    if($(".events_for a.for_events").size() > 1){
        var $prev = $('.ios_prev'),
            $next = $('.ios_next');
        $(".ios_arrow").show();
        $(".events_for .iosslider").iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            infiniteSlider: true,
            snapSlideCenter: true,
            navNextSelector: $next,
            navPrevSelector: $prev,
            autoSlide: true
        });
    }
    var $banners = $('.banners'),
        $bannersImg = $banners.find('.item');
    if($bannersImg.size() > 1){
        $(".banners .iosslider").iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            infiniteSlider: true,
            snapSlideCenter: true,
            autoSlide: true,
            onSliderLoaded: function(){$(window).resize()}
        })
    }

    var loadWeather = function(){
        $.ajax({
            url : "http://api.wunderground.com/api/203472cccd81598b/geolookup/conditions/q/61.754931,29.624634.json",
            dataType : "jsonp",
            success : function(data) {
                var location = data['location']['city'],
                    co_data = data['current_observation'],
                    summary = {
                        temp_c: co_data['temp_c'],
                        icon: co_data['icon'],
                        wind_mps: Math.round(co_data['wind_kph']/3.6),
                        humidity: parseInt(co_data['relative_humidity'])
                    };
                $('#weather-icon').css({backgroundImage:'url(/static/images/weather/'+summary.icon+'.gif)'});
                $('#weather-temp').text(summary.temp_c);
                $('#weather-wind').text(summary.wind_mps);
                $('#weather-humidity').text(summary.humidity);
            }
        })
    };
    loadWeather();
    setInterval(loadWeather, 1000 * 60 * 60 * 6);


    $("a[data-toggle=tab]").on("shown.bs.tab", function (e) {
        var id = $(e.target).attr("href").substr(10);
        window.location.hash = id;
    });
    function SwitchPage(hash) {
        if(hash) {
            var $a = $('a[href="#tab-pane-' + hash + '"]'),
                $section;
            if($a.size()==1){
                $section = $a.closest('.section');
                $a.tab('show');
            } else {
                $section = $('#page-section-' + hash);
            }
            if($section.size()==1) {
                $('html,body').animate({'scrollTop': $section.offset().top});
            }
        }
    }
    SwitchPage(window.location.hash.substr(1));
    $('body').on(
        'click',
        'a[href*=#]',
        function(e){
            e.preventDefault();
            var hash = $(this).attr('href').split('#')[1];
            SwitchPage(hash);
            window.location.hash = hash;
        }
    )
});