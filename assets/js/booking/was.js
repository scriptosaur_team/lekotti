angular.module('booking',
    [
        'bookingControllers',
        'ui.bootstrap',
        'ui.select2'
    ]
);

var bookingControllers = angular.module('bookingControllers', []);

bookingControllers
    .controller('bookingController', ['$scope',
        function ($scope) {
            $scope.bData = eval($('#booking-data').html());

            $scope.clearResult = function(){
                $scope.result = {
                    'dates': {},
                    'places': {
                        'adult': 0,
                        'children': 0
                    },
                    'options': [],
                    'terms': parseInt(_.first(_.keys($scope.bData.payment_terms)))
                }
            };
            $scope.clearResult();
            /*
            $scope.result = {
                'accType': 1,
                'objectNumber': 1,
                'dates': {
                    'checkIn': date,
                    'checkOut': date
                },
                'places': {
                    'adult': 1,
                    'children': 2
                }
                'options': [1, 3],
                'terms': 1
            };
             */

            moment.lang($scope.bData.config.lang);
            $scope.initDate = moment();
            $scope.minDate = $scope.initDate;
            $scope.maxDate = moment().add('years', 1);
            $scope.dateFormats = ['dd.MM.yyyy', 'dd MMMM yyyy', 'yyyy/MM/dd'];
            $scope.dateFormat = $scope.dateFormats[0];
            $scope.calendarioCinta = $('#calendario_cinta');
            $scope.calendarioSeleccionado = $scope.calendarioCinta.find('#calendario_seleccionado');


            $scope.n_range = function(num){
                num = parseInt(num);
                return _.range(1, num+1);
            };
            $scope.z_range = function(num){
                num = parseInt(num);
                return _.range(0, num+1);
            };

            $scope.acc_types = _.object(_.map($scope.bData.types, function(arr){
                arr['oq_variants'] = $scope.n_range(arr['object_number']);
                arr['oq_selected'] = 1;
                arr['adults_variants'] = $scope.n_range(arr['adults_number']);
                arr['adults_selected'] = 1;
                arr['children_variants'] = $scope.z_range(arr['guests_number']-arr['adults_number']);
                arr['children_selected'] = 0;
                return [arr.id, arr];
            }));
            $scope.result.accType = 0;


            $scope.set_acc_type_selected_id = function(id){
                if(_.has($scope.acc_types, id)) {
                    $scope.result.accType = id;
                } else {
                    $scope.result.accType = 0;
                }
            };
            $scope.bedsRecount = function(){
                $scope.acc_types[$scope.result.accType].adults_selected = $scope.result.places.adult;
                $scope.acc_types[$scope.result.accType].children_selected = $scope.result.places.children;
                for (var i in $scope.acc_types) {
                    var cType = $scope.acc_types[i],
                        oqSelected = parseInt(cType['oq_selected']),
                        guestsNumber = cType['guests_number'] * oqSelected,
                        childrenCount = guestsNumber-cType['adults_selected'];
                    $scope.acc_types[i]['adults_variants'] = $scope.n_range(cType['adults_number'] * oqSelected);
                    $scope.acc_types[i]['children_variants'] = $scope.z_range(childrenCount);
                    if (cType['children_selected'] > childrenCount) {
                        cType['children_selected'] = childrenCount;
                        $scope.result.places.children = childrenCount;
                    }
                }
            };
            $scope.showAccTypeDescription = function(id){
                $.fancybox({
                    content: $scope.acc_types[id].description,
                    maxWidth: 590,
                    wrapCSS: 'fancybox-booking',
                    padding: 20
                })
            };

            $scope.MesesListaInit = function () {
                var $mesesLista = $('.meses_lista'),
                    $meses = $('.meses'),
                    $mesesContenedor = $('.meses_contenedor'),
                    mesesListaWidth,
                    mesesListaOffset,
                    mesesWidth,
                    mesesContenedorOffset,
                    $marker = $("<div>").addClass("marker"),
                    $hoverMarker =$mesesLista.find('.hover_marker'),
                    initial = true;

                function SetMesesListaSizes() {
                    mesesListaOffset = $mesesLista.offset().left;
                    mesesWidth = $meses.width();
                    $mesesLista.width(function(){
                        return $(this).children('.meses_lista_elemento').size() * 44;
                    });
                    mesesListaWidth = $mesesLista.width();
                    mesesContenedorOffset = mesesListaWidth - mesesWidth;
                    $mesesContenedor.width(function(){
                        return mesesListaWidth * 2 - mesesWidth;
                    });
                    if(initial) {
                        $mesesLista.css({
                            left: mesesContenedorOffset
                        });
                        $mesesContenedor.css({
                            left: -mesesContenedorOffset
                        })
                    }
                }
                $(window).resize(SetMesesListaSizes);
                SetMesesListaSizes();

                $mesesLista
                    .draggable({containment: "parent"})
                    .hover(
                        function(){$hoverMarker.addClass('visible')},
                        function(){$hoverMarker.removeClass('visible')}
                    )
                    .mousemove(
                        function(e){
                            $hoverMarker.css({
                                left: e.pageX - $mesesLista.offset().left - 22
                            });
                            $marker.data("ptj", false);
                        }
                    )
                    .mousedown(function(){
                        $marker.data("ptj", true);
                    })
                    .mouseup(function(){
                        if($marker.data("ptj")) {
                            $marker.css({
                                left: $hoverMarker.css("left")
                            });
                        }
                    });
                $marker
                   .appendTo($mesesLista)
                   .draggable({containment: "parent"});
                $hoverMarker.draggable({containment: "parent"});
            };
            $scope.MesesListaInit();

            setTimeout(function(){
//                $('#meses').iosSlider({
//                    desktopClickDrag: true
//                });
                $('#calendario_slider').iosSlider({
                    desktopClickDrag: true,
                    navPrevSelector: "#calendario_boton_previo",
                    navNextSelector: "#calendario_boton_siguiente",
                    responsiveSlideContainer: false,
                    responsiveSlides: false
                });
                $(window).resize(function(){
                    $('#calendario_slider').iosSlider("update");
                });
            },1);


            $scope.wfss = function(e){
                $scope.calendarioSeleccionado = $scope.calendarioCinta.find('.seleccionado');
                var cellWidth = 29,
                    $ps = $scope.calendarioSeleccionado.children('.puede_ser');
                    var offset = $scope.calendarioSeleccionado.offset(),
                        bLeft = offset.left,
                        bTop = offset.top,
                        dX = e.pageX - bLeft,
                        dY = e.pageY - bTop;
                    if (dY > 0 && dY < 30) {
                        var aDias = Math.floor(dX / cellWidth);
                        $ps.width(aDias * cellWidth);
                    } else {
                        $ps.width(0);
                    }
                    $('.sel_balloon .check-out').text($scope.result.dates.checkOut);
                };


            $scope.moverCinta = function(){
                $scope.calendarioSeleccionado = $scope.calendarioCinta.find('.seleccionado');
                var cellWidth = 29,
                    $ps = $scope.calendarioSeleccionado.children('.puede_ser');
                $scope.calendarioCinta.mousemove($scope.wfss);
                $ps.click(function(e){
                    var aDias = Math.floor($(this).width() / cellWidth),
                        fecha_checkout = moment($scope.dtCheckIn).add('d', aDias);
                    fecha_checkout.add('d', 1);
                    $scope.dtCheckOut = fecha_checkout._d;
                    $scope.result.dates.checkOut = $scope.formatDate(fecha_checkout);
                    $scope.calendarioSeleccionado.width((aDias + 1) * cellWidth);
                    $ps.width(0);
                    $scope.calendarioCinta.off('mousemove', $scope.wfss);
                });
            };

            $scope.seleccioneLaFecha = function($event){
                var $target = $($event.target),
                    ano = $target.data('ano'),
                    mes = $target.data('mes')-1,
                    dia = $target.data('dia');
                if (ano && mes && dia && $scope.calendarioCinta.hasClass('claro')) {
                    var fecha = moment(),
                        fecha_checkout = moment();
                    fecha.year(ano).month(mes).date(dia);
                    fecha_checkout.year(ano).month(mes).date(dia);
                    fecha_checkout.add('d', 1);
                    var $seleccionado = $('<div class="seleccionado"><div class="puede_ser"></div></div>').appendTo($target);
                    $($scope.bData.config.baloon_template).appendTo($seleccionado);
                    if(!$scope.result.dates.checkIn) {
                        $scope.dtCheckIn = fecha._d;
                        $scope.result.dates.checkIn = $scope.formatDate(fecha);
                        $scope.dtCheckOut = fecha_checkout._d;
                        $scope.result.dates.checkOut = $scope.formatDate(fecha_checkout);
                        $scope.result.accType = $target.closest('.tipo').data('tipo');
                        $('.sel_balloon .check-in').text($scope.result.dates.checkIn);
                        $('.sel_balloon .check-out').text($scope.result.dates.checkOut);
                    } else if (!$target.hasClass('masdias')) {
                        $scope.dtCheckOut = fecha_checkout._d;
                        $scope.result.dates.checkOut = $scope.formatDate(fecha_checkout);
                        $scope.calendarioCinta.off('mousemove', $scope.wfss);
                        $target.addClass('masdias');
                    }
                    $seleccionado.click(function(e){
                        if($(e.target).hasClass('seleccionado')){
                            $seleccionado.remove();
                            $scope.calendarioCinta.addClass('claro');
                            $scope.dtCheckIn = null;
                            $scope.result.dates.checkIn = null;
                            $scope.dtCheckOut = null;
                            $scope.result.dates.checkOut = null;
                        }
                    });
                    $target.closest('.calendario_slider_cinta_mes').addClass('sMes');
                    $scope.calendarioCinta.removeClass('claro');
                    $scope.moverCinta();
                }
            };
            $scope.esSeleccionado = function(tipo, mes_crudo, dia){
                var fecha = moment(mes_crudo);
                fecha.date(dia);
                return tipo == $scope.result.accType;
            };


            $scope.openDatePicker = function($event, dpID) {
                $event.preventDefault();
                $event.stopPropagation();
                if (dpID == 'ci') {
                    $scope.datePickerCIOpened = true;
                    $scope.datePickerCOOpened = false;
                } else {
                    $scope.datePickerCIOpened = false;
                    $scope.datePickerCOOpened = true;
                }
            };
            $scope.formatDate = function(date) {
                if (date) {
                    return moment(date).format("D.MM.YYYY");
                } else {
                    return null;
                }
            };
            $scope.$watch('dtCheckIn', function(dtCheckIn){
                $scope.result.dates.checkIn = $scope.formatDate(dtCheckIn);
            });
            $scope.$watch('dtCheckOut', function(dtCheckOut){
                $scope.result.dates.checkOut = $scope.formatDate(dtCheckOut);
            });

            $('.extra_option_checkbox')
                .on('ifChecked', function(){
                    $scope.result.options.push($(this).val());
                    $scope.result.options.sort();
                    $scope.$apply(function(){
                        $scope.result.options = _.uniq($scope.result.options, true);
                    });
                })
                .on('ifUnchecked', function(){
                    var newOptionsArray = _.without($scope.result.options, $(this).val());
                    $scope.$apply(function(){
                        $scope.result.options = newOptionsArray;
                    });
                });
            $('.payment_terms_rb')
                .on('ifClicked', function(){
                    var termsVal = parseInt($(this).val());
                    $scope.$apply(function(){
                        $scope.result.terms = termsVal;
                    });
                });


            $scope.select2Options = {
                minimumResultsForSearch: -1,
                allowClear: true
            };


            $('input').iCheck({
                checkboxClass: 'icheckbox_fs',
                radioClass: 'iradio_fs'
            });
            $('.payment_item_label').click(function(){
                $(this).siblings(".description").stop().slideToggle(900);
                $(this).parent(".payment_item").toggleClass("open");
                $(".payment_item .turn").click(function(){
                    $(this)
                        .closest(".description").stop().slideUp(900)
                        .closest(".payment_item").removeClass("open");
                })
            });
            $(".terms_of_booking, .term_title").fancybox({
                wrapCSS: 'fancybox-booking',
                maxWidth: 590,
                padding: 20
            });

            console.log($scope);

            $scope.formSubmit = function(){
                var postData = {
                    'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
                    'acc_type': $scope.result.accType,
                    'date_check_in': $scope.result.dates.checkIn,
                    'date_check_out': $scope.result.dates.checkOut,
                    'adults': $scope.result.places.adult,
                    'children': $scope.result.places.children,
                    'extra_options': $scope.result.options,
                    'payment_terms': $scope.result.terms,
                    'name': $('#user_data_fio').val(),
                    'email': $('#user_data_email').val(),
                    'phone': $('#user_data_phone').val(),
                    'check_in_time': $('#user_data_check_in_time').val(),
                    'note': $('#user_data_note').val(),
                };
                console.log($scope.result);
                console.log(postData);
                $.post(
                    $('#booking_form').data('action'),
                    postData
                );
            }
            $('#booking_form').submit(function(){return false;});
        }
    ]);