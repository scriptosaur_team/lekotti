bookingFilters = angular.module 'bookingFilters', []


bookingFilters.filter 'dateFormat', ['$filter', ->
  (input, format='D.MM.YYYY')->
    if input.isValid()
      input.format format
    else
      ''
]