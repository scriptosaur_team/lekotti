$ ->

  $bookingForm = $ "#booking_form"

  $bookingForm.find ".popup_link"
    .fancybox
      wrapCSS: 'fancybox-booking'
      maxWidth: 590
      padding: 20

#  $bookingForm.find "select"
#    .select2
#      minimumResultsForSearch: -1

  $bookingForm.find "input"
    .iCheck
      checkboxClass: "icheckbox_fs"
      radioClass: "iradio_fs"

#  $bookingForm.find "input.date"
#    .datepicker
#      dateFormat: "dd.mm.yy"

  $bookingForm.find ".payment_item_label"
    .click ->
      $(@).siblings(".description").stop().slideToggle 900
      $(@).parent(".payment_item").toggleClass "open"
    .find ".turn"
      .click ->
        $ @
          .closest(".description").stop().slideUp 900
          .closest(".payment_item").removeClass "open"