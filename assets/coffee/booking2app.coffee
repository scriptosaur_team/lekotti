'use strict'
after = (ms, cb) -> setTimeout cb, ms


bookingApp = angular.module 'bookingApp', [
  'bookingControllers'
  'bookingFilters'
  'bookingDirectives'
  'ui.select2'
  'ui.date'
]


