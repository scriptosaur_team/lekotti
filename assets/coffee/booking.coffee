$ ->
  BookingApp =
    $bookingForm: $ '#booking_form'
    $tipoDeAlojamiento: $('#booking_form').find '.tipo_de_alojamiento'
    $tipoDeAlojamientoSelect: $ '#acc_type_select'
    $objectNumberSelect: $ '#object_number_select'
    $calendarioCinta: $ '#calendario_cinta'
    $calendarioCintaMes: $ '.calendario_slider_cinta_mes'
    $calendario: $ '.calendario'

    $calendarioSeleccionado: $ '#calendario_seleccionado'
    $puedeSer: $ '#puede_ser'
    $calendarioTip: $ '#calendario_tip'

    $paqueteCalendarioSeleccionado: $ '#package_seleccionado'
    $paquetePuedeSer: $ '#package_puede_ser'
    $paqueteCalendarioTip: $ '#package_calendario_tip'

    $diasEscala: $ '.mes_dias_elemento'
    $childrenSelect: $ '#children-select'
    $adultSelect: $ '#adults-select'
    $objectNumeroSelect: $ '#object_number_select'
    $extraOptionCheckbox: $('.extra_option_checkbox')
    $paymentTermsRB: $('.payment_terms_rb')
    $entrada: $('.entrada')

    cellWidth: 29

    cintasPuntos:
      meses: {}
      calendario: {}
    mesDiasNumero: {}
    calendarioCintaFecha: moment()
    calendarioContenedorOffset: 0

    prices: window['prices']

    result:
      'type': 0
      'object_number': 1
      'dates':
        'checkIn': moment(null)
        'checkOut': moment(null)
      'paq_dates':
        'checkIn': moment(null)
        'checkOut': moment(null)
      'places':
        'adult': 1
        'children': 0
      'options': []
      'terms': 1

    after: (ms, cb)-> setTimeout cb, ms



    Design: ->
      app = @
      $primeroMes = app.$calendarioCinta.find('.calendario_slider_cinta_mes').first()
      primeroMes_m_datos = "#{$primeroMes.data('mes')}".split "."
      minDate = new Date(primeroMes_m_datos[0], primeroMes_m_datos[1]-1, $primeroMes.data('primero-dia'))
      $ultimoMes = app.$calendarioCinta.find('.calendario_slider_cinta_mes').last()
      ultimoMes_m_datos = "#{$ultimoMes.data('mes')}".split "."
      maxDate = new Date(ultimoMes_m_datos[0], ultimoMes_m_datos[1]-1, $ultimoMes.data('ultimo-dia'))
      @$bookingForm.find 'select'
        .select2
          minimumResultsForSearch: -1
      @$bookingForm.find 'input'
        .iCheck
          checkboxClass: 'icheckbox_fs'
          radioClass: 'iradio_fs'
      @$bookingForm.find 'input.date'
        .datepicker
          dateFormat: 'dd.mm.yy'
          minDate: minDate
          maxDate: maxDate
          onSelect: (d)->
            if d
              dt = moment d, "DD.MM.YYYY"
              if app.result.type <=0
                app.SelectAccType 1
              app.DateSelect dt.year(), dt.month()+1, dt.date(), app.result.type
      $('.payment_item_label').click ->
        $(@).siblings(".description").stop().slideToggle(900)
        $(@).parent(".payment_item").toggleClass("open")
        $(".payment_item .turn").click ->
          $(@)
            .closest(".description").stop().slideUp(900)
            .closest(".payment_item").removeClass("open")

      app.$bookingForm.find(".popup_link").fancybox
          wrapCSS: 'fancybox-booking'
          maxWidth: 590
          padding: 20
      app.$calendarioCinta.find('.dia').children('.package').each ->
        $this = $ @
        dias = parseInt $this.data('dias')
        $this.css
          width: (dias * app.cellWidth) - 1




    SelectAccType: (typeID) ->
      @result.type = typeID
      $selectedTipo = @$selectedAccType()
      $selectedTipo.addClass 'active'
        .siblings().removeClass 'active'
      @$tipoDeAlojamientoSelect.select2('val', typeID)
      @$objectNumberSelect
        .html @$selectedAccTypeObjectsNumber().html()
        .removeAttr 'disabled'
        .select2('val', @result.object_number)
      @SelectObjectNumber()
      @SetChildrenCountVariants()
      @SetAdultCountVariants()
      $('#cs_tipo').text $selectedTipo.find('.valor').text()
      @SelectValidate()
      @PriceRecount()
      @$tipoDeAlojamientoSelect.closest('.dh').removeClass 'has-error'


    $selectedAccType: ->
      return @$tipoDeAlojamiento.filter("[data-type=#{@result.type}]")


    $selectedAccTypeObjectsNumber: ->
      return @$selectedAccType().find 'select.numero'


    SelectObjectNumber: (numero=null) ->
      app = @
      if not numero
        numero = parseInt @$selectedAccTypeObjectsNumber().select2('val')
      @result.object_number = parseInt numero
      @$selectedAccTypeObjectsNumber().select2('val', app.result.object_number)
      @$objectNumberSelect.select2('val', app.result.object_number)
      @SelectValidate()
      @PriceRecount()


    SetMesesListaPosition: (ano_y_mes, dia=0, animate=true)->
      mesPosition = @cintasPuntos.meses[ano_y_mes]
      diaPosition = dia * 1.42
      if animate
        $('.meses_lista .marker').animate
          left: mesPosition + diaPosition
      else
        $('.meses_lista .marker').css
          left: mesPosition + diaPosition

    MesesListaInit: ->
      $mesesLista = $ '.meses_lista'
      $meses = $ '.meses'
      $mesesContenedor = $ '.meses_contenedor'
      mesesListaWidth = 0
      mesesListaOffset = 0
      mesesWidth = 0
      mesesContenedorOffset = 0
      $marker = $mesesLista.find ".marker"
      $hoverMarker =$mesesLista.find '.hover_marker'
      initial = true
      app = @

      SetMesesListaSizes = ->
        mesesListaOffset = $mesesLista.offset().left
        mesesWidth = $meses.width()
        $mesesLista.width ->
          return $(@).children('.meses_lista_elemento').size() * 44
        mesesListaWidth = $mesesLista.width()
        mesesContenedorOffset = mesesListaWidth - mesesWidth
        $mesesContenedor.width ->
          return mesesListaWidth * 2 - mesesWidth
        if initial
          initial = false
          $mesesLista.children('.meses_lista_elemento').each ->
            $mes = $(@)
            app.cintasPuntos.meses[$mes.data('mes')] = $mes.offset().left - mesesListaOffset
          $mesesLista.css
            left: mesesContenedorOffset
        $mesesContenedor.css
          left: -mesesContenedorOffset

      $(window).resize SetMesesListaSizes
      SetMesesListaSizes()

      SetCalendarioPositionByMarker = (animate=false)->
        pos = parseFloat($hoverMarker.css "left")
        $marker.css
          left: pos
        mes = _.findLastKey app.cintasPuntos.meses, (p)->
          p < pos
        dias = Math.floor((pos - app.cintasPuntos.meses[mes]) / 1.42)
        dias = 0 if isNaN(dias)
        app.SetCalendarioPosition mes, dias, animate


      $mesesLista
        .draggable
          containment: "parent"
        .hover ->
          $hoverMarker.addClass 'visible'
        , -> $hoverMarker.removeClass 'visible'
        .mousemove (e) ->
          $hoverMarker.css
            left: e.pageX - $mesesLista.offset().left - 22
          $marker.data "ptj", false
        .mousedown ->
          $marker.data "ptj", true
        .mouseup ->
          if $marker.data("ptj")
            SetCalendarioPositionByMarker(true)
      $marker
        .draggable
          containment: "parent"
          drag: ->
            SetCalendarioPositionByMarker()
        .hover ->
          $hoverMarker.removeClass 'visible'
        , -> $hoverMarker.addClass 'visible'




    SetCalendarioPosition: (ano_y_mes, dia=0, animate=false)->
      mesPosition = @cintasPuntos.calendario[ano_y_mes]
      diaPosition = dia * 29
      fechaPosition = mesPosition + diaPosition
      if animate
        $('.calendario_slider_cinta').animate
          left: @calendarioContenedorOffset - fechaPosition
      else
        $('.calendario_slider_cinta').css
          left: @calendarioContenedorOffset - fechaPosition

    CalendarioCintaInit: ->
      $calendarioContenedor = $ '.calendario_slider'
      calendarioCintaWidth = 0
      calendarioCintaOffset = 0
      calendarioWidth = 0
      initial = true
      app = @

      SetCalendarioCintaSizes = ->
        calendarioCintaOffset = app.$calendarioCinta.offset().left
        calendarioWidth = app.$calendario.width()
        app.$calendarioCinta.width ->
          cc_width = 0
          $(@).children('.calendario_slider_cinta_mes').each ->
            $mes = $(@)
            cc_width += parseInt $mes.width()
          cc_width
        calendarioCintaWidth = app.$calendarioCinta.width()
        app.calendarioContenedorOffset = calendarioCintaWidth - calendarioWidth
        $calendarioContenedor.width ->
          return calendarioCintaWidth * 2 - calendarioWidth
        if initial
          initial = false
          $('.calendario_slider_cinta_mes').each ->
            $mes = $(@)
            app.cintasPuntos.calendario[$mes.data('mes')] = $mes.offset().left - calendarioCintaOffset
            app.mesDiasNumero[$mes.data('mes')] = $mes.data('ultimo-dia') - $mes.data('primero-dia') + 1
          app.$calendarioCinta.css
            left: app.calendarioContenedorOffset
        $calendarioContenedor.css
          left: -app.calendarioContenedorOffset

      $(window).resize SetCalendarioCintaSizes
      SetCalendarioCintaSizes()

      app.$calendarioCinta
        .draggable
          containment: "parent"
          drag: (event, ui)->
            pos = parseInt(app.calendarioContenedorOffset - ui.position.left)
            mes = _.findLastKey app.cintasPuntos.calendario, (p)->
              p < pos
            dias = Math.floor((pos - app.cintasPuntos.calendario[mes]) / 29)
            dias = 0 if isNaN(dias)
            app.SetMesesListaPosition mes, dias+1, false

    Slide2Mes: (e)->
      app = @
      e.preventDefault()
      $boton = $ e.target
      pos = parseInt(app.$calendario.offset().left - app.$calendarioCinta.offset().left)
      mes = _.findLastKey app.cintasPuntos.calendario, (p)->
        p <= pos
      ma = mes.split '.'
      mmes = moment [ma[0], ma[1]-1, 1]
      if $boton.hasClass 'previo'
        nm = mmes.clone().subtract 'M', 1
      else if $boton.hasClass 'siguiente'
        nm = mmes.clone().add 'M', 1
      nms = nm.months() + 1
      nms = "0#{nms}" if nms < 10
      nmi = "#{nm.years()}.#{nms}"
      app.SetMesesListaPosition nmi
      app.SetCalendarioPosition nmi, 0, true



    SetDateCheckIn: (ano, mes, dia, duration=1)->
      CI_Fecha = moment [ano, mes-1, dia]
      @result.dates.checkIn = CI_Fecha
      $('#tip_check_in_date').text CI_Fecha.format "D MMMM"
      $('#package_tip_check_in_date').text CI_Fecha.format "D MMMM"
      $('#check-in-date').datepicker "setDate", "#{dia}.#{mes}.#{ano}"
      if not @result.dates.checkOut.isValid()
        CO_Fecha = CI_Fecha.clone().add "d", duration
        @result.dates.checkOut = CO_Fecha
        $('#tip_check_out_date').text CO_Fecha.format "D MMMM"
        $('#package_tip_check_out_date').text CO_Fecha.format "D MMMM"
        $('#check-out-date').datepicker "setDate", CO_Fecha.format "DD.MM.YYYY"

    SetDateCheckOut: (ano, mes, dia)->
      CO_Fecha = moment [ano, mes-1, dia]
      @result.dates.checkOut = CO_Fecha
      $('#tip_check_out_date').text CO_Fecha.format "D MMMM"
      $('#package_tip_check_out_date').text CO_Fecha.format "D MMMM"
      $('#check-out-date').datepicker "setDate", "#{dia}.#{mes}.#{ano}"


    GetPackageChain: (start_date, acc_type=app.result.acc_type, conditions=app.result.terms)->
      app = @
      chain =
        total: 0
        package_sets: []

      DateString = (dt)->
        dt.format "YYYY.MM.DD"

      GetPackages = (period_begin) ->
        p_list = []
        dtf = DateString period_begin
        try
          p = app.prices['by_type'][acc_type][dtf][conditions]['package']
          dc = moment.duration(period_begin-start_date).asDays()
          pk = _.keys p
          if pk.length > 0
            pl = _.min(pk)
            dc += parseInt pl
            p_list.push dc
            new_period_begin = period_begin.clone().add "d", pl
            n_list = GetPackages new_period_begin
            if n_list
              p_list = p_list.concat n_list
            else
              p_list
          else
            p_list
        catch
          p_list

        return p_list

      chain.package_sets = GetPackages start_date


    DeselectPackage: ->
      @$calendarioCinta
        .removeClass 'una_noche'
        .addClass 'claro'
      @$paqueteCalendarioSeleccionado.fadeOut 200

    SelectPackage: (e)->
      e.stopPropagation()
      app = @
      $target = $ e.target
      app.DateMarkerReset(e)
      app.DeselectPackage()

      markerLeft = $target.offset().left - app.$calendarioCinta.offset().left
      markerTop = $target.offset().top - app.$calendarioCinta.offset().top
      markerWidth = $target.outerWidth()

      markerPosition =
        left: markerLeft
        top: markerTop
        width: markerWidth
      if markerTop > 150
        app.$calendarioTip.addClass 'at_top'
      else
        app.$calendarioTip.removeClass 'at_top'

      app.$paqueteCalendarioSeleccionado
        .css markerPosition
        .fadeIn 200

      app.$paquetePuedeSer
        .css
          left: markerWidth

      $dia = $target.closest '.dia'
      ano = parseInt $dia.data 'ano'
      mes = parseInt($dia.data 'mes')
      dia = parseInt $dia.data 'dia'
      package_date_begin = moment [ano, mes-1, dia]
      conditions = $target.data 'conditions'
      acc_type = $target.closest('.tipo').data 'tipo'
      app.SelectAccType acc_type
      package_long_variants = app.GetPackageChain package_date_begin, acc_type, conditions
      app.SetDateCheckIn ano, mes, dia, package_long_variants[0]
      CI_Fecha = moment [ano, mes-1, dia]
      CO_Fecha = CI_Fecha.clone().add "d", package_long_variants[0]
      @PackageMarkerWideInit = (e)=> @PackageMarkerWide(e, package_long_variants, CI_Fecha, acc_type, conditions)
      @$calendarioCinta
        .removeClass 'claro'
        .addClass 'una_noche'
        .on 'mousemove', @PackageMarkerWideInit
      price = app.PriceCountFor CI_Fecha, CO_Fecha, acc_type, conditions
      $('#package_seleccionado_precio').text price
      $('#package_tip_price').text price
      app.result.paq_dates.checkIn = CI_Fecha
      app.result.paq_dates.checkOut = CO_Fecha
      app.PaqPriceRecount()


    PackageMarkerWide: (e, package_duration_variants, CI_Fecha, acc_type, conditions)->
      # растягиваем маркер над пакетами при движении мыши
      app = @
#      console.log @result
#      CI_Fecha = @result.dates.checkIn
      if CI_Fecha and package_duration_variants.length > 0
        $ps = @$paquetePuedeSer
        offset = @$paquetePuedeSer.offset()
        bLeft = offset.left
        bTop = offset.top
        dX = e.pageX - bLeft
        dY = e.pageY - bTop
        current_package_duration = package_duration_variants[0]
        if dY > 0 and dY < 30 and dX > 0
          aDias = Math.ceil(dX / @cellWidth)
          pDias = _.min(_.filter(package_duration_variants, (n)-> n >= (aDias + current_package_duration)))
        else
          pDias = current_package_duration
        $ps.width((pDias - current_package_duration) * @cellWidth)
        CO_Fecha = CI_Fecha.clone().add "d", pDias
#        console.log CI_Fecha
#        console.log pDias
#        console.log CO_Fecha
        $('#package_tip_check_out_date').text CO_Fecha.format "D MMMM"
        price = app.PriceCountFor CI_Fecha, CO_Fecha, acc_type, conditions
        $('#package_seleccionado_precio').text price
        $('#package_tip_price').text price
        $('#package_tip_nights_count').text pDias
        app.result.paq_dates.checkOut = CO_Fecha


    PackageMarkerFix: (e)->
      # устанавливаем выбранный интервал
      e.stopPropagation()
      app = @
      cWidth = app.$paqueteCalendarioSeleccionado.width()
      app.$paqueteCalendarioSeleccionado.width cWidth + app.$paquetePuedeSer.width()
      app.$calendarioCinta
        .removeClass 'una_noche claro'
        .off 'mousemove', app.PackageMarkerWideInit
      app.$paquetePuedeSer.width 0

      app.PaqPriceRecount()


    PaqPriceRecount: ->
      app = @
      if app.prices['by_type'][app.result.type]
        totalPrice = app.PriceCountFor app.result.paq_dates.checkIn, app.result.paq_dates.checkOut, app.result.type, app.result.terms

        if app.result.terms is 1
          full_cost = totalPrice
          now_cost = totalPrice * .2
        else if app.result.terms is 2
          full_cost = now_cost = totalPrice * .8

        $('#full_cost').text full_cost
        $('input[name=price]').val full_cost
        $('#now_cost').text now_cost.toFixed 2




    PlaceDateMarker: (ano, mes, dia, tipo)->
      # размещаем маркер на таблице
      app = @
      app.DeselectPackage()
      $diaCell = $(".dia.libre[data-ano=#{ano}][data-mes=#{mes}][data-dia=#{dia}][data-tipo=#{tipo}]").first()
      markerLeft = $diaCell.offset().left - app.$calendarioCinta.offset().left + 1
      markerTop = $diaCell.offset().top - app.$calendarioCinta.offset().top
      markerPosition =
        left: markerLeft
        top: markerTop
      if markerTop > 150
        app.$calendarioTip.addClass 'at_top'
      else
        app.$calendarioTip.removeClass 'at_top'
      app.$calendarioSeleccionado
        .css markerPosition
        .fadeIn 200
      @DateMarkerWideInit = (e)=> @DateMarkerWide(e)
      @$calendarioCinta
        .removeClass 'claro'
        .on 'mousemove', @DateMarkerWideInit

    DiaEscalaSelect: ->
      app = @
      app.$diasEscala.removeClass 'selected'
      if app.result.dates.checkIn and app.result.dates.checkOut
        ciMes = app.result.dates.checkIn.clone().date 1
        coMes = app.result.dates.checkOut.clone().date 1
        mesAffected = coMes.diff(ciMes, 'month')
        if mesAffected == 0
          $mes = app.$calendarioCintaMes.filter "[data-mes='#{ciMes.format("YYYY.MM")}']"
          $mes.find(app.$diasEscala).each ->
            dia = $(@).data 'dia'
            if parseInt(app.result.dates.checkIn.format('D')) <= dia <= parseInt(app.result.dates.checkOut.format('D'))
              $(@).addClass 'selected'
        else if mesAffected >= 1
          $ciMes = app.$calendarioCintaMes.filter "[data-mes='#{ciMes.format("YYYY.MM")}']"
          $ciMes.find(app.$diasEscala).each ->
            dia = $(@).data 'dia'
            if parseInt(app.result.dates.checkIn.format('D')) <= dia
              $(@).addClass 'selected'
          $coMes = app.$calendarioCintaMes.filter "[data-mes='#{coMes.format("YYYY.MM")}']"
          $coMes.find(app.$diasEscala).each ->
            dia = $(@).data 'dia'
            if dia <= parseInt(app.result.dates.checkOut.format('D'))
              $(@).addClass 'selected'
          if mesAffected > 1
            for dm in [1..mesAffected-1]
              cMes = ciMes.clone().add('M', dm)
              app.$calendarioCintaMes.filter "[data-mes='#{cMes.format("YYYY.MM")}']"
                .find(app.$diasEscala).addClass 'selected'


    DateMarkerReset: (e)->
      # сбрасываем выбранные даты
      e.stopPropagation()
      app = @
      @$calendarioSeleccionado.fadeOut 200, ->
        app.$calendarioSeleccionado.width app.cellWidth-1
        app.$puedeSer.width 0
      $.datepicker._clearDate $('#check-in-date')
      $.datepicker._clearDate $('#check-out-date')
      @$calendarioCinta
        .removeClass 'una_noche'
        .addClass 'claro'
        .off 'mousemove', @DateMarkerWideInit
      app.$diasEscala.removeClass 'selected'
      @after 10, ->
        app.result.dates.checkIn = moment(null)
        app.result.dates.checkOut = moment(null)
        app.SelectValidate()


    DateMarkerWide: (e)->
      # растягиваем маркер при движении мыши
      CI_Fecha = @result.dates.checkIn
      if CI_Fecha
        $ps = @$puedeSer
        offset = @$calendarioSeleccionado.offset()
        bLeft = offset.left
        bTop = offset.top
        dX = e.pageX - bLeft
        dY = e.pageY - bTop
        if dY > 0 and dY < 30 and dX > 0
          aDias = Math.floor(dX / @cellWidth)
          $ps.width(aDias * @cellWidth)
          CO_Fecha = CI_Fecha.clone().add "d", aDias+1
          if aDias > 0
            @$calendarioSeleccionado.addClass 'to_booking'
          else
            @$calendarioSeleccionado.removeClass 'to_booking'
        else
          $ps.width(0)
          CO_Fecha = CI_Fecha.clone().add "d", 1
          @$calendarioSeleccionado.removeClass 'to_booking'
        $('#tip_check_out_date').text CO_Fecha.format "D MMMM"

    DateMarkerFix: (e)->
      # устанавливаем выбранный интервал
      e.stopPropagation()
      app = @
      aDias = Math.round app.$puedeSer.width()/app.cellWidth + 1
      @$calendarioSeleccionado.removeClass 'to_booking'
      app.SetDateInterval aDias
      app.PriceRecount()

    SetDateInterval: (dias_numero)->
      app = @
      if app.result.dates.checkIn
        fecha_checkout = app.result.dates.checkIn.clone().add 'd', dias_numero
        app.SetDateCheckOut fecha_checkout.year(), fecha_checkout.month()+1, fecha_checkout.date()
        app.$calendarioSeleccionado.width dias_numero * app.cellWidth
        app.$puedeSer.width 0
        app.$calendarioCinta
          .removeClass 'una_noche claro'
          .off 'mousemove', app.DateMarkerWideInit
        app.DiaEscalaSelect()


    DateSelect: (ano, mes, dia, tipo)->
      app = @

      if app.$calendarioCinta.hasClass 'claro'
        app.SetDateCheckIn ano, mes, dia
        app.SelectAccType tipo
        app.PlaceDateMarker ano, mes, dia, tipo
        app.$calendarioCinta
          .addClass 'una_noche'
          .removeClass 'claro'
      else if app.$calendarioCinta.hasClass 'una_noche'
        app.SelectAccType tipo
        app.SetDateCheckOut ano, mes, dia
        app.SetDateInterval app.result.dates.checkOut.diff(app.result.dates.checkIn, 'days')
        app.$calendarioCinta
          .removeClass 'una_noche claro'
      app.DiaEscalaSelect()
      if app.result.dates.checkIn
        $('#cs_date_check_in').text app.result.dates.checkIn.format("DD.MM.YY")
      if app.result.dates.checkOut
        $('#cs_date_check_out').text app.result.dates.checkOut.format("DD.MM.YY")
      app.PriceRecount()
      $('input.date').closest('.dh').removeClass 'has-error'
      app.SelectValidate()



    SetChildrenCountVariants: ->
      app = @
      $sTipo = app.$tipoDeAlojamiento.filter("[data-type=#{app.result.type}]")
      objectsCount = parseInt $sTipo.data 'objects'
      guestCount = parseInt $sTipo.data 'guests'
      totalGuestCount = objectsCount * guestCount
      totalChildrenCount = totalGuestCount - app.result.places.adult
      if totalChildrenCount > 0
        app.$childrenSelect
          .removeAttr 'disabled'
          .html ''
        childrenCountVariants = [0..totalChildrenCount]
        for n in childrenCountVariants
          app.$childrenSelect.append $('<option>').text(n)
        if app.result.places.children in childrenCountVariants
          app.$childrenSelect.select2 'val', app.result.places.children
        else
          app.$childrenSelect.select2 'val', 0

    SetAdultCountVariants: ->
      app = @
      $sTipo = app.$tipoDeAlojamiento.filter("[data-type=#{app.result.type}]")
      objectsCount = parseInt $sTipo.data 'objects'
      adultCount = parseInt $sTipo.data 'adults'
      guestCount = parseInt $sTipo.data 'guests'
      totalGuestCount = objectsCount * guestCount
      totalAdultCount = adultCount * objectsCount
      maxAdultCount = totalGuestCount - app.result.places.children
      if totalAdultCount > maxAdultCount
        totalAdultCount = maxAdultCount
      if totalAdultCount > 0
        app.$adultSelect
          .removeAttr 'disabled'
          .html ''
        adultCountVariants = [1..totalAdultCount]
        for n in adultCountVariants
          app.$adultSelect.append $('<option>').text(n)
        if app.result.places.adult in adultCountVariants
          app.$adultSelect.select2 'val', app.result.places.adult
        else
          app.$adultSelect.select2 'val', 1
    SelectChildrenCount: (c) ->
      app = @
      app.result.places.children = parseInt c
      app.SetAdultCountVariants()
      app.SetObjectNeededCount()
      $('#cs_children_count').text c
      if c > 0
        $('#cs_children_count_block').css
          display: 'inline'
      app.SelectValidate()

    SelectAdultCount: (c) ->
      app = @
      app.result.places.adult = parseInt c
      app.SetChildrenCountVariants()
      app.SetObjectNeededCount()
      $('#cs_adult_count').text c
      app.SelectValidate()

    SetObjectNeededCount: (numero)->
      app = @
      if parseInt(numero) > 0
        objectsNeeded = numero
      else
        guestsCount = app.result.places.children + app.result.places.adult
        $sTipo = app.$tipoDeAlojamiento.filter("[data-type=#{app.result.type}]")
        maxGuestsPerObject = parseInt($sTipo.data 'guests')
        maxAdultPerObject = parseInt($sTipo.data 'adults')
        objectsMayBeNeeded = [
          Math.ceil(app.result.places.children/maxGuestsPerObject - 1)
          Math.ceil(app.result.places.adult/maxAdultPerObject)
          Math.ceil(guestsCount/maxGuestsPerObject)
        ]
        objectsNeeded = Math.max objectsMayBeNeeded...
      app.SelectObjectNumber objectsNeeded
      app.$calendarioCintaMes.each ->
        $(@).find('.tipo').each ->
          $(@).find('.dia').each ->
            $this = $ @
            if $this.hasClass('parcial') or $this.hasClass('libre')
              if parseInt($this.data('libre')) < objectsNeeded
                $this.addClass("parcial").removeClass('libre')
              else
                $this.removeClass("parcial").addClass('libre')



    SelectExtraOptions: ->
      app = @
      app.result.options = []
      cs_v = []
      app.$extraOptionCheckbox.each ->
        $this = $ @
        if $this.is ':checked'
          app.result.options.push parseInt($this.val())
          cs_v.push $this.closest('.terms_item').find('.valor').text()
      $('#cs_extra').html cs_v.join ', '
      if app.result.options.length > 0
        $('#cs_extra_block').show()
      else
        $('#cs_extra_block').hide()
      @PriceRecount()

    SelectTerms: ->
      app = @
      $selectedPaymentTermsRB = app.$paymentTermsRB.filter(':checked')
      app.result.terms = parseInt $selectedPaymentTermsRB.val()
      $('#cs_terms').text $selectedPaymentTermsRB.closest('.payment_item').find('.valor').text()
      $('.dia .package').removeClass('active').filter("[data-conditions=#{app.result.terms}]").addClass('active')
      @PriceRecount()


    SelectValidate: ->
      app = @
      console.log app.result
      dateCheckIn = if app.result.dates.checkIn.isValid() then app.result.dates.checkIn else app.result.paq_dates.checkIn
      dateCheckOut = if app.result.dates.checkOut.isValid() then app.result.dates.checkOut else app.result.paq_dates.checkOut
      if app.result.type > 0 and dateCheckIn.isValid() and dateCheckOut.isValid()
        app.$entrada.addClass 'allow'
      else
        app.$entrada
          .removeClass 'allow'
          .slideUp()

    IsEmail: (email)->
      regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
      regex.test email

    FormValidate: ->
      app = @
      form_valid = true

      if @$entrada.hasClass 'allow'
        if app.$entrada.is ':visible'
          $('.datos_personales').find('.personale').each ->
            $this = $(@)
            $field = $this.find('input, textarea')
            $field.one 'keyup', ->
              $(@).closest('.personale').removeClass 'has-error'

            if $field.hasClass('required') and not $field.val()
              $this.addClass 'has-error'
              form_valid = false
            else if $field.hasClass('email') and not app.IsEmail($field.val())
              $this.addClass 'has-error special'
              form_valid = false
            else
              $this.removeClass 'has-error special'
        else
          app.$entrada.slideDown()
          form_valid = false
      else
        app.$entrada.slideUp()
        $('#acc_type_select, input.date').each ->
          $this = $ @
          if not $this.val()
            $(@).closest('.dh').addClass 'has-error'
        form_valid = false

      return form_valid

    PriceCountFor: (dateCheckIn, dateCheckOut, acc_type, conditions) ->
      app = @
      if app.prices['by_type'][acc_type]
        period = moment.duration(dateCheckOut - dateCheckIn).asDays()

        DateString = (dt)->
          dt.format "YYYY.MM.DD"

        PeriodPriceCount = (acc_type, period_begin, period_end, days, conditions)->
          ds_paso = 1
          dq = moment.duration(period_end - period_begin).asDays()

          paquetes_precios = @prices['by_type'][acc_type][DateString period_begin][conditions]['package']
          pqv = _.filter _.keys(paquetes_precios), (n)-> n <= dq
          if pqv.length > 0
            ds_paso = _.max pqv
            precio = paquetes_precios[ds_paso]
          else
            dias_precios = @prices['by_type'][acc_type][DateString period_begin][conditions]['single']
            pv = _.max _.filter _.keys(dias_precios), (n)-> n <= period
            precio = dias_precios[pv]

          new_period_begin = period_begin.clone().add "d", ds_paso

          if new_period_begin < period_end
            precio += PeriodPriceCount acc_type, new_period_begin, period_end, days, conditions
          precio

        if period > 0
          periodPrice = PeriodPriceCount acc_type, dateCheckIn, dateCheckOut, period, conditions
        else
          periodPrice = 0

        # а это вынести в админку надо
        optionsPrice = 0
        if 1 in app.result.options
          optionsPrice += 50
        if 2 in app.result.options and period > 0
          optionsPrice += (app.result.places.adult * 15 + app.result.places.children * 7) * period
        if 3 in app.result.options
          optionsPrice += app.result.places.adult * 20

        totalPrice = periodPrice * app.result.object_number + optionsPrice

        totalPrice

    PriceRecount: ->
      app = @
      if app.prices['by_type'][app.result.type]
        totalPrice = app.PriceCountFor app.result.dates.checkIn, app.result.dates.checkOut, app.result.type, app.result.terms

        if app.result.terms is 1
          full_cost = totalPrice
          now_cost = totalPrice * .2
        else if app.result.terms is 2
          full_cost = now_cost = totalPrice * .8

        $('#full_cost').text full_cost
        $('input[name=price]').val full_cost
        $('#now_cost').text now_cost.toFixed 2





    Init: ->
      app = @
      $form = app.$bookingForm
      app.Design()

      app.$tipoDeAlojamiento.click ->
        app.SelectAccType $(@).data('type')
      app.$tipoDeAlojamientoSelect.on 'change', ->
        app.SelectAccType $(@).val()

      app.$tipoDeAlojamiento.on 'change', '.numero', (e)->
        app.SelectObjectNumber e.val
        app.SelectAccType $(@).closest(app.$tipoDeAlojamiento).data 'type'
      app.$objectNumberSelect.on 'change', (e)->
        app.SelectObjectNumber e.val


      app.$calendarioCinta.on 'click', '.dia.libre', (e)->
        if app.$calendarioCinta.hasClass 'claro'
          $dia = $(@)
          ano = $dia.data 'ano'
          mes = $dia.data 'mes'
          dia = $dia.data 'dia'
          tipo = $dia.data 'tipo'
          app.DateMarkerReset(e)
          app.DeselectPackage()
          app.DateSelect ano, mes, dia, tipo
      app.$puedeSer.on 'click', (e)=> app.DateMarkerFix(e)
      app.$calendarioSeleccionado.on 'click', (e)=> app.DateMarkerReset(e)
      app.$calendarioCinta.on 'click', '.package', (e)-> app.SelectPackage(e)
      app.$paqueteCalendarioSeleccionado.on 'click', (e)=> app.DeselectPackage(e)
      app.$paquetePuedeSer.on 'click', (e)=> app.PackageMarkerFix(e)

      app.$adultSelect.on 'change', (e)->
        app.SelectAdultCount e.val
      app.$childrenSelect.on 'change', (e)->
        app.SelectChildrenCount e.val
      app.$objectNumeroSelect.on 'change', (e)->
        app.SetObjectNeededCount e.val
      app.$tipoDeAlojamiento.find('.numero').on 'change', (e)->
        app.SetObjectNeededCount e.val


      app.$extraOptionCheckbox.on 'ifToggled', => app.SelectExtraOptions()
      app.$paymentTermsRB.on 'ifToggled', => app.SelectTerms()
      app.SelectTerms()

      @MesesListaInit()
      @CalendarioCintaInit()

      $form.on 'submit', => app.FormValidate()
      $('.calendario_boton').on 'click', (e)=> app.Slide2Mes(e)


  BookingApp.Init()