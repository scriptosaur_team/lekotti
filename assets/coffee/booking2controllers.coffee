bookingControllers = angular.module 'bookingControllers', []


bookingControllers.controller 'bookingController', ['$scope'
    ($scope) ->

      moment.lang window['config']['lang']
      $scope.acc_types = window['config']['acc_types']
      for i, acc_type of $scope.acc_types
        $scope.acc_types[i]['object_number_range'] = [1..acc_type['object_number']]
        $scope.acc_types[i]['object_number_selected'] = 1
      $scope.extra_options = window['config']['extra_options']
      $scope.payment_terms = window['config']['payment_terms']
      $scope.dateFormats =
        'default': 'dd.MM.yyyy'
        'datepicker': 'd.mm.yy'


      $scope.result_init = ->
        $scope.result =
          'accType': 0
          'objectNumber': 1
          'dates':
            'checkIn': moment ''
            'checkOut': moment ''
          'places':
            'adult': 1
            'children': 2
          'options': []
          'terms': 1
      $scope.result_init()

      $scope.set_acc_type_selected_id = (id)->
        $scope.result.accType = id


      $scope.select2Options =
        minimumResultsForSearch: -1
        allowClear: true

      $scope.checkInDate = ''
      $scope.checkOutDate = ''
      $scope.dateOptions =
        yearRange: '1900:-0'
        dateFormat: $scope.dateFormats.datepicker

      $scope.$watch 'checkInDate', ->
        $scope.result.dates.checkIn = moment $scope.checkInDate
      $scope.$watch 'checkOutDate', ->
        $scope.result.dates.checkOut = moment $scope.checkOutDate
      $scope.$watch 'result.dates', ->
        $scope.checkInDate = $scope.result.dates.checkIn.toDate()
        $scope.checkOutDate = $scope.result.dates.checkOut.toDate()



      $('.extra_option_checkbox').on
        'ifChecked': ->
          $scope.result.options.push parseInt $(@).val()
          $scope.result.options.sort()
          $scope.$apply ->
            $scope.result.options = _.uniq $scope.result.options, true
        'ifUnchecked': ->
          newOptionsArray = _.without $scope.result.options, parseInt $(@).val()
          $scope.$apply ->
            $scope.result.options = newOptionsArray

      $('.payment_terms_rb')
        .on 'ifClicked', ->
          termsVal = parseInt $(@).val()
          $scope.$apply ->
            $scope.result.terms = termsVal


      console.log $scope
  ]