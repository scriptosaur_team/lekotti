��    ?        Y         p     q  '   �     �     �     �     �     �     �     	               %     2     9     E  @   R     �  S   �  Y   �     I     a     n  	   �  
   �  ,   �  
   �     �     �  /   �  $     )   4  (   ^  �   �     4	     E	  
   V	     a	     j	  
   r	     }	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     
     
     
     )
     2
     8
  e  =
     �  *   �     �     �               "     )     <     E     Q     c  
   u     �     �  M   �     �  X   �  U   N     �     �     �     �     �  )   �     '     6     E  .   M  !   |  *   �  *   �  �   �     �     �     �     �     �     �     �     
       	        #     3     :     @     M     R     X     _     k  
   p     {     �     �     �     �     �     �     �     �     >       0      %   ?   
   ,   2                  -      <            6      (       =      )   4   /   8       $   7               9       +                     !       #                           "      ;             *   '   5      .             &      	   3                      1                          :               A name for chunks group A unique name for this chunk of content Access settings Additional settings Advanced options Alias Description Display settings English Finnish Group Chunk Group Chunks Hidden Image Chunk Image Chunks Item's parent left unchanged. Item couldn't be parent to itself. Russian Short name to address site tree from templates.<br /><b>Note:</b> change with care. Short name to address site tree item from a template.<br /><b>Reserved aliases:</b> "%s". Show in breadcrumb path Show in menu Show in site tree Site Tree Site Trees Site tree title for presentational purposes. Text Chunk Text Chunks Title Whether to show this item in a breadcrumb path. Whether to show this item in a menu. Whether to show this item in a site tree. Whether to show this item in navigation. You are seeing this warning because "URL as Pattern" option is active and pattern entered above seems to be invalid. Currently registered URL pattern names and parameters:  active in summer active in winter activities activity address background banner title banners bus content date description icon icon hover image key link main image name ordering phone photo gallery place on map promo regional short description show map title type Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-30 23:11+0400
PO-Revision-Date: 2014-02-06 09:11+0400
Last-Translator:   <admin@admin.admin>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Translated-Using: django-rosetta 0.7.3
 Nimi paloina ryhmä Yksilöivä nimi tämän kimpale sisältö Käyttöoikeus-asetukset Lisää asetuksia Lisäasetukset Alias kuvaus Näytön asetukset englanti suomalainen Konsernin Kimpale Konsernin Paloina Piilotettu Kuva Kimpale Kuva Paloina Kohteen emoyhtiön jätetään ennalleen. Kohde ei voinut olla vanhempi itse. venäläinen Lyhyt nimi, osoite sivuston puu malleista.<br /><b>Huomautus:</b>  vaihda huolellisesti. Lyhyt nimi, osoite sivuston puu kohteen mallista.<br /><b>Varattu aliakset:</b> "%s". Näytä breadcrumb polku Näytä-valikko Näytä sivuston puu Sivuston Puu Sivuston Puut Sivuston puu otsikko ilmaisullinen aikoa. Teksti Kimpale Teksti Paloina otsikko Onko näytä tämän kohteen breadcrumb polku. Näkyykö tämä kohde valikosta. Onko näytä tämän kohteen sivuston puu. Onko näytä tämän kohteen navigointiin. Näet tämän varoituksen, koska "URL-Pattern" - vaihtoehto on aktiivinen ja malli tuli yllä näyttää olevan virheellinen. Tällä hetkellä rekisteröity URL-malli nimet ja parametrit:  aktiivinen kesällä aktiivinen talvella toiminta toiminta osoite tausta banner otsikko bannerit linja sisältö päivämäärä kuvaus ikoni kuvake hover kuva avain linkki pääkuvaan nimi tilaaminen puhelin kuvagalleria paikka kartalla edistäminen alueellinen lyhyt kuvaus näytä kartta otsikko tyyppi 