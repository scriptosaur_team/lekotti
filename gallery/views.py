from django.shortcuts import render_to_response
from django.template import RequestContext
from main.models import ExtendFlatPage, FlatPageImage
from interesting.models import InterestingPhoto
from activity.models import ActivityPhoto
from events.models import EventPhoto


def gallery(request, pk):
    def check_active(page):
        if int(page.id) == int(pk):
            page.active = True
        return page
    pages = [check_active(page) for page in ExtendFlatPage.objects.filter(show_gallery_in_popup=True)]
    photos = FlatPageImage.objects.filter(page__id=pk)
    try:
        photo_active_index = int(request.GET.get('active', 0))
    except:
        photo_active_index = 0
    return render_to_response('gallery/popup_gallery.html',
                              {
                                  'pages': pages,
                                  'photos': photos,
                                  'photo_active_index': photo_active_index,
                                  'photo_active': photos[photo_active_index],
                              },
                              context_instance=RequestContext(request))


def app_gallery(request, app_code, pk):
    if app_code == 'interesting':
        photos = InterestingPhoto.objects.filter(event__id=pk)
    elif app_code == 'activity':
        photos = ActivityPhoto.objects.filter(event__id=pk)
    elif app_code == 'events':
        photos = EventPhoto.objects.filter(event__id=pk)
    else:
        photos = None
    try:
        photo_active_index = int(request.GET.get('active', 0))
    except:
        photo_active_index = 0
    return render_to_response('gallery/popup_gallery.html',
        {
            'photos': photos,
            'photo_active_index': photo_active_index,
            'photo_active': photos[photo_active_index],
        },
                              context_instance=RequestContext(request))