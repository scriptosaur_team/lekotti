from django.conf.urls import patterns, url


urlpatterns = patterns('',
                       url(r'^(?P<pk>\d+)/$', 'gallery.views.gallery', name='gallery'),
                       url(r'^(?P<app_code>\w+)/(?P<pk>\d+)/$', 'gallery.views.app_gallery', name='gallery'),
                       )