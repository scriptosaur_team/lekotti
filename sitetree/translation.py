from modeltranslation.translator import translator, TranslationOptions
from .models import TreeItem


class TreeItemTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'keywords', 'summer_description', 'summer_keywords')


translator.register(TreeItem, TreeItemTranslationOptions)