# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'HelpPage'
        db.create_table(u'admin_help_helppage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=128)),
            ('ordering', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=100)),
            ('text', self.gf('cked.fields.RichTextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'admin_help', ['HelpPage'])


    def backwards(self, orm):
        # Deleting model 'HelpPage'
        db.delete_table(u'admin_help_helppage')


    models = {
        u'admin_help.helppage': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'HelpPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordering': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '128'}),
            'text': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '512'})
        }
    }

    complete_apps = ['admin_help']