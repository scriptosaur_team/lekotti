from django.conf.urls import patterns, url

urlpatterns = patterns('',
                       url(r'^$', 'admin_help.views.home', name='home'),
                       url(r'^(?P<slug>\w+)/$', 'admin_help.views.detail', name='detail'),
                       )