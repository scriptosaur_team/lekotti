from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from cked.fields import RichTextField


class HelpPage(models.Model):
    title = models.CharField(_('title'), max_length=512)
    slug = models.SlugField(_('slug'), max_length=128, unique=True)
    ordering = models.PositiveSmallIntegerField(_('ordering'), default=100)
    text = RichTextField(_('text'), blank=True, null=True)

    def __unicode__(self):
        return u'%s' % self.title

    def get_absolute_url(self):
        return reverse('help:detail', args=[str(self.slug)])

    class Meta:
        verbose_name = _('help page')
        verbose_name_plural = _('help pages')
        ordering = ['ordering']