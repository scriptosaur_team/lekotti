from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from .models import *


def home(request):
    pages = HelpPage.objects.all()
    return render_to_response('admin_help/home.html', {'pages': pages},
                              context_instance=RequestContext(request))


def detail(request, slug):
    page = get_object_or_404(HelpPage, slug=slug)
    return render_to_response('admin_help/detail.html', {'page': page}, context_instance=RequestContext(request))