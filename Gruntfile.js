/*global module:false*/
module.exports = function(grunt) {

grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.

    copy: {
        images: {
            expand: true,
            cwd: 'assets/images/',
            src: '**',
            dest: 'static/images/'
        },
        sound: {
            expand: true,
            cwd: 'assets/sound/',
            src: '**',
            dest: 'static/sound/'
        },
        fonts: {
            expand: true,
            cwd: 'assets/fonts/',
            src: '**',
            dest: 'static/fonts/'
        },
        glyphicons: {
            expand: true,
            cwd: 'bower_components/bootstrap/fonts/',
            src: '**',
            dest: 'static/fonts/'
        },
        js: {
            expand: true,
            cwd: 'bower_components/moment/lang/',
            src: [
                'ru.js',
                'fi.js'
            ],
            dest: 'static/js/moment/'
        }
    },
    less: {
      development: {
          options: {
              compress: false
          },
          files: {
              'static/css/style.css': 'assets/less/style.less'
          }
      }
    },
    cssmin: {
        options: {
            banner: '/*! <%= pkg.name %> v<%= pkg.version %>, <%= grunt.template.today("yyyy-mm-dd") %> */\n'
        },
        combine: {
            files: {
                'static/css/style.css': [
                    'static/css/style.css',
                    'bower_components/select2/select2.css'
//                    'bower_components/angular-ui-select/dist/select.css'
                ]
            }
        }
    },
    coffee: {
        compile: {
            files: {
                'static/js/booking.js': [
                    'assets/coffee/booking.coffee'
                ],
                'static/js/booking2.js': [
                    'assets/coffee/booking2.coffee',
                    'assets/coffee/booking2app.coffee',
                    'assets/coffee/booking2controllers.coffee',
                    'assets/coffee/booking2filters.coffee',
                    'assets/coffee/booking2directives.coffee'
                ]
            }
        }
    },
    concat: {
        options: {
        },
        js: {
            src: [
//                'bower_components/jquery/dist/jquery.js',
                'bower_components/jqueryui/ui/core.js',
//                'bower_components/jqueryui/ui/effect.js',
                'bower_components/jqueryui/ui/widget.js',
                'bower_components/jqueryui/ui/mouse.js',
                'bower_components/jqueryui/ui/draggable.js',
                'bower_components/jqueryui/ui/datepicker.js',
//                'bower_components/jqueryui/ui/tabs.js',
//                'bower_components/fancybox/source/jquery.fancybox.js',
//                'bower_components/jquery-selectBox/jquery.selectBox.min.js',
//                'bower_components/jquery-icheck/icheck.js',
//                'bower_components/jquery-form/jquery.form.js',
//                'bower_components/iosslider/_src/jquery.iosslider.min.js',
//                'bower_components/bootstrap/js/transition.js',
//                'bower_components/bootstrap/js/collapse.js',
//                'bower_components/bootstrap/js/dropdown.js',
//                'bower_components/bootstrap/js/tab.js',
//                'bower_components/bootstrap/js/tooltip.js',
                'assets/js/script.js'
            ],
            dest: 'static/js/script.js'
        },
        ui: {
            src: [
                'bower_components/jqueryui/ui/core.js',
                'bower_components/jqueryui/ui/effect.js',
                'bower_components/jqueryui/ui/widget.js',
                'bower_components/jqueryui/ui/tabs.js',
            ],
            dest: 'static/js/jquery-ui.min.js'
        },
        booking: {
            src: [
//                'bower_components/underscore/underscore.js',
//                'bower_components/angular/angular.js',
//                'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                'bower_components/lodash/dist/lodash.js',
                'bower_components/select2/select2.js',
//                'bower_components/angular-ui-select2/src/select2.js',
                'bower_components/jquery-icheck/icheck.js',
//                'bower_components/iosslider/_src/jquery.iosslider.js',
                'bower_components/moment/moment.js',
                'static/js/booking.js'
            ],
            dest: 'static/js/booking.js'
        },
        booking2: {
            src: [
                'bower_components/angular/angular.js',
                'bower_components/lodash/dist/lodash.js',
                'bower_components/select2/select2.js',
                'bower_components/angular-ui-select2/src/select2.js',
                'bower_components/angular-ui-date/src/date.js',
                'bower_components/jquery-icheck/icheck.js',
                'bower_components/moment/moment.js',
                'static/js/booking2.js'
            ],
            dest: 'static/js/booking2.js'
        }
    },
    uglify: {
        options: {
            banner: '/*! <%= pkg.name %> v<%= pkg.version %>, <%= grunt.template.today("yyyy-mm-dd") %> */\n'
        },
        build: {
            src: 'static/js/script.js',
            dest: 'static/js/script.js'
        },
        pages: {
            expand: true,
            cwd: 'assets/js/pages/',
            src: '*.js',
            dest: 'static/js/'
        }
    },
    watch: {
        less: {
            files: 'assets/less/**/*.less',
            tasks: ['less', 'cssmin']
        },
        coffee: {
            files: 'assets/coffee/**/*.coffee',
            tasks: ['coffee', 'concat', 'uglify']
        },
        js: {
            files: 'assets/js/**/*.js',
            tasks: ['concat', 'uglify']
        },
        images: {
            files: 'assets/images/**/*',
            tasks: ['copy:images']
        }
    }
});

grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-less');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-coffee');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-contrib-copy');


grunt.registerTask('default', ['copy', 'less', 'cssmin', 'coffee', 'concat', 'uglify', 'watch']);

};
