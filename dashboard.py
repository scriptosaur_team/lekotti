"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'lekotti.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'lekotti.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from django.utils.text import capfirst
from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name, AppListElementMixin


class RecentComments(modules.ModelList):
    def __init__(self, **kwargs):
        super(RecentComments, self).__init__(**kwargs)
        self.template = 'admin_tools/dashboard/modules/recent_comments.html'
        self.message = kwargs.get('message', '')

    def init_with_context(self, context):
        if self._initialized:
            return
        items = self._visible_models(context['request'])
        if not items:
            return
        for model, perms in items:
            model_dict = {}
            model_dict['title'] = capfirst(model._meta.verbose_name_plural)
            if perms['change']:
                model_dict['change_url'] = self._get_admin_change_url(model, context)
            if perms['add']:
                model_dict['add_url'] = self._get_admin_add_url(model, context)
            self.children.append(model_dict)
        if self.extra:
            for extra_url in self.extra:
                model_dict = {}
                model_dict['title'] = extra_url['title']
                model_dict['change_url'] = extra_url['change_url']
                model_dict['add_url'] = extra_url.get('add_url', None)
                self.children.append(model_dict)
        self._initialized = True


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for lekotti.
    """
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.ModelList(
            title=_('static site content'),
            models=[
                'main.models.ExtendFlatPage',
                'chunks.models.Chunk',
                'chunks.models.Image',
                'tablefield.models.Table',
            ]
        ))

        self.children.append(modules.Group(
            title=_('dynamic site content'),
            children=[
                modules.ModelList(
                    title=_('Banners'),
                    models=[
                        'main.models.BannerIcon',
                        'main.models.Banner',
                        'main.models.InnerBanner',
                    ]
                ),
                modules.ModelList(
                    title=_('Habitation'),
                    models=[
                        'habitation.models.Habitation',
                        'habitation.models.BookingProgram',
                        'habitation.models.Link',
                    ]
                ),
                modules.ModelList(
                    title=_('Booking'),
                    models=[
                        'booking.models.AccommodationType',
                        'booking.models.AdditionalOption',
                        'booking.models.PaymentTerm',
                        'booking.models.Booking',
                        'booking.models.RateCard',
                        'booking.models.PackagePriceList',
                        'booking.models.BookingPrice',
                        'booking.models.BookingPackagePrice',
                    ]
                ),
                modules.ModelList(
                    title=_('ski slope'),
                    models=[
                        'skislope.models.Schedule',
                        'skislope.models.Reschedule',
                        'skislope.models.PriceType',
                    ]
                ),
                modules.ModelList(
                    title=_('bus'),
                    models=[
                        'contact.models.BusRun',
                        'contact.models.BusStop',
                    ]
                ),
                modules.ModelList(
                    title=_('other'),
                    models=[
                        'events.models.Event',
                        'activity.models.Activity',
                        'interesting.models.Interesting',
                    ]
                ),
            ]
        ))
        # self.children.append(modules.ModelList(
        #     title=_('dynamic site content'),
        #     models=[
        #         'main.models.Banner',
        #         'main.models.InnerBanner',
        #         'habitation.models.Habitation',
        #         'habitation.models.BookingProgram',
        #         'skislope.models.WorkTime',
        #         'skislope.models.Reschedule',
        #         'events.models.Event',
        #         'activity.models.Activity',
        #         'interesting.models.Interesting',
        #         'contact.models.BusRun',
        #         'contact.models.BusStop',
        #     ]
        # ))
        #
        self.children.append(RecentComments(
            title=_('Recent comments'),
            models=[
                'main.models.PageComment',
                'events.models.EventComment',
                'activity.models.ActivityComment',
                'interesting.models.PlaceComment',
            ]
        ))

        self.children.append(modules.Group(
            title=_('Administration'),
            display='tabs',
            children=[
                modules.ModelList(
                    title=_('Site options'),
                    models=[
                        'siteoptions.models.Season',
                        'main.models.RobotsTXTFile',
                        'email_templates.models.EmailTemplate',
                    ]
                ),
                modules.ModelList(
                    title=_('Sitetree'),
                    models=['sitetree.models.Tree']
                ),
                modules.Group(
                    _('languages'),
                    display='stacked',
                    children=[
                        modules.ModelList(
                            models=['main.models.Language']
                        ),
                        modules.LinkList(
                            _('for translators'),
                            children=[
                                [_('templates translate interface'), '/admin/translate'],
                            ]
                        ),
                    ]
                ),
                modules.AppList(
                    title=_('access control'),
                    models=[
                        'django.contrib.auth.models.User',
                        'django.contrib.auth.models.Group',
                        'allauth.*',
                    ]
                ),
            ]
        ))


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for lekotti.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
