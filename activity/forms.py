from django import forms
from .models import ActivityComment


class CommentForm(forms.ModelForm):
    class Meta:
        model = ActivityComment
        fields = ['message']