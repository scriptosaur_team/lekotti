from modeltranslation.translator import translator, TranslationOptions
from .models import Activity


class ActivityTranslationOptions(TranslationOptions):
    fields = ('name', 'short_description', 'description')


translator.register(Activity, ActivityTranslationOptions)