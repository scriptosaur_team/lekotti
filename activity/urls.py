from django.conf.urls import patterns, url
from .views import ActivityDetailView

urlpatterns = patterns('',
                       url(r'^$', 'activity.views.home', name='home'),
                       url(r'^(?P<slug>\w+)/$', ActivityDetailView.as_view(), name='detail'),
                       )