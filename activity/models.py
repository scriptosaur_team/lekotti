from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField, get_thumbnail
from sorl.thumbnail.helpers import ThumbnailError
from main.models import Comment, SEOFields
from cked.fields import RichTextField
from django_resized import ResizedImageField


class Activity(SEOFields, models.Model):
    name = models.CharField(_('name'), max_length=512)
    slug = models.SlugField(_('slug'), unique=True, null=True, blank=True)
    ordering = models.IntegerField(_('ordering'), default=100)
    image = ResizedImageField(max_width=1000, max_height=1000, upload_to='activity', verbose_name=_('main image'))
    main = models.BooleanField(_('show on main page'), default=True)
    winter = models.BooleanField(_('active in winter'), default=True)
    summer = models.BooleanField(_('active in summer'), default=True)
    short_description = models.TextField(_('short description'), blank=True, null=True)
    description = RichTextField(_('description'), blank=True, null=True)
    show_comments = models.BooleanField(_('enable comments'), default=False)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('activity')
        verbose_name_plural = _('activities')

    def __unicode__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('activity:detail', args=[str(self.slug)])


class ActivityPhoto(models.Model):
    image = ImageField(_('image'), upload_to='event_gallery')
    ordering = models.IntegerField(_('ordering'), default=100)
    event = models.ForeignKey(Activity)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('photo gallery')
        verbose_name_plural = _('photo gallery')


class ActivityComment(Comment):
    activity = models.ForeignKey(Activity, verbose_name=_('activity'))

    class Meta(Comment.Meta):
        verbose_name = _('activity comment')
        verbose_name_plural = _('activity comments')