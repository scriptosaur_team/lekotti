from django import template
from ..models import Activity

register = template.Library()


@register.inclusion_tag('activity/activity_announce.html')
def activity_announce():
    activity = Activity.objects.filter(main=True)
    return {'activity': activity}