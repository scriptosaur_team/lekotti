# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Activity.show_comments'
        db.add_column(u'activity_activity', 'show_comments',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


        # Changing field 'Activity.image'
        db.alter_column(u'activity_activity', 'image', self.gf('django_resized.forms.ResizedImageField')(max_length=100, max_width=1000))

    def backwards(self, orm):
        # Deleting field 'Activity.show_comments'
        db.delete_column(u'activity_activity', 'show_comments')


        # Changing field 'Activity.image'
        db.alter_column(u'activity_activity', 'image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100))

    models = {
        u'activity.activity': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Activity'},
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1000'}),
            'main': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'short_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'show_comments': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'summer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'winter': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'activity.activityphoto': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'ActivityPhoto'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['activity.Activity']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['activity']