# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Activity.name_ru'
        db.add_column(u'activity_activity', 'name_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.name_en'
        db.add_column(u'activity_activity', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.name_fi'
        db.add_column(u'activity_activity', 'name_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.short_description_ru'
        db.add_column(u'activity_activity', 'short_description_ru',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.short_description_en'
        db.add_column(u'activity_activity', 'short_description_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.short_description_fi'
        db.add_column(u'activity_activity', 'short_description_fi',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.description_ru'
        db.add_column(u'activity_activity', 'description_ru',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.description_en'
        db.add_column(u'activity_activity', 'description_en',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Activity.description_fi'
        db.add_column(u'activity_activity', 'description_fi',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Activity.name_ru'
        db.delete_column(u'activity_activity', 'name_ru')

        # Deleting field 'Activity.name_en'
        db.delete_column(u'activity_activity', 'name_en')

        # Deleting field 'Activity.name_fi'
        db.delete_column(u'activity_activity', 'name_fi')

        # Deleting field 'Activity.short_description_ru'
        db.delete_column(u'activity_activity', 'short_description_ru')

        # Deleting field 'Activity.short_description_en'
        db.delete_column(u'activity_activity', 'short_description_en')

        # Deleting field 'Activity.short_description_fi'
        db.delete_column(u'activity_activity', 'short_description_fi')

        # Deleting field 'Activity.description_ru'
        db.delete_column(u'activity_activity', 'description_ru')

        # Deleting field 'Activity.description_en'
        db.delete_column(u'activity_activity', 'description_en')

        # Deleting field 'Activity.description_fi'
        db.delete_column(u'activity_activity', 'description_fi')


    models = {
        u'activity.activity': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Activity'},
            'description': ('cked.fields.RichTextField', [], {}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'short_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'summer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'winter': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        }
    }

    complete_apps = ['activity']