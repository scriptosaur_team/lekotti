from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from main.utils import get_season
from .models import *
import datetime
from django.shortcuts import redirect
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView
from .forms import CommentForm


def home(request):
    season = get_season(request)
    activities = Activity.objects.filter(**{season: True})
    return render_to_response('activity/home.html', {'activities': activities},
                              context_instance=RequestContext(request))


def detail(request, pk):
    activity = get_object_or_404(Activity, pk=pk)
    return render_to_response('activity/detail.html', {'activity': activity}, context_instance=RequestContext(request))


class ActivityDetailView(DetailView, FormView):
    form_class = CommentForm
    model = Activity
    template_name = 'activity/detail.html'
    context_object_name = 'activity'

    def get_context_data(self, **kwargs):
        context = super(ActivityDetailView, self).get_context_data(**kwargs)
        context['comments'] = ActivityComment.objects.filter(activity=self.get_object())
        context['form'] = CommentForm
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = CommentForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.activity = self.object
            f.add_time = datetime.datetime.now()
            f.author = request.user
            f.save()
            if request.is_ajax():
                self.template_name = 'main/comment_created.html'
                ctx = self.get_context_data(form=form)
                ctx.update({'comment': f})
                return self.render_to_response(ctx)
            else:
                return redirect(self.object)
        else:
            return self.render_to_response(self.get_context_data(form=form))