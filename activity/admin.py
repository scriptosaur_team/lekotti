from django.contrib import admin
from main.utils import TranslationTabMixin
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.admin import AdminImageMixin
from .models import *


class GalleryInline(AdminImageMixin, admin.TabularInline):
    model = ActivityPhoto
    extra = 8


class ActivityAdmin(AdminImageMixin, TranslationTabMixin):
    list_display = ['name', 'slug', 'winter', 'summer', 'ordering', 'main']
    list_editable = ['winter', 'summer', 'ordering', 'main', 'slug']
    inlines = [GalleryInline]
    fieldsets = (
        (None, {'fields': ['name', 'slug', 'image', 'short_description', 'description']}),
        (_('view options'), {'classes': ('collapse',),
                             'fields': ['main', 'winter', 'summer', 'show_comments', 'ordering']}),
        (_('SEO winter'), {'classes': ('collapse',),
                           'fields': ['title_winter', 'meta_description_winter', 'meta_keywords_winter']}),
        (_('SEO summer'), {'classes': ('collapse',),
                           'fields': ['title_summer', 'meta_description_summer', 'meta_keywords_summer']}),
    )


class ActivityCommentAdmin(admin.ModelAdmin):
    list_display = ['message', 'author', 'add_time']
    list_filter = ['activity']
    ordering = ['-add_time']


admin.site.register(Activity, ActivityAdmin)
admin.site.register(ActivityComment, ActivityCommentAdmin)