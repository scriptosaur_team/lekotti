# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AccommodationType'
        db.create_table(u'booking_accommodationtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('object_number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('guests_number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('adults_number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('photo', self.gf('django_resized.forms.ResizedImageField')(max_length=100, max_width=1600, max_height=1200)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal(u'booking', ['AccommodationType'])


    def backwards(self, orm):
        # Deleting model 'AccommodationType'
        db.delete_table(u'booking_accommodationtype')


    models = {
        u'booking.accommodationtype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AccommodationType'},
            'adults_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'guests_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'object_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'photo': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1600', 'max_height': '1200'})
        }
    }

    complete_apps = ['booking']