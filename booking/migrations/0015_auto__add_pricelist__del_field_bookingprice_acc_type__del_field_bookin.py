# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PriceList'
        db.create_table(u'booking_pricelist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('acc_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.AccommodationType'])),
            ('conditions', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.PaymentTerm'])),
        ))
        db.send_create_signal(u'booking', ['PriceList'])

        # Deleting field 'BookingPrice.acc_type'
        db.delete_column(u'booking_bookingprice', 'acc_type_id')

        # Deleting field 'BookingPrice.condition'
        db.delete_column(u'booking_bookingprice', 'condition')

        # Adding field 'BookingPrice.price_list'
        db.add_column(u'booking_bookingprice', 'price_list',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['booking.PriceList']),
                      keep_default=False)

        # Adding field 'BookingPrice.nights_count'
        db.add_column(u'booking_bookingprice', 'nights_count',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'PriceList'
        db.delete_table(u'booking_pricelist')


        # User chose to not deal with backwards NULL issues for 'BookingPrice.acc_type'
        raise RuntimeError("Cannot reverse this migration. 'BookingPrice.acc_type' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'BookingPrice.acc_type'
        db.add_column(u'booking_bookingprice', 'acc_type',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.AccommodationType']),
                      keep_default=False)

        # Adding field 'BookingPrice.condition'
        db.add_column(u'booking_bookingprice', 'condition',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1),
                      keep_default=False)

        # Deleting field 'BookingPrice.price_list'
        db.delete_column(u'booking_bookingprice', 'price_list_id')

        # Deleting field 'BookingPrice.nights_count'
        db.delete_column(u'booking_bookingprice', 'nights_count')


    models = {
        u'booking.accommodationtype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AccommodationType'},
            'adults_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'default_price': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '100'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'guests_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'object_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'photo': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1600', 'max_height': '1200'})
        },
        u'booking.additionaloption': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AdditionalOption'},
            'accommodation_type': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'accommodation type'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['booking.AccommodationType']"}),
            'active_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'active_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'booking.booking': {
            'Meta': {'ordering': "['-add_time']", 'object_name': 'Booking'},
            'acc_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.AccommodationType']"}),
            'add_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'adults': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'check_in_time': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'children': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'date_check_in': ('django.db.models.fields.DateField', [], {}),
            'date_check_out': ('django.db.models.fields.DateField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'extra_options': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['booking.AdditionalOption']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'object_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'payment_terms': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['booking.PaymentTerm']", 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'booking.bookingpackageprice': {
            'Meta': {'ordering': "['date_check_in']", 'object_name': 'BookingPackagePrice'},
            'acc_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.AccommodationType']"}),
            'date_check_in': ('django.db.models.fields.DateField', [], {}),
            'date_check_out': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'booking.bookingprice': {
            'Meta': {'ordering': "['date']", 'object_name': 'BookingPrice'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nights_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'price_list': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.PriceList']"})
        },
        u'booking.paymentterm': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'PaymentTerm'},
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'booking.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'acc_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.AccommodationType']"}),
            'conditions': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.PaymentTerm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['booking']