# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Booking'
        db.create_table(u'booking_booking', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('add_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('acc_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.AccommodationType'])),
            ('date_check_in', self.gf('django.db.models.fields.DateField')()),
            ('date_check_out', self.gf('django.db.models.fields.DateField')()),
            ('adults', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('children', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('check_in_time', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('note', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'booking', ['Booking'])

        # Adding M2M table for field extra_options on 'Booking'
        m2m_table_name = db.shorten_name(u'booking_booking_extra_options')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('booking', models.ForeignKey(orm[u'booking.booking'], null=False)),
            ('additionaloption', models.ForeignKey(orm[u'booking.additionaloption'], null=False))
        ))
        db.create_unique(m2m_table_name, ['booking_id', 'additionaloption_id'])

        # Adding M2M table for field payment_terms on 'Booking'
        m2m_table_name = db.shorten_name(u'booking_booking_payment_terms')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('booking', models.ForeignKey(orm[u'booking.booking'], null=False)),
            ('paymentterm', models.ForeignKey(orm[u'booking.paymentterm'], null=False))
        ))
        db.create_unique(m2m_table_name, ['booking_id', 'paymentterm_id'])


    def backwards(self, orm):
        # Deleting model 'Booking'
        db.delete_table(u'booking_booking')

        # Removing M2M table for field extra_options on 'Booking'
        db.delete_table(db.shorten_name(u'booking_booking_extra_options'))

        # Removing M2M table for field payment_terms on 'Booking'
        db.delete_table(db.shorten_name(u'booking_booking_payment_terms'))


    models = {
        u'booking.accommodationtype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AccommodationType'},
            'adults_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'guests_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'object_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'photo': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1600', 'max_height': '1200'})
        },
        u'booking.additionaloption': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AdditionalOption'},
            'accommodation_type': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'accommodation type'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['booking.AccommodationType']"}),
            'active_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'active_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'booking.booking': {
            'Meta': {'ordering': "['-add_time']", 'object_name': 'Booking'},
            'acc_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.AccommodationType']"}),
            'add_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'adults': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'check_in_time': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'children': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'date_check_in': ('django.db.models.fields.DateField', [], {}),
            'date_check_out': ('django.db.models.fields.DateField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'extra_options': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['booking.AdditionalOption']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'payment_terms': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['booking.PaymentTerm']", 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'booking.paymentterm': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'PaymentTerm'},
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['booking']