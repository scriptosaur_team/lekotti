# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PaymentTerm'
        db.create_table(u'booking_paymentterm', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('description', self.gf('cked.fields.RichTextField')(null=True, blank=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal(u'booking', ['PaymentTerm'])


    def backwards(self, orm):
        # Deleting model 'PaymentTerm'
        db.delete_table(u'booking_paymentterm')


    models = {
        u'booking.accommodationtype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AccommodationType'},
            'adults_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'guests_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'object_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'photo': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1600', 'max_height': '1200'})
        },
        u'booking.additionaloption': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AdditionalOption'},
            'accommodation_type': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'accommodation type'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['booking.AccommodationType']"}),
            'active_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'active_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'booking.paymentterm': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'PaymentTerm'},
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['booking']