# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'AccommodationType.name_fi'
        db.add_column(u'booking_accommodationtype', 'name_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AccommodationType.name_ru'
        db.add_column(u'booking_accommodationtype', 'name_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AccommodationType.name_en'
        db.add_column(u'booking_accommodationtype', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AccommodationType.description'
        db.add_column(u'booking_accommodationtype', 'description',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AccommodationType.description_fi'
        db.add_column(u'booking_accommodationtype', 'description_fi',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AccommodationType.description_ru'
        db.add_column(u'booking_accommodationtype', 'description_ru',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AccommodationType.description_en'
        db.add_column(u'booking_accommodationtype', 'description_en',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'AccommodationType.name_fi'
        db.delete_column(u'booking_accommodationtype', 'name_fi')

        # Deleting field 'AccommodationType.name_ru'
        db.delete_column(u'booking_accommodationtype', 'name_ru')

        # Deleting field 'AccommodationType.name_en'
        db.delete_column(u'booking_accommodationtype', 'name_en')

        # Deleting field 'AccommodationType.description'
        db.delete_column(u'booking_accommodationtype', 'description')

        # Deleting field 'AccommodationType.description_fi'
        db.delete_column(u'booking_accommodationtype', 'description_fi')

        # Deleting field 'AccommodationType.description_ru'
        db.delete_column(u'booking_accommodationtype', 'description_ru')

        # Deleting field 'AccommodationType.description_en'
        db.delete_column(u'booking_accommodationtype', 'description_en')


    models = {
        u'booking.accommodationtype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AccommodationType'},
            'adults_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'guests_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'object_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'photo': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1600', 'max_height': '1200'})
        }
    }

    complete_apps = ['booking']