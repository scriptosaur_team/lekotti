# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PaymentTerm.name_fi'
        db.add_column(u'booking_paymentterm', 'name_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PaymentTerm.name_ru'
        db.add_column(u'booking_paymentterm', 'name_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PaymentTerm.name_en'
        db.add_column(u'booking_paymentterm', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PaymentTerm.description_fi'
        db.add_column(u'booking_paymentterm', 'description_fi',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PaymentTerm.description_ru'
        db.add_column(u'booking_paymentterm', 'description_ru',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PaymentTerm.description_en'
        db.add_column(u'booking_paymentterm', 'description_en',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PaymentTerm.name_fi'
        db.delete_column(u'booking_paymentterm', 'name_fi')

        # Deleting field 'PaymentTerm.name_ru'
        db.delete_column(u'booking_paymentterm', 'name_ru')

        # Deleting field 'PaymentTerm.name_en'
        db.delete_column(u'booking_paymentterm', 'name_en')

        # Deleting field 'PaymentTerm.description_fi'
        db.delete_column(u'booking_paymentterm', 'description_fi')

        # Deleting field 'PaymentTerm.description_ru'
        db.delete_column(u'booking_paymentterm', 'description_ru')

        # Deleting field 'PaymentTerm.description_en'
        db.delete_column(u'booking_paymentterm', 'description_en')


    models = {
        u'booking.accommodationtype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AccommodationType'},
            'adults_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'guests_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'object_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'photo': ('django_resized.forms.ResizedImageField', [], {'max_length': '100', 'max_width': '1600', 'max_height': '1200'})
        },
        u'booking.additionaloption': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'AdditionalOption'},
            'accommodation_type': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'accommodation type'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['booking.AccommodationType']"}),
            'active_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'active_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'booking.paymentterm': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'PaymentTerm'},
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['booking']