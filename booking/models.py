from django.db import models
from django.utils.translation import pgettext_lazy, ugettext_lazy as _
from django_resized import ResizedImageField
from cked.fields import RichTextField
from datetime import timedelta
from django.core.urlresolvers import reverse
import random
import hashlib


class AccommodationType(models.Model):
    name = models.CharField(_('name'), max_length=256)
    object_number = models.PositiveSmallIntegerField(_('object number'), default=1)
    guests_number = models.PositiveSmallIntegerField(_('max number of guests'), default=1)
    adults_number = models.PositiveSmallIntegerField(_('max number of adults'), default=1)
    photo = ResizedImageField(_('photo'), upload_to="booking", max_width=1600, max_height=1200)
    ordering = models.IntegerField(_('ordering'), default=100)
    description = RichTextField(_('description'), blank=True, null=True)
    default_price = models.PositiveSmallIntegerField(_('default price'), default=100, help_text=_('in Euros'))

    def __unicode__(self):
        return self.name

    def object_number_variants(self):
        return range(1, self.object_number+1)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('accommodation type')
        verbose_name_plural = _('accommodation types')


class AdditionalOption(models.Model):
    slug = models.SlugField(_('code'), help_text=_('for use in program code'))
    name = models.CharField(_('name'), max_length=256)
    description = RichTextField(_('description'), blank=True, null=True)
    accommodation_type = models.ManyToManyField(AccommodationType, related_name='accommodation type', blank=True,
                                                null=True)
    active_from = models.DateField(_('active from'), blank=True, null=True)
    active_to = models.DateField(_('active to'), blank=True, null=True)
    ordering = models.IntegerField(_('ordering'), default=100)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['ordering']
        verbose_name = _('additional option')
        verbose_name_plural = _('additional options')


class PaymentTerm(models.Model):
    slug = models.SlugField(_('code'), help_text=_('for use in program code'))
    name = models.CharField(_('name'), max_length=256)
    description = RichTextField(_('description'), blank=True, null=True)
    ordering = models.IntegerField(_('ordering'), default=100)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['ordering']
        verbose_name = _('payment term')
        verbose_name_plural = _('payment terms')


class Booking(models.Model):
    add_time = models.DateTimeField(_('add time'), auto_now_add=True)
    acc_type = models.ForeignKey(AccommodationType, verbose_name=_('accommodation type'))
    object_number = models.PositiveSmallIntegerField(_('object number'), default=1)
    date_check_in = models.DateField(_('date check in'))
    date_check_out = models.DateField(_('date check out'))
    adults = models.PositiveSmallIntegerField(_('adults'), default=1)
    children = models.PositiveSmallIntegerField(_('children'), default=0)
    extra_options = models.ManyToManyField(AdditionalOption,
                                           verbose_name=_('additional options'), blank=True, null=True)
    payment_terms = models.ManyToManyField(PaymentTerm, verbose_name=_('payment terms'), blank=True, null=True)
    price = models.FloatField(_('price'), blank=True, null=True)
    name = models.CharField(_('name'), max_length=512)
    email = models.EmailField(_('email'))
    phone = models.CharField(_('phone'), max_length=32)
    check_in_time = models.CharField(_('check in time'), max_length=128, null=True, blank=True)
    note = models.TextField(_('note'), blank=True)

    private_key = models.CharField(max_length=64, default=random.getrandbits(64), null=True, blank=True)

    cancelled = models.BooleanField(_('cancelled'), default=False)

    def __unicode__(self):
        return u'%s (%s - %s)' % (self.name, self.date_check_in, self.date_check_out)

    def get_public_key(self):
        hash_source = '|'.join([
            u'%s' % self.id,
            u'%s' % self.add_time,
            u'%s' % self.private_key
        ])
        return hashlib.md5(hash_source).hexdigest().upper()

    def test_key(self, key):
        return self.get_public_key() == key

    def get_cancel_url(self):
        return reverse('booking:cancel', args=[str(self.id), self.get_public_key()])

    class Meta:
        verbose_name = _('booking order')
        verbose_name_plural = _('booking orders')
        ordering = ['-add_time']


class RateCard(models.Model):
    name = models.CharField(_('name'), max_length=255, unique=True)
    nights_count = models.PositiveSmallIntegerField(_('min count of nights'), default=1)
    acc_type = models.ForeignKey(AccommodationType, verbose_name=_('accommodation type'))
    conditions = models.ForeignKey(PaymentTerm)
    description = models.TextField(_('description'), blank=True)

    def __unicode__(self):
        return u'%s' % self.name

    def get_description(self):
        return u'%s | %s | %s %s' % \
               (self.acc_type, self.conditions, self.nights_count, pgettext_lazy('rate card', 'nights'))

    def get_full_description(self):
        return u'%s (%s)' % (self.__unicode__(), self.get_description())

    class Meta:
        ordering = ['nights_count']
        verbose_name = _('rate card far single days')
        verbose_name_plural = _('rate cards far single days')


class PackagePriceList(models.Model):
    name = models.CharField(_('name'), max_length=512)
    acc_type = models.ForeignKey(AccommodationType, verbose_name=_('accommodation type'))
    conditions = models.ForeignKey(PaymentTerm, verbose_name=_('conditions'))
    description = models.TextField(_('description'), blank=True)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name = _('rate card (for packages)')
        verbose_name_plural = _('rate cards (for packages)')


class BookingPrice(models.Model):
    rate_card = models.ForeignKey(RateCard, verbose_name=_('rate card'))
    date = models.DateField(_('date'))
    price = models.PositiveIntegerField(_('price'), null=True)

    def __unicode__(self):
        return ''

    def get_rate_card_description(self):
        return u'%s' % self.rate_card.get_description()

    class Meta:
        verbose_name = _('price for separate day')
        verbose_name_plural = _('prices for separate days')
        ordering = ['date']


class BookingPackagePrice(models.Model):
    price_list = models.ForeignKey(PackagePriceList)
    date_check_in = models.DateField(_('date check in'))
    date_check_out = models.DateField(_('date check out'))
    price = models.PositiveIntegerField(_('price'), null=True, blank=True)

    def get_price(self):
        if self.price:
            return self.price
        else:
            price = 0
            days_count = int((self.date_check_out - self.date_check_in).days)
            for n in range(days_count):
                d = self.date_check_in + timedelta(n)
                s_price = BookingPrice.objects.filter(
                    rate_card__acc_type=self.price_list.acc_type,
                    date=d,
                    rate_card__nights_count__lte=days_count
                ).order_by('-rate_card__nights_count').first()
                if s_price:
                    price += s_price.price
                else:
                    return None
            return price
    get_price.short_description = _('price calculated')

    def get_days(self):
        return (self.date_check_out - self.date_check_in).days

    def __unicode__(self):
        return ''

    class Meta:
        verbose_name = _('package offer price')
        verbose_name_plural = _('package offer prices')
        ordering = ['date_check_in']