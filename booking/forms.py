from django import forms
from .models import Booking
from django.utils.translation import ugettext_lazy as _


class BookingAddForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BookingAddForm, self).__init__(*args, **kwargs)
        self.fields['acc_type'].empty_label = ''

    class Meta:
        model = Booking
        exclude = ['private_key', 'cancelled']
        widgets = {
            'object_number': forms.Select(),
            'adults': forms.HiddenInput(),
            'children': forms.HiddenInput(),
            'extra_options': forms.CheckboxSelectMultiple(),
        }


class BookingPriceImportForm(forms.Form):
    file = forms.FileField(label=_('File CSV'))


class BookingCancelForm(forms.ModelForm):
    public_key = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(BookingCancelForm, self).__init__(*args, **kwargs)
        self.fields['public_key'].initial = kwargs['instance'].get_public_key()

    class Meta:
        model = Booking
        fields = []