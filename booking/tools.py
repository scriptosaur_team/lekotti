import csv
import object_tools
import time
from django.utils.translation import ugettext as _
from django.http import HttpResponse
from django.shortcuts import redirect


class ExportBookingPrices(object_tools.ObjectTool):
    name = 'export_booking_prices'
    label = _('Export prices')

    def view(self, request, extra_context=None):
        response = HttpResponse(content_type='text/csv;charset=utf-8')
        response['Content-Disposition'] = 'attachment; filename="booking_prices_%s.csv"' %\
                                          time.strftime("%Y.%m.%d_%H.%M", time.localtime())
        writer = csv.writer(response)
        writer.writerow([
            _('Accommodation type'),
            _('Condition'),
            _('Date'),
            _('Price'),
        ])
        for p in self.model.objects.all():
            writer.writerow([
                p.acc_type_id,
                p.condition,
                p.date,
                p.price,
            ])

        return response

# object_tools.tools.register(ExportBookingPrices)


class ImportBookingPrices(object_tools.ObjectTool):
    name = 'import_booking_prices'
    label = _('Import prices')

    def view(self, request, extra_context=None):
        return redirect('admin:booking_prices_import')

# object_tools.tools.register(ImportBookingPrices)