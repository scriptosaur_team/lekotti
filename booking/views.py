from django.views.generic import TemplateView, CreateView, FormView, ListView, DetailView, UpdateView, View
from .models import *
from .forms import *
from datetime import date
from dateutil.relativedelta import relativedelta
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
import calendar
import csv
import time
import json
import hashlib
import base64
from django.http import HttpResponse
import dateutil.parser
from django.conf import settings
from email_templates.utils import send_templated_email
from django.shortcuts import get_object_or_404, redirect


class Booking2View(CreateView):
    template_name = 'booking/booking2.html'
    form_class = BookingAddForm

    def get_context_data(self, **kwargs):
        context = super(Booking2View, self).get_context_data(**kwargs)
        context['types'] = AccommodationType.objects.all()
        context['options'] = AdditionalOption.objects.all()
        context['payment_terms'] = PaymentTerm.objects.all()
        return context

    def form_valid(self, form):
        self.object = form.save()
        cancel_url = u'https://%s%s' % (
            self.request.META['HTTP_HOST'],
            self.object.get_cancel_url()
        )
        send_templated_email('new_order', {
            'data': self.object
        })
        send_templated_email('new_order_4client', {
            'data': self.object,
            'cancel_url': cancel_url
        }, form.cleaned_data['email'])
        return super(Booking2View, self).form_valid(form)

    def get_success_url(self):
        return reverse('booking:success', args=[str(self.object.id)])


class Booking2DataView(View):

    def get_context_data(self, **kwargs):
        context = super(Booking2DataView, self).get_context_data(**kwargs)
        context['precios'] = {}

        precios = BookingPrice.objects.all()
        for precio in precios:
            precio_tipo = int(precio.rate_card.acc_type_id)
            try:
                at = context['precios'][precio_tipo]
            except KeyError:
                at = context['precios'].update({precio_tipo: {}})

            precio_dt = precio.date.strftime("%Y.%m.%d")
            try:
                adt = context['precios'][precio_tipo][precio_dt]
            except KeyError:
                adt = context['precios'][precio_tipo].update({precio_dt: {}})

            precio_cond = int(precio.rate_card.conditions_id)
            try:
                apc = context['precios'][precio_tipo][precio_dt][precio_cond]
            except KeyError:
                apc = context['precios'][precio_tipo][precio_dt].update({precio_cond: {
                    'single': {},
                    'package': {},
                }})

            precio_dias_numero = int(precio.rate_card.nights_count)
            try:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['single'][precio_dias_numero]
            except KeyError:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['single']\
                    .update({precio_dias_numero: precio.price})

        paquetes_precios = BookingPackagePrice.objects.all()
        for precio in paquetes_precios:
            precio_tipo = int(precio.price_list.acc_type_id)
            try:
                at = context['precios'][precio_tipo]
            except KeyError:
                at = context['precios'].update({precio_tipo: {}})

            precio_dt = precio.date_check_in.strftime("%Y.%m.%d")
            try:
                adt = context['precios'][precio_tipo][precio_dt]
            except KeyError:
                adt = context['precios'][precio_tipo].update({precio_dt: {}})

            precio_cond = int(precio.price_list.conditions_id)
            try:
                apc = context['precios'][precio_tipo][precio_dt][precio_cond]
            except KeyError:
                apc = context['precios'][precio_tipo][precio_dt].update({precio_cond: {
                    'single': {},
                    'package': {},
                }})

            precio_paquete_dias_numero = int(precio.get_days())
            try:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['package'][precio_paquete_dias_numero]
            except KeyError:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['package']\
                    .update({precio_paquete_dias_numero: precio.get_price()})

        context['precios_json'] = json.dumps(context['precios'])
        return context

    def render_to_response(self, context, **response_kwargs):
        return super(Booking2DataView, self).render_to_response(
            context,
            mimetype='application/json',
            content_type='application/json',
            **response_kwargs)


class BookingView(CreateView):
    template_name = 'booking/booking.html'
    form_class = BookingAddForm

    def get_context_data(self, **kwargs):
        context = super(BookingView, self).get_context_data(**kwargs)
        context['types'] = AccommodationType.objects.all()
        context['options'] = AdditionalOption.objects.all()
        context['payment_terms'] = PaymentTerm.objects.all()
        precios = BookingPrice.objects.all()
        today = date.today()

        context['precios'] = {}

        for precio in precios:
            precio_tipo = int(precio.rate_card.acc_type_id)
            try:
                at = context['precios'][precio_tipo]
            except KeyError:
                at = context['precios'].update({precio_tipo: {}})

            precio_dt = precio.date.strftime("%Y.%m.%d")
            try:
                adt = context['precios'][precio_tipo][precio_dt]
            except KeyError:
                adt = context['precios'][precio_tipo].update({precio_dt: {}})

            precio_cond = int(precio.rate_card.conditions_id)
            try:
                apc = context['precios'][precio_tipo][precio_dt][precio_cond]
            except KeyError:
                apc = context['precios'][precio_tipo][precio_dt].update({precio_cond: {
                    'single': {},
                    'package': {},
                }})

            precio_dias_numero = int(precio.rate_card.nights_count)
            try:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['single'][precio_dias_numero]
            except KeyError:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['single']\
                    .update({precio_dias_numero: precio.price})

        paquetes_precios = BookingPackagePrice.objects.all()

        for precio in paquetes_precios:
            precio_tipo = int(precio.price_list.acc_type_id)
            try:
                at = context['precios'][precio_tipo]
            except KeyError:
                at = context['precios'].update({precio_tipo: {}})

            precio_dt = precio.date_check_in.strftime("%Y.%m.%d")
            try:
                adt = context['precios'][precio_tipo][precio_dt]
            except KeyError:
                adt = context['precios'][precio_tipo].update({precio_dt: {}})

            precio_cond = int(precio.price_list.conditions_id)
            try:
                apc = context['precios'][precio_tipo][precio_dt][precio_cond]
            except KeyError:
                apc = context['precios'][precio_tipo][precio_dt].update({precio_cond: {
                    'single': {},
                    'package': {},
                }})

            precio_paquete_dias_numero = int(precio.get_days())
            try:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['package'][precio_paquete_dias_numero]
            except KeyError:
                adn = context['precios'][precio_tipo][precio_dt][precio_cond]['package']\
                    .update({precio_paquete_dias_numero: precio.get_price()})

        context['precios_json'] = json.dumps(context['precios'])

        tipos = {int(tipo.id): tipo for tipo in context['types']}

        date_start = min([
            BookingPrice.objects.filter(date__gte=today).order_by('date').first().date,
            BookingPackagePrice.objects.filter(date_check_in__gte=today).order_by('date_check_in').first().date_check_in
        ])
        date_end = max([
            BookingPrice.objects.order_by('date').last().date,
            BookingPackagePrice.objects.order_by('date_check_out').last().date_check_out
        ])

        def month_days_list(month_date):
            days_list = [
                date(month_date.year, month_date.month, d)
                for d in range(1, calendar.monthrange(month_date.year, month_date.month)[1]+1)
            ]
            if month_date.year == today.year and month_date.month == today.month:
                days_list = days_list[today.day-1:]

            return {
                'info': month_date,
                'dias': days_list
            }

        def dia_info(dia, tipo):
            try:
                rate_card = RateCard.objects.filter(acc_type=tipo).first()
                price = BookingPrice.objects.filter(date=dia, rate_card=rate_card)[0].price
            except:
                price = None
            booking = Booking.objects.filter(acc_type=tipo, date_check_in__lte=dia, date_check_out__gte=dia)
            booked = 0
            for b in booking:
                booked += b.object_number

            try:
                price_lists = PackagePriceList.objects.filter(acc_type=tipo)
                packages = BookingPackagePrice.objects.filter(date_check_in=dia, price_list__in=price_lists)
                packages = [{
                    'id': p.id,
                    'days': p.get_days(),
                    'price': p.get_price(),
                    'date_check_in': p.date_check_in,
                    'date_check_out': p.date_check_out,
                    'conditions': p.price_list.conditions
                } for p in packages]
            except:
                packages = None

            return {
                'date': dia,
                'tomorrow': dia + relativedelta(days=1),
                'total': tipos[tipo.id].object_number,
                'libre': tipos[tipo.id].object_number - booked,
                'price': price,
                'packages': packages
            }

        def dias_info(month_date, tipo):
            dias_list = [
                dia_info(date(month_date.year, month_date.month, d), tipo)
                for d in range(1, calendar.monthrange(month_date.year, month_date.month)[1]+1)
            ]
            if month_date.year == today.year and month_date.month == today.month:
                dias_list = dias_list[today.day-1:]
            return dias_list

        def crear_mes_tipo(tipo, month_date):
            return {
                'tipo': tipo,
                'dias': dias_info(month_date, tipo)
            }

        def crear_mes(month_date):
            return {
                'info': month_date,
                'lista_dias': month_days_list(month_date),
                'por_tipo': [crear_mes_tipo(tipo, month_date) for tipo in context['types']]
            }

        context['listaDeLosMeses'] = []
        date_start_display = today
        while date_start_display < date_end + relativedelta(months=1):
            context['listaDeLosMeses'].append(crear_mes(date_start_display))
            date_start_display = date_start_display + relativedelta(months=1)
        return context

    def form_valid(self, form):
        self.object = form.save()
        cancel_url = u'https://%s%s' % (
            self.request.META['HTTP_HOST'],
            self.object.get_cancel_url()
        )
        print(cancel_url)
        send_templated_email('new_order', {
            'data': self.object
        })
        send_templated_email('new_order_4client', {
            'data': self.object,
            'cancel_url': cancel_url
        }, form.cleaned_data['email'])
        return super(BookingView, self).form_valid(form)

    def get_success_url(self):
        return reverse('booking:success', args=[str(self.object.id)])


class BookingSuccessView(DetailView):
    model = Booking
    template_name = 'booking/booking_success.html'
    context_object_name = 'order'

    def get_context_data(self, **kwargs):
        context = super(BookingSuccessView, self).get_context_data(**kwargs)
        context['MERCHANT_ID'] = settings.MERCHANT_ID
        context['PAYMENT_AMOUNT'] = u'%.2f' % self.object.price
        context['ORDER_NUMBER'] = u'%s' % self.object.id
        context['return_address'] = u'https://%s%s' % (
            self.request.META['HTTP_HOST'],
            reverse('booking:success', args=[str(self.object.id)])
        )
        hash_source = '|'.join([
            settings.MERCHANT_SECRET,
            settings.MERCHANT_ID,
            context['PAYMENT_AMOUNT'],
            context['ORDER_NUMBER'],
            u'',
            u'',
            u'EUR',
            context['return_address'],
            context['return_address'],
            u'',
            context['return_address'],
            u'S1',
            u'en_US',
            u'',
            u'1',
            u'',
            u'',
        ])
        context['AUTHCODE'] = hashlib.md5(hash_source).hexdigest().upper()
        return context


class BookingCancelView(UpdateView):
    model = Booking
    template_name = 'booking/booking_cancel.html'
    form_class = BookingCancelForm

    def dispatch(self, request, *args, **kwargs):
        return super(BookingCancelView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BookingCancelView, self).get_context_data(**kwargs)
        context['order'] = get_object_or_404(self.model, id=self.object.id)
        return context

    def form_valid(self, form):
        self.object.cancelled = True
        self.object.save()
        return redirect('/')


class BookingPricesExportView(ListView):

    model = BookingPrice

    def dispatch(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv;charset=utf-8')
        response['Content-Disposition'] = 'attachment; filename="booking_prices_%s.csv"' %\
                                          time.strftime("%Y.%m.%d_%H.%M", time.localtime())
        csv_delimiter = settings.CSV_DELIMITER or ';'
        writer = csv.writer(response, delimiter=csv_delimiter)
        writer.writerow([
            'Rate card',
            'Date',
            'Price',
        ])
        for p in self.model.objects.all():
            writer.writerow([
                p.rate_card,
                p.date,
                p.price,
            ])
        return response


class BookingPackagePricesExportView(ListView):

    model = BookingPackagePrice

    def dispatch(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv;charset=utf-8')
        response['Content-Disposition'] = 'attachment; filename="booking_package_prices_%s.csv"' %\
                                          time.strftime("%Y.%m.%d_%H.%M", time.localtime())
        csv_delimiter = settings.CSV_DELIMITER or ';'
        writer = csv.writer(response, delimiter=csv_delimiter)
        writer.writerow([
            'Rate card',
            'Date check in',
            'Date check out',
            'Price',
        ])
        for p in self.model.objects.all():
            writer.writerow([
                p.price_list,
                p.date_check_in,
                p.date_check_out,
                p.price,
            ])
        return response


class BookingPricesImportView(FormView):
    form_class = BookingPriceImportForm
    template_name = 'admin/booking/booking_prices_import.html'

    def get_success_url(self):
        return reverse('admin:booking_bookingprice_changelist')

    def get_context_data(self, **kwargs):
        context = super(BookingPricesImportView, self).get_context_data(**kwargs)
        context['app'] = BookingPrice._meta
        return context

    def form_valid(self, form):
        uploaded_data = self.request.FILES['file'].read()
        csv_delimiter = settings.CSV_DELIMITER or ';'
        reader = csv.reader(uploaded_data.split('\n')[1:], delimiter=csv_delimiter)
        for row in reader:
            try:
                rate_card, created = RateCard.objects.get_or_create(name=row[0])
                try:
                    new_price, created = BookingPrice.objects.get_or_create(
                        rate_card=rate_card,
                        date=dateutil.parser.parse(row[1]),
                    )
                except BookingPrice.MultipleObjectsReturned:
                    BookingPrice.objects.filter(
                        rate_card__name=row[0],
                        date=row[1],
                    ).all().delete()
                    new_price = BookingPrice(
                        rate_card=rate_card,
                        date=dateutil.parser.parse(row[1]),
                    )

                new_price.price = int(row[2])
                new_price.save()
            except:
                pass
        return super(BookingPricesImportView, self).form_valid(form)


class BookingPackagePricesImportView(FormView):
    form_class = BookingPriceImportForm
    template_name = 'admin/booking/booking_package_prices_import.html'

    def get_success_url(self):
        return reverse('admin:booking_bookingpackageprice_changelist')

    def get_context_data(self, **kwargs):
        context = super(BookingPackagePricesImportView, self).get_context_data(**kwargs)
        context['app'] = BookingPackagePrice._meta
        return context

    def form_valid(self, form):
        uploaded_data = self.request.FILES['file'].read()
        csv_delimiter = settings.CSV_DELIMITER or ';'
        reader = csv.reader(uploaded_data.split('\n')[1:], delimiter=csv_delimiter)
        for row in reader:
            try:
                rate_card, created = PackagePriceList.objects.get_or_create(name=row[0])
                try:
                    new_price, created = BookingPackagePrice.objects.get_or_create(
                        price_list=rate_card,
                        date_check_in=dateutil.parser.parse(row[1]),
                        date_check_out=dateutil.parser.parse(row[2]),
                    )
                except BookingPackagePrice.MultipleObjectsReturned:
                    BookingPrice.objects.filter(
                        price_list__name=row[0],
                        date_check_in=row[1],
                        date_check_out=row[2],
                    ).all().delete()
                    new_price = BookingPackagePrice(
                        price_list=rate_card,
                        date_check_in=dateutil.parser.parse(row[1]),
                        date_check_out=dateutil.parser.parse(row[2]),
                    )

                new_price.price = int(row[3])
                new_price.save()
            except:
                pass
        return super(BookingPackagePricesImportView, self).form_valid(form)