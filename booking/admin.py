from django.contrib import admin
from django.conf.urls import patterns, url
from .models import *
from .views import BookingPricesImportView, BookingPricesExportView, \
    BookingPackagePricesExportView, BookingPackagePricesImportView
from main.utils import TranslationTabMixin
import object_tools
from .tools import ExportBookingPrices, ImportBookingPrices
from django.utils.safestring import mark_safe
from daterange_filter.filter import DateRangeFilter


class PriceInline(admin.TabularInline):
    model = BookingPrice
    extra = 16


class AccommodationTypeAdmin(TranslationTabMixin):
    list_display = ['name', 'ordering', 'default_price']
    list_editable = ['ordering', 'default_price']


class AdditionalOptionAdmin(TranslationTabMixin):
    list_display = ['name', 'ordering']
    list_editable = ['ordering']


class PaymentTermsAdmin(TranslationTabMixin):
    list_display = ['name', 'ordering']
    list_editable = ['ordering']


class BookingPriceAdmin(admin.ModelAdmin):
    list_display = ['rate_card', 'get_rate_card_description', 'date', 'price']
    readonly_fields = ['get_rate_card_description']
    list_editable = ['price', 'date']
    list_filter = ['rate_card', ['date', DateRangeFilter]]

    def get_urls(self):
        urls = super(BookingPriceAdmin, self).get_urls()
        extra_urls = patterns('',
                              url(r'^export/$', self.admin_site.admin_view(BookingPricesExportView.as_view()),
                                  name='booking_prices_export'),
                              url(r'^import/$', self.admin_site.admin_view(BookingPricesImportView.as_view()),
                                  name='booking_prices_import'),
                              )
        return extra_urls + urls


class BookingPackagePriceAdmin(admin.ModelAdmin):
    list_display = ['price_list', 'date_check_in', 'date_check_out', 'price', 'get_price']
    list_editable = ['date_check_in', 'date_check_out', 'price']
    list_filter = ['price_list', ['date_check_in', DateRangeFilter]]

    def get_urls(self):
        urls = super(BookingPackagePriceAdmin, self).get_urls()
        extra_urls = patterns('',
                              url(r'^export/$', self.admin_site.admin_view(BookingPackagePricesExportView.as_view()),
                                  name='booking_package_prices_export'),
                              url(r'^import/$', self.admin_site.admin_view(BookingPackagePricesImportView.as_view()),
                                  name='booking_package_prices_import'),
                              )
        return extra_urls + urls


class BookingPriceInline(admin.TabularInline):
    model = BookingPrice


class BookingPackagePriceInline(admin.TabularInline):
    model = BookingPackagePrice
    fields = ['date_check_in', 'date_check_out', 'price', 'get_price']
    readonly_fields = ['get_price']


class BookingPackagePriceListAdmin(admin.ModelAdmin):
    list_display = ['name', 'acc_type', 'conditions']
    inlines = [BookingPackagePriceInline]


class RateCardAdmin(admin.ModelAdmin):
    list_display = ['name', 'acc_type', 'conditions', 'nights_count']
    list_filter = ['acc_type', 'conditions']


admin.site.register(AccommodationType, AccommodationTypeAdmin)
admin.site.register(AdditionalOption, AdditionalOptionAdmin)
admin.site.register(PaymentTerm, PaymentTermsAdmin)
admin.site.register(Booking)
admin.site.register(RateCard, RateCardAdmin)
admin.site.register(BookingPrice, BookingPriceAdmin)
admin.site.register(BookingPackagePrice, BookingPackagePriceAdmin)
admin.site.register(PackagePriceList, BookingPackagePriceListAdmin)