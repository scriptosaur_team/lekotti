from modeltranslation.translator import translator, TranslationOptions
from .models import AccommodationType, AdditionalOption, PaymentTerm


class AccommodationTypeTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


class AdditionalOptionTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


class PaymentTermsTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


translator.register(AccommodationType, AccommodationTypeTranslationOptions)
translator.register(AdditionalOption, AdditionalOptionTranslationOptions)
translator.register(PaymentTerm, PaymentTermsTranslationOptions)
