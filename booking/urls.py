from django.conf.urls import patterns, url
from .views import *


urlpatterns = patterns('',
                       url(r'^$', BookingView.as_view(), name='home'),
                       url(r'^new/$', Booking2View.as_view(), name='new'),
                       url(r'^new/data/$', Booking2DataView.as_view(), name='new_data'),
                       url(r'^success/(?P<pk>\d+)/$', BookingSuccessView.as_view(), name='success'),
                       url(r'^cancel/(?P<pk>\d+)/(?P<public_key>\w+)/$', BookingCancelView.as_view(), name='cancel'),
                       )