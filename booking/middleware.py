import re

from django.conf import settings

from django.http import HttpResponseRedirect
from django.utils.translation import get_language


class BookingSSLMiddleware:
    def __init__(self):
        self.BOOKING_USE_SSL = getattr(settings, 'BOOKING_USE_SSL', False)
        self.SECURE_SECTION_PATH = "^/%s/booking/" % get_language()
        self.SECURE_SECTION_PATH = re.compile(self.SECURE_SECTION_PATH, re.IGNORECASE)

    def secure_url(self, request):
        uri = 'https://{}{}'.format(
            request.get_host(),
            request.get_full_path(),
        )

        return uri

    def process_request(self, request):
        if self.BOOKING_USE_SSL and self.SECURE_SECTION_PATH.match(request.path) and not request.is_secure():
            return HttpResponseRedirect(self.secure_url(request))