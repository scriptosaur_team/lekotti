from django.contrib import admin
from main.utils import TranslationTabMixin
from .models import *


class HabitationAdmin(TranslationTabMixin):
    list_display = ['name', 'active', 'ordering']
    list_editable = ['active', 'ordering']


class BookingProgramAdmin(TranslationTabMixin):
    list_display = ['__unicode__', 'ordering', 'check_in_weekday', 'nights']
    list_editable = ['ordering', 'check_in_weekday', 'nights']


class PriceListAdmin(TranslationTabMixin):
    list_display = ['code', 'name', 'ordering']
    list_editable = ['ordering']


class LinkAdmin(TranslationTabMixin):
    list_display = ['title', 'link', 'new_tab', 'ordering']
    list_editable = ['new_tab', 'ordering']


admin.site.register(Habitation, HabitationAdmin)
admin.site.register(BookingProgram, BookingProgramAdmin)
admin.site.register(PriceList, PriceListAdmin)
admin.site.register(Link, LinkAdmin)