from django import template
from ..models import Habitation, BookingProgram, PriceList, Link
import datetime

register = template.Library()


@register.inclusion_tag('habitation/habitation_variants.html')
def habitation_variants():
    return {'habitation': Habitation.objects.all()}


@register.inclusion_tag('habitation/habitation_list.html')
def habitation_list():
    return {'habitation': Habitation.objects.all()}


@register.inclusion_tag('habitation/booking_programs.html')
def booking_programs():
    return {'booking_programs': BookingProgram.objects.all()}


@register.inclusion_tag('habitation/booking_widget.html')
def booking_widget(program=None):
    today = datetime.date.today()
    date_check_in = today + datetime.timedelta(days=1)
    date_check_out = date_check_in + datetime.timedelta(days=1)
    if program:
        if program.check_in_weekday is not None:
            date_check_in = today + datetime.timedelta((program.check_in_weekday-today.weekday()) % 7)
        if program.nights is not None:
            date_check_out = date_check_in + datetime.timedelta(days=program.nights)
    return {
        'program': program,
        'date_check_in': date_check_in,
        'date_check_out': date_check_out,
    }


@register.inclusion_tag('habitation/price_lists.html')
def price_lists():
    return {'price_lists': PriceList.objects.all()}


@register.inclusion_tag('habitation/aside_links.html')
def aside_links():
    return {'links': Link.objects.all()}