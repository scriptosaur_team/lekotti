# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Habitation.name_ru'
        db.add_column(u'habitation_habitation', 'name_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=24, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Habitation.name_en'
        db.add_column(u'habitation_habitation', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=24, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Habitation.name_fi'
        db.add_column(u'habitation_habitation', 'name_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=24, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Habitation.description_ru'
        db.add_column(u'habitation_habitation', 'description_ru',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Habitation.description_en'
        db.add_column(u'habitation_habitation', 'description_en',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Habitation.description_fi'
        db.add_column(u'habitation_habitation', 'description_fi',
                      self.gf('cked.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Habitation.name_ru'
        db.delete_column(u'habitation_habitation', 'name_ru')

        # Deleting field 'Habitation.name_en'
        db.delete_column(u'habitation_habitation', 'name_en')

        # Deleting field 'Habitation.name_fi'
        db.delete_column(u'habitation_habitation', 'name_fi')

        # Deleting field 'Habitation.description_ru'
        db.delete_column(u'habitation_habitation', 'description_ru')

        # Deleting field 'Habitation.description_en'
        db.delete_column(u'habitation_habitation', 'description_en')

        # Deleting field 'Habitation.description_fi'
        db.delete_column(u'habitation_habitation', 'description_fi')


    models = {
        u'habitation.habitation': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Habitation'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'description': ('cked.fields.RichTextField', [], {}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['habitation']