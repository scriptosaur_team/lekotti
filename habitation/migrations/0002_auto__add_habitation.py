# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Habitation'
        db.create_table(u'habitation_habitation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=24)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('description', self.gf('cked.fields.RichTextField')()),
        ))
        db.send_create_signal(u'habitation', ['Habitation'])


    def backwards(self, orm):
        # Deleting model 'Habitation'
        db.delete_table(u'habitation_habitation')


    models = {
        u'habitation.habitation': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Habitation'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'description': ('cked.fields.RichTextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['habitation']