# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PriceList.name_ru'
        db.add_column(u'habitation_pricelist', 'name_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PriceList.name_en'
        db.add_column(u'habitation_pricelist', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PriceList.name_fi'
        db.add_column(u'habitation_pricelist', 'name_fi',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PriceList.table_data_ru'
        db.add_column(u'habitation_pricelist', 'table_data_ru',
                      self.gf('tablefield.fields.TableField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PriceList.table_data_en'
        db.add_column(u'habitation_pricelist', 'table_data_en',
                      self.gf('tablefield.fields.TableField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PriceList.table_data_fi'
        db.add_column(u'habitation_pricelist', 'table_data_fi',
                      self.gf('tablefield.fields.TableField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PriceList.name_ru'
        db.delete_column(u'habitation_pricelist', 'name_ru')

        # Deleting field 'PriceList.name_en'
        db.delete_column(u'habitation_pricelist', 'name_en')

        # Deleting field 'PriceList.name_fi'
        db.delete_column(u'habitation_pricelist', 'name_fi')

        # Deleting field 'PriceList.table_data_ru'
        db.delete_column(u'habitation_pricelist', 'table_data_ru')

        # Deleting field 'PriceList.table_data_en'
        db.delete_column(u'habitation_pricelist', 'table_data_en')

        # Deleting field 'PriceList.table_data_fi'
        db.delete_column(u'habitation_pricelist', 'table_data_fi')


    models = {
        u'habitation.bookingprogram': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'BookingProgram'},
            'description': ('cked.fields.RichTextField', [], {}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'habitation.habitation': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Habitation'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'habitation.pricelist': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceList'},
            'code': ('django.db.models.fields.SlugField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'table_data': ('tablefield.fields.TableField', [], {}),
            'table_data_en': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_fi': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'}),
            'table_data_ru': ('tablefield.fields.TableField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['habitation']