# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BookingProgram'
        db.create_table(u'habitation_bookingprogram', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('cked.fields.RichTextField')()),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal(u'habitation', ['BookingProgram'])


    def backwards(self, orm):
        # Deleting model 'BookingProgram'
        db.delete_table(u'habitation_bookingprogram')


    models = {
        u'habitation.bookingprogram': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'BookingProgram'},
            'description': ('cked.fields.RichTextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        },
        u'habitation.habitation': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Habitation'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'description': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fi': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('cked.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '24', 'null': 'True', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '100'})
        }
    }

    complete_apps = ['habitation']