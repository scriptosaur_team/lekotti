from modeltranslation.translator import translator, TranslationOptions
from .models import Habitation, BookingProgram, PriceList, Link


class HabitationTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


translator.register(Habitation, HabitationTranslationOptions)


class BookingProgramTranslationOptions(TranslationOptions):
    fields = ('description',)


translator.register(BookingProgram, BookingProgramTranslationOptions)


class PriceListTranslationOptions(TranslationOptions):
    fields = ('name', 'table_data')


translator.register(PriceList, PriceListTranslationOptions)


class LinkTranslationOptions(TranslationOptions):
    fields = ('title',)


translator.register(Link, LinkTranslationOptions)