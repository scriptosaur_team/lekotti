from django.db import models
from django import forms
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField
from cked.fields import RichTextField
from django.utils.html import strip_tags
from tablefield.fields import TableField
from django_resized import ResizedImageField


class Habitation(models.Model):
    name = models.CharField(_('name'), max_length=24)
    slug = models.SlugField(_('slug'), null=True, blank=True)
    active = models.BooleanField(_('active'), default=True)
    ordering = models.IntegerField(_('ordering'), default=100)
#     image = ImageField(_('image'), upload_to='habitation')
    image = ResizedImageField(max_width=1000, upload_to='habitation', verbose_name=_('image'))
    description = RichTextField(_('description'), blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['ordering']
        verbose_name = _('habitation variant')
        verbose_name_plural = _('habitation variants')


class BookingProgram(models.Model):
    WEEKDAY_CHOICES = (
        (0, _('monday')),
        (1, _('tuesday')),
        (2, _('wednesday')),
        (3, _('thursday')),
        (4, _('friday')),
        (5, _('saturday')),
        (6, _('sunday')),
    )
    description = RichTextField(_('description'))
    ordering = models.IntegerField(_('ordering'), default=100)
    check_in_weekday = models.PositiveSmallIntegerField(_('check in weekday'),
                                                        choices=WEEKDAY_CHOICES, blank=True, null=True)
    nights = models.PositiveSmallIntegerField(_('number of nights'), blank=True, null=True)

    def __unicode__(self):
        return u'%s' % strip_tags(self.description)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('booking program')
        verbose_name_plural = _('booking programs')


class PriceList(models.Model):
    name = models.CharField(_('title'), max_length=256, blank=True, null=True)
    code = models.SlugField(_('slug'), max_length=32, help_text=_('for use in templates'))
    ordering = models.IntegerField(_('ordering'), default=100)
    table_data = TableField(_('table'),
                            help_text=_('Click on cell to edit, right-click to ad or remove columns and rows.<br>'
                                        'First row contains table header'))

    def __unicode__(self):
        return u'%s [%s]' % (self.name, self.code)

    class Meta:
        verbose_name = _('price list')
        verbose_name_plural = _('price lists')
        ordering = ['ordering']


class Link(models.Model):
    title = models.CharField(_('title'), max_length=32)
    link = models.CharField(_('link'), max_length=128)
    new_tab = models.BooleanField(_('open in new tab'), default=False)
    ordering = models.IntegerField(_('ordering'), default=100)

    def __unicode__(self):
        return u'%s [%s]' % (self.title, self.link)

    class Meta:
        ordering = ['ordering']
        verbose_name = _('aside link')
        verbose_name_plural = _('aside links')