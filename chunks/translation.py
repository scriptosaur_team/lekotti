from modeltranslation.translator import translator, TranslationOptions
from .models import Chunk


class ChunkTranslationOptions(TranslationOptions):
    fields = ('content',)


translator.register(Chunk, ChunkTranslationOptions)