$(function(){
    var FixedColumn = {
        $column: $('.fixed-column'),
        $cPlace: $('.pagecontent'),
        cOffset: 22,
        getDims: function(){
            this.cHeight = this.$column.outerHeight()
            this.cpHeight = this.$cPlace.outerHeight()
            this.t0 = this.$cPlace.offset().top + this.cOffset
            this.t1 = this.cpHeight - this.cHeight
            this.t2 = this.t1 + this.t0
        },
        scrollHandler: function(){
            var app = this
            if(app.t1<0){
                 app.$column.removeClass('fixed fixed-bottom').css({top:0})
            } else {
                app.scrollTop = $(window).scrollTop()
                if(app.scrollTop>app.t2){
                    app.$column.addClass('fixed-bottom').removeClass('fixed').css({top:app.t1})
                }else if(app.scrollTop>app.t0){
                    app.$column.addClass('fixed').removeClass('fixed-bottom').css({top:0})
                }else{
                    app.$column.removeClass('fixed fixed-bottom').css({top:0})
                }
            }
        },
        Init: function(){
            var app = this
            app.getDims()
            app.scrollHandler()
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(){app.getDims()})
            $(window).scroll(function(){app.scrollHandler()})
            $(window).resize(function(){app.getDims()})
        }
    }
    FixedColumn.Init()
})