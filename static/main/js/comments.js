$(function(){
    function CheckAuth(){
        $('.fancybox-inner form').ajaxForm({
            error: function(data){
                $('.fancybox-inner').html(data.responseJSON.html).height('auto')
                CheckAuth()
            },
            success: function(data){
                $('#comments').removeClass('not-auth')
                $.fancybox.close()
            }
        })
    }
    function CommentFormsHandle($forms){
        $forms.ajaxForm({
            beforeSubmit: function(arr, $form, options){
            	var $msg = $form.find('.form-group.comment_text'),
            		msg = $.trim($form.find('#id_message').val())
            	if(msg.length<=0){
            		$msg.addClass('has-error')
            		$msg.find('.no-text-label').removeClass('hidden')
            		return false
            	}
                if($("#comments").hasClass("not-auth")){
			        $.cookie('comment_form_id', $form.prop('id'), { path: '/' })
			        $.cookie('comment_page', window.location.href, { path: '/' })
			        $.cookie('comment_message', msg, { path: '/' })
                	window.location.href = "/accounts/login/"
                	return false
                    var $thisForm = $form
                    $.fancybox({
                        href: $("#auth-link").prop("href")||"/accounts/login/",
                        type: "ajax",
                        minWidth: 600,
                        transitionIn  : 'elastic',
                        transitionOut : 'elastic',
                        scrolling : 'no',
                        preload   : true,
                        autoDimensions : false,
                        padding: [50,64,60,64],
                        helpers: {
                            overlay: {
                                locked: false,
                                css : { 'overflow' : 'no' }
                            }
                        },
                        afterShow: function(){
                            $('.fancybox-inner').on('click', 'a', function(e){
                                e.preventDefault()
                                var url = $(this).prop('href')
                                $('.fancybox-inner')
                                    .addClass('load')
                                    .load(url, function(){
                                        $('.fancybox-inner').removeClass('load')
                                        CheckAuth()
                                    })
                            })
                            CheckAuth()
                        },
                        afterClose: function(){
                            if(!$('#comments').hasClass('not-auth')){
                                $thisForm.find('[name=csrfmiddlewaretoken]').val($.cookie('csrftoken'))
                                $thisForm.find('[type=submit]').click()
                            }
                        }
                    })
                    return false
                }
            },
            success: function(responseText, statusText, xhr, $form){
                var $commentCreated = $(responseText)
                var cID = parseInt($form.find('[name=parent]').val())
                var $aComment
                $form.find('textarea').val('')
                if(cID>0){
                    $aComment = $('#comments ul.comments [data-id='+cID+']')
                    $form.hide()
                }else{
                    $aComment = $('#comments ul.comments li').last()
                }
                if($aComment.size()==1){
	                $commentCreated
	                    .css({opacity:0})
	                    .insertAfter($aComment)
	                    .animate({opacity: 1}, {duration:400})
                }else{
	                $commentCreated
	                    .css({opacity:0})
	                    .animate({opacity: 1}, {duration:400})
	                    .appendTo("#comments ul.comments")
                }
                CommentFormsHandle($commentCreated.find('form'))
            }
        })
    }
    CommentFormsHandle($("#comments form"))
    $('#comments').on('click', '.reply', function(e){
        e.preventDefault();
        var $commentInner = $(this).parent().siblings(".comment_inner")
        $commentInner.stop().slideToggle(function(){
            if($commentInner.is(':visible')) $commentInner.find('textarea').focus()
        });
    });
    $('#auth-link').click(function(e){
        e.preventDefault()
        $.cookie('comment_page', window.location.href, { path: '/' })
        $.cookie('comment_message', $('#id_message').val(), { path: '/' })
        window.location.href = "/accounts/login/"
/*
        $.fancybox({
            href: $("#auth-link").prop("href")||"/accounts/login/",
            type: "ajax",
            minWidth: 600,
            transitionIn  : 'elastic',
            transitionOut : 'elastic',
            scrolling : 'no',
            preload   : true,
            autoDimensions : false,
            padding: [50,64,60,64],
            helpers: {
                overlay: {
                    locked: false,
                    css : { 'overflow' : 'no' }
                }
            },
            afterShow: function(){
                $('.fancybox-inner').on('click', 'a', function(e){
                    e.preventDefault()
                    var url = $(this).prop('href')
                    $('.fancybox-inner')
                        .addClass('load')
                        .load(url, function(){
                            $('.fancybox-inner').removeClass('load')
                            CheckAuth()
                        })
                })
                CheckAuth()
            },
            afterClose: function(){
                window.location.reload()
            }
        })
*/
    })
    if($.cookie('comment_page')) {
    	$.removeCookie('comment_page', { path: '/' })
    	var formId = $.cookie('comment_form_id') || 'answer_to_0'
    	if($.cookie('comment_message')){
    		$('#'+formId+' textarea').val($.cookie('comment_message'))
    		$('#'+formId).submit()
			$.removeCookie('comment_form_id', { path: '/' })
			$.removeCookie('comment_message', { path: '/' })
    	}
    }
/*
    $("#comments").on('keyup', 'textarea', function(){
    	var commentContent = $.trim($(this).val()),
    		$sendButton = $(this).closest('form').find("[type=submit]")
    	if(commentContent.length>0){
    		$sendButton.removeClass('disabled').removeAttr('disabled')
    	}else{
    		$sendButton.addClass('disabled').attr('disabled', 'disabled')
    	}
    })
*/
})