$(function(){
    $('.index_banner .iosslider').iosSlider({
        snapToChildren: true,
        desktopClickDrag: true,
        infiniteSlider: true,
        snapSlideCenter: true,
        navSlideSelector: $('.iosSliderButtons .button'),
        onSlideChange: slideContentChange,
        onSliderLoaded: slideContentChange,
        autoSlide: true,
        autoSlideTimer: 2500
    })
    function slideContentChange(args) {
        $('.iosSliderButtons .button').removeClass('active');
        $('.iosSliderButtons .button:eq(' + (args.currentSlideNumber - 1) + ')').addClass('active');
    }
    $('.iosSliderButtons').on('click', '.button', function(e){
        var $this = $(this)
        if($this.hasClass('active')){
            e.preventDefault()
            var url = $('.index_banner .iosslider .slide:eq('+$this.index()+') .slide-link a').attr('href')
            if(url) window.location.href = url
        }
        $('.index_banner .iosslider').iosSlider('autoSlidePause')
    })
    $('.announce-list .iosslider').each(function(){
        var $this = $(this),
            $prev = $this.siblings('.prev'),
            $next = $this.siblings('.next')
        $this.iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            infiniteSlider: true,
            navNextSelector: $next,
            navPrevSelector: $prev,
            autoSlide: true
        })
    })
    $('a.scheme').fancybox({
        type: 'image',
        padding: [60,64,60,64]
    })
})