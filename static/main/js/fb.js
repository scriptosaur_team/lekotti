$(document).ready(function() {
    var messageMaxLength = 200
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_UK/all.js', function(){
        var pageAccessToken = '760259827321136|e3df330624baa16f91505630d2a74ac7';
        FB.api(
            '/Lekotti/feed?fields=from,message,id,source,picture&date_format=U&limit=2',
            {access_token : pageAccessToken},
            function(response){
                var fb_messages = response.data;
                console.log(fb_messages);
                if(fb_messages){
                    for(var i in fb_messages){
                        var postDate = new Date(fb_messages[i].created_time*1000),
                            dateFormatted = postDate.toLocaleDateString(),
                            fb_message = fb_messages[i].message,
                            fb_picture = fb_messages[i].picture,
                            user = fb_messages[i].from,
                            fb_video = fb_messages[i].source,
                            fb_id = fb_messages[i].id;
                            
	                        var fb_bit_link = new Array();
	                        fb_bit_link = fb_id.split('_');
                            
                            

                        if(fb_message){
                            if(fb_message.length > messageMaxLength){
                                var trimmedString = fb_message.substr(0, messageMaxLength)
                                fb_message = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + ' …'
                            }
                            fb_message = "<p>" + fb_message + "</p>"
                        } else {
                            fb_message = ''
                        }
                        
                        if(fb_picture){
                            fb_picture = "<img class='img-responsive fb-mess-picture' src='" + fb_picture + "'>"
                        } else {
                            fb_picture = ''
                        }
                        if(fb_video){
	                        fb_video = "<div>" + fb_video + "</div>"
                        }else {
	                        fb_video ='';
                        }
                    	if(fb_bit_link){
	                        var fb_bit_link_first = fb_bit_link[0];
	                        var fb_bit_link_second = fb_bit_link[1];
	                        /* var fb_link_url = "https://www.facebook.com/"+fb_bit_link_first+"/posts/"+fb_bit_link_second; */
                    }


                        var mess =
                            "<div class='fb-message-wrap media'>" +
                                "<img src='https://graph.facebook.com/"+ user.id +"/picture' class='fb-avatar pull-left media-object'>" +
                                "<div class='media-body'>" +
                                	"<a href='https://www.facebook.com/"+fb_bit_link_first+"/posts/"+fb_bit_link_second+"' target='_blank' class='fb-url'>"+
                                    "<p>" + user.name + "</p>" +
                                    fb_message +
                                    fb_picture +
                                    fb_video +
                                    "<time>" + dateFormatted + "</time>" +
                                    "</a>" +
                                "</div>" +
                            "</div>"
                        $("#fb-place").append($(mess));
                    }
                }
            }
        );
    });
})