$(function(){
    $('.booking-widget').each(function(){
        var $widget = $(this),
            baseUrl = $widget.data('booking-url'),
            checkin_weekday = $widget.data('checkin_weekday'),
            nights = $widget.data('nights'),
            $rules = $widget.find('.reservation-rules'),
            $showRules = $widget.find('[href=#rules]'),
            $hideRules = $widget.find('[href=#hide_rules]'),
            $checkIn = $widget.find('.date-select.check-in'),
            $checkInField = $checkIn.find('input.datepicker'),
            $checkInValue = $checkIn.find('.check-in-value'),
            $checkOut = $widget.find('.date-select.check-out'),
            $checkOutField = $checkOut.find('input.datepicker'),
            $checkOutValue = $checkOut.find('.check-out-value'),
            $submit = $widget.find('.submit')
        $showRules.click(function(e){
            e.preventDefault()
            if(!$showRules.hasClass('inactive')){
                $rules.slideDown('fast')
                $showRules.addClass('inactive')
            }
        })
        $hideRules.click(function(e){
            e.preventDefault()
            $rules.slideUp('fast')
            $showRules.removeClass('inactive')
        })
        $checkInField.datepicker({
            dateFormat: 'dd.mm.yy',
            minDate: 1,
            onSelect: function(dateText, inst){
                var selectedDate = $(this).val(),
                    dateArray = selectedDate.split('.'),
                    dateFormatted = dateArray[0]+'.'+dateArray[1]
                $checkInValue.text(dateFormatted)
                if(nights!=undefined){
                    var checkoutDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)
                    checkoutDate.setTime(checkoutDate.getTime() + (24 * 60 * 60 * 1000 * nights))
                    $checkOutValue.text($.datepicker.formatDate('dd.mm', checkoutDate))
                    $checkOutField.datepicker( "setDate", checkoutDate)
                }
                CreateLink()
            },
            beforeShowDay: function(dday){
                if(checkin_weekday != undefined){
                    var dWeekday = (dday.getDay()+6)%7
                    if(dWeekday == checkin_weekday)
                        return [1]
                    else
                        return [0]
                } else
                    return [1]
            }
        })
        $checkIn.click(function(){
            $checkInField.datepicker('show')
        })
        $checkOutField.datepicker({
            dateFormat: 'dd.mm.yy',
            minDate: 1+parseInt(nights),
            onSelect: function(dateText, inst){
                var selectedDate = $(this).val(),
                    dateArray = selectedDate.split('.'),
                    dateFormatted = dateArray[0]+'.'+dateArray[1]
                $checkOutValue.text(dateFormatted)
                if(nights!=undefined){
                    var checkinDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)
                    checkinDate.setTime(checkinDate.getTime() - (24 * 60 * 60 * 1000 * nights))
                    $checkInValue.text($.datepicker.formatDate('dd.mm', checkinDate))
                    $checkInField.datepicker( "setDate", checkinDate)
                }
                CreateLink()
            },
            beforeShowDay: function(dday){
                if(checkin_weekday != undefined && nights != undefined){
                    var dWeekday = (dday.getDay()+6) % 7,
                        checkout_weekday = (checkin_weekday+nights) % 7
                    if(dWeekday == checkout_weekday)
                        return [1]
                    else
                        return [0]
                } else
                    return [1]
            }
        })
        $checkOut.click(function(){
            $checkOutField.datepicker('show')
        })
        function CreateLink(){
            var checkInDateArray = $checkInField.val().split('.'),
                checkOutDateArray = $checkOutField.val().split('.'),
                link = baseUrl + (baseUrl.indexOf('?')<0?'?':'&')
                    + 'checkin_monthday='+checkInDateArray[0]
                    + '&checkin_year_month='+checkInDateArray[2]+'-'+checkInDateArray[1]
                    + '&checkout_monthday='+checkOutDateArray[0]
                    + '&checkout_year_month='+checkOutDateArray[2]+'-'+checkOutDateArray[1]
            $submit.prop('href', link)
        }
        CreateLink()
    })
})