from django.db import models
from django.utils.translation import ugettext_lazy as _


class Season(models.Model):
    name = models.CharField(_('name'), max_length=16)
    code = models.SlugField(_('code'), help_text=_('for use in program code'), editable=False)
    date_begin = models.CharField(_('date_begin'), max_length=5, help_text=_('date format: dd.mm'), default='')

    class Meta:
        verbose_name = _('season')
        verbose_name_plural = _('seasons')

    def __unicode__(self):
        return u'%s' % self.name