from django.contrib import admin
from .models import *


class SeasonAdmin(admin.ModelAdmin):
    list_display = ['name', 'date_begin', 'code']
    list_editable = ['date_begin']
    readonly_fields = ['code']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Season, SeasonAdmin)
